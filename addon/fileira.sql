-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Tempo de geração: 20-Dez-2023 às 09:51
-- Versão do servidor: 8.0.31
-- versão do PHP: 8.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `fileira`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `auditing`
--

DROP TABLE IF EXISTS `auditing`;
CREATE TABLE IF NOT EXISTS `auditing` (
  `id` int NOT NULL AUTO_INCREMENT,
  `system_element` varchar(60) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `action_executed` varchar(60) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `data_before` text COLLATE utf8mb4_general_ci,
  `data_after` text COLLATE utf8mb4_general_ci,
  `user_name` varchar(60) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `execution_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=125813 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Extraindo dados da tabela `auditing`
--

INSERT INTO `auditing` (`id`, `system_element`, `action_executed`, `data_before`, `data_after`, `user_name`, `execution_date`) VALUES
(125582, 'utilizador', 'inserir', '', '', '', '2023-10-05 09:27:42'),
(125583, 'produto', 'inserir', '', 'Identificador: 336, Designação: Cabax, Código pautal: 23, Prodesi: yes', '', '2023-10-06 07:48:12'),
(125584, 'produto', 'inserir', '', 'Identificador: 337, Designação: Cabawwx, Código pautal: 23, Prodesi: yes', '', '2023-10-06 07:49:48'),
(125585, 'fileira', 'alterar', '', '', '', '2023-10-11 10:35:12'),
(125586, 'fileira', 'alterar', 'Identificador: 2, Fileira: Cana de Açucar, Comentário: O caldo da cana extraído nas moendas da indústria é bombeado, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Identificador: 2, Fileira: Cana de papa, Comentário: Boa fileira, Identificador Tipo produção: 1, Tipo produção: Vegetal', '', '2023-10-11 11:16:19'),
(125587, 'fileira', 'inserir', '', 'Identificador: 42, Fileira: Mingal, Comentário: Boa fileira, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Adminin', '2023-10-12 09:12:14'),
(125588, 'fileira', 'inserir', '', 'Identificador: 43, Fileira: Padal, Comentário: Boa fileira, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Adminin', '2023-10-12 09:24:32'),
(125589, 'fileira', 'inserir', '', 'Identificador: 44, Fileira: Pandal, Comentário: Boa fileira, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Adminin', '2023-10-12 09:27:25'),
(125590, '../documents/subRowPhoto/2/', 'image', '', '', 'Adminin', '2023-10-12 09:56:48'),
(125591, '../documents/subRowPhoto/2/', 'image', '', '', 'Adminin', '2023-10-12 09:58:19'),
(125592, 'sub fileira', 'inserir', '', 'Identificador: 26, Designação: Pandal, Comment: Boa fileira', 'Adminin', '2023-10-12 10:37:41'),
(125593, 'sub fileira', 'inserir', '', 'Identificador: 27, Designação: Malabal, Comment: Boa fileira', 'Adminin', '2023-10-12 10:39:51'),
(125594, '../documents/productPhoto/9/', '', '', '', 'Adminin', '2023-10-12 13:04:31'),
(125595, '../documents/productPhoto/9/', '', '', '', 'Adminin', '2023-10-12 13:04:50'),
(125596, '../documents/productPhoto/9/', '', '', '', 'Adminin', '2023-10-12 13:05:16'),
(125597, 'produto', 'inserir', '', 'Identificador: 338, Designação: Maladax, Código pautal: 23, Prodesi: yes', 'Adminin', '2023-10-12 13:41:23'),
(125598, 'produto', 'inserir', '', 'Identificador: 339, Designação: Malad, Código pautal: 23, Prodesi: yes', 'Adminin', '2023-10-12 13:41:50'),
(125599, 'produto', 'inserir', '', 'Identificador: 340, Designação: Mal, Código pautal: 23, Prodesi: yes', 'Adminin', '2023-10-12 13:42:12'),
(125600, '../documents/subProductPhoto/2/', '', '', '', 'Adminin', '2023-10-12 13:59:32'),
(125601, '../documents/subProductPhoto/2/', '', '', '', 'Adminin', '2023-10-12 13:59:51'),
(125602, '../documents/subProductPhoto/2/', '', '', '', 'Adminin', '2023-10-12 14:00:19'),
(125603, '../documents/subProductPhoto/2/', '', '', '', 'Adminin', '2023-10-12 14:00:49'),
(125604, '../documents/subProductPhoto/2/', '', '', '', 'Adminin', '2023-10-12 14:01:31'),
(125605, '../documents/subProductPhoto/2/', '', '', '', 'Adminin', '2023-10-13 07:31:51'),
(125606, 'sub produto', 'inserir', '', 'Identificador: 67, Designação: Pambalax, Código pautal: 23, Prodesi: yes', 'Adminin', '2023-10-13 07:42:14'),
(125607, 'sub produto', 'inserir', '', 'Identificador: 68, Designação: Pambalax, Código pautal: 23, Prodesi: yes', 'Adminin', '2023-10-13 07:47:22'),
(125608, '../documents/subProductPhoto/68/', '', '', '', 'Adminin', '2023-10-13 07:48:34'),
(125609, '../documents/productPhoto/1/', '', '', '', 'Adminin', '2023-10-13 08:08:06'),
(125610, '../documents/productPhoto/2/', '', '', '', 'Adminin', '2023-10-13 08:11:25'),
(125611, 'produto', 'inserir', '', 'Identificador: 341, Designação: Sexta 13, Código pautal: 23, Prodesi: yes', 'Adminin', '2023-10-13 08:12:59'),
(125612, 'produto', 'inserir', '', 'Identificador: 342, Designação: Sexta 13, Código pautal: 23, Prodesi: yes', 'Adminin', '2023-10-13 08:22:43'),
(125613, '../documents/productPhoto/1/', '', '', '', 'Adminin', '2023-10-13 08:36:35'),
(125614, '../documents/subProductPhoto/68/', '', '', '', 'Adminin', '2023-10-13 08:55:18'),
(125615, '../documents/subProductPhoto/1/', '', '', '', 'Adminin', '2023-10-13 08:56:01'),
(125616, '../documents/rowPhoto/1/', 'image', '', '', 'Adminin', '2023-10-13 09:06:23'),
(125617, '../documents/subRowPhoto/1/', 'image', '', '', 'Adminin', '2023-10-13 09:16:30'),
(125618, 'Estrutura nacional de produção', 'eliminar', 'Identificador fileira: 43, Fileira: Padal, Identificador sub fileira: 4, Sub fileira: Alimentar, Identificador produto: 57, Produto: Frango, Identificador sub prodduto: 7, Sub produto: Materiais de construção, Identificador unidade de medida: 2, Símbolo: L', '', '', '2023-10-19 10:30:31'),
(125619, 'Estrutura nacional de produção', 'eliminar', '', '', '', '2023-10-19 10:31:23'),
(125620, 'Estrutura nacional de produção', 'eliminar', '', '', '', '2023-10-19 10:31:47'),
(125621, 'fileira', 'alterar', 'Identificador: 42, Fileira: Mingal, Comentário: Boa fileira, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Identificador: 42, Fileira: Mingaff, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', '', '2023-10-26 11:13:21'),
(125622, 'fileira', 'alterar', 'Identificador: 42, Fileira: Mingaff, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', '', '2023-10-26 11:13:59'),
(125623, 'fileira', 'alterar', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', '', '2023-10-26 11:14:07'),
(125624, 'fileira', 'alterar', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', '', '2023-10-26 11:14:08'),
(125625, 'fileira', 'alterar', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', '', '2023-10-26 11:14:09'),
(125626, 'fileira', 'alterar', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', '', '2023-10-26 11:14:09'),
(125627, 'fileira', 'alterar', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', '', '2023-10-26 11:14:10'),
(125628, 'fileira', 'alterar', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', '', '2023-10-26 11:14:10'),
(125629, 'fileira', 'alterar', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', '', '2023-10-26 11:14:11'),
(125630, 'fileira', 'alterar', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', '', '2023-10-26 11:14:12'),
(125631, 'fileira', 'alterar', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', '', '2023-10-26 11:14:13'),
(125632, 'fileira', 'alterar', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', '', '2023-10-26 11:14:15'),
(125633, 'fileira', 'alterar', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', '', '2023-10-26 11:14:16'),
(125634, 'fileira', 'alterar', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', '', '2023-10-26 11:14:17'),
(125635, 'fileira', 'alterar', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', '', '2023-10-26 11:14:17'),
(125636, 'fileira', 'alterar', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', '', '2023-10-26 11:14:18'),
(125637, 'fileira', 'alterar', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', '', '2023-10-26 11:14:19'),
(125638, 'fileira', 'alterar', 'Identificador: 42, Fileira: Mingal, Comentário: Boa, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Identificador: 42, Fileira: Mingal, Comentário: Boa Fileira, Identificador Tipo produção: 1, Tipo produção: Vegetal', '', '2023-10-26 11:18:28'),
(125639, 'fileira', 'alterar', 'Identificador: 42, Fileira: Mingal, Comentário: Boa Fileira, Identificador Tipo produção: 1, Tipo produção: Vegetal', 'Identificador: 42, Fileira: Mingal, Comentário: Boa Fileirahffffffffffffffffufufuurrufjfjfjffufuufudffyfufuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuweybntjuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuyiy7ik7iygfththbtnnnnnnnbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrjkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkiu, Identificador Tipo produção: 1, Tipo produção: Vegetal', '', '2023-10-26 13:22:18'),
(125640, 'banner', 'eliminar', '', '', '', '2023-10-31 14:04:06'),
(125641, 'banner', 'eliminar', '', '', '', '2023-10-31 14:04:06'),
(125642, 'banner', 'eliminar', '', '', '', '2023-10-31 14:04:06'),
(125643, 'banner', 'eliminar', '', '', '', '2023-10-31 14:04:06'),
(125644, 'banner', 'inserir', '', 'id11titleAmaraldescriptionÉ bom amar', '', '2023-10-31 14:04:54'),
(125645, '../documents/bannerPhoto/10/', 'image', '', '', '', '2023-10-31 14:09:34'),
(125646, 'banner', 'eliminar', '', '', '', '2023-10-31 14:11:16'),
(125647, 'banner', 'eliminar', '', '', '', '2023-10-31 14:11:16'),
(125648, 'banner', 'eliminar', '', '', '', '2023-10-31 14:11:16'),
(125649, 'banner', 'eliminar', '', '', '', '2023-10-31 14:11:16'),
(125650, 'product_5_4', 'inserir', '', 'Identificador: 1, Designação: O arroz é bom!', '', '2023-10-31 15:19:16'),
(125651, '../documents/product_5_4_Photo/1/', 'image', '', '', '', '2023-11-01 09:27:53'),
(125652, 'product_5_4', 'inserir', '', 'Identificador: 2, Designação: Banana', '', '2023-11-01 09:49:29'),
(125653, 'produto', 'alterar', 'Identificador: 2, Designação: Banana', 'Identificador: 2, Designação: Café', '', '2023-11-01 10:02:59'),
(125654, 'produto', 'alterar', 'Identificador: 2, Designação: Café', 'Identificador: 2, Designação: Café', '', '2023-11-01 10:03:09'),
(125655, 'produto', 'eliminar', 'Identificador: 2, Designação: Café', '', '', '2023-11-01 10:24:30'),
(125656, 'produto', 'eliminar', '', '', '', '2023-11-01 10:24:32'),
(125657, 'productive row54', 'inserir', '', 'id: 2, designation, Fatal, descriptionFatal boa', 'Internauta', '2023-11-01 21:48:00'),
(125658, 'productive row54', 'inserir', '', 'id: 3, designation, Fatal, descriptionFatal boa', 'Internauta', '2023-11-01 21:50:52'),
(125659, 'productive row54', 'inserir', '', 'id: 4, designation, Fatal, descriptionFatal boa', 'Internauta', '2023-11-01 21:51:01'),
(125660, 'productive row54', 'inserir', '', 'id: 5, designation, Fatal, descriptionFatal boa', 'Internauta', '2023-11-01 21:52:26'),
(125661, 'productiveRow54', 'alterar', 'id: 5, designation, Fatal, descriptionFatal boa', 'id: 5, designation, Mingal, descriptionFatal boa', 'Internauta', '2023-11-01 22:02:44'),
(125662, 'productivtrow_54', 'eliminar', 'id: 5, designation, Mingal, descriptionFatal boa', '', 'Internauta', '2023-11-01 22:10:05'),
(125663, 'productivtrow_54', 'eliminar', 'id: 6, designation, tyu, descriptionyyu7', '', 'Internauta', '2023-11-01 22:10:05'),
(125664, 'legislação', 'inserir', '', 'id1titlePandalegislation_typePandall', 'Internauta', '2023-11-02 21:29:00'),
(125665, 'legislação', 'alterar', 'id1titlePandalegislation_typePandall', 'id1titlePambalalegislation_typePandall', 'Internauta', '2023-11-02 21:40:09'),
(125666, 'legislação', 'eliminar', 'id1titlePambalalegislation_typePandall', '', 'Internauta', '2023-11-02 21:49:18'),
(125667, 'legislação', 'eliminar', 'id2titleer566legislation_type566', '', 'Internauta', '2023-11-02 21:49:18'),
(125668, 'legislação', 'eliminar', 'id3title5555legislation_type555', '', 'Internauta', '2023-11-02 21:49:18'),
(125669, 'legislação', 'eliminar', '', '', 'Internauta', '2023-11-02 21:50:06'),
(125670, 'legislação', 'eliminar', '', '', 'Internauta', '2023-11-02 21:50:06'),
(125671, 'legislação', 'eliminar', '', '', 'Internauta', '2023-11-02 21:50:06'),
(125672, 'event', 'inserir', '', 'id: 1, title, Novembro azul, event_date2023-11-01 00:00:00, localLuanda, descriptionUma data para conscientização do cancro da próstata!', 'Internauta', '2023-11-03 21:08:42'),
(125673, 'event', 'alterar', 'id: 1, title, Novembro azul, event_date2023-11-01 00:00:00, localLuanda, descriptionUma data para conscientização do cancro da próstata!', 'id: 1, title, Novembro azulax, event_date2023-11-01 00:00:00, localLuanda, descriptionUma data para conscientização do cancro da próstata!', 'Internauta', '2023-11-03 21:22:27'),
(125674, 'event', 'eliminar', 'id: 2, title, ee, event_date2023-11-03 21:25:54, localee, descriptionee', '', 'Internauta', '2023-11-03 21:32:00'),
(125675, 'event', 'eliminar', 'id: 3, title, ee, event_date2023-11-03 21:25:54, localee, descriptione', '', 'Internauta', '2023-11-03 21:32:00'),
(125676, 'product_5_4', 'inserir', '', 'Identificador: 3, Designação: Fuba', '', '2023-11-08 08:10:39'),
(125677, 'product_5_4', 'inserir', '', 'Identificador: 4, Designação: Banana', '', '2023-11-08 08:36:20'),
(125678, 'productive row54', 'inserir', '', 'id: 7, designation, Gado bovino, descriptionFalar é fácil.', '', '2023-11-08 08:39:30'),
(125679, '../documents/rowPhoto54/7/', 'image', '', '', '', '2023-11-08 08:47:27'),
(125680, 'productive row54', 'inserir', '', 'id: 8, designation, Banana, descriptionGadpo suíno', '', '2023-11-08 08:56:36'),
(125681, 'produto', 'alterar', '', '', '', '2023-11-08 13:27:00'),
(125682, 'produto', 'alterar', '', '', '', '2023-11-08 13:28:35'),
(125683, 'productiveRow54', 'alterar', 'id: 8, designation, Banana, descriptionGadpo suíno', 'id: 8, designation, Banaxx, descriptionMuito bom', '', '2023-11-08 13:29:53'),
(125684, 'productiveRow54', 'alterar', 'id: 8, designation, Banaxx, descriptionMuito bom', 'id: 8, designation, Banaxx, descriptionMuito bom', '', '2023-11-08 13:29:57'),
(125685, 'productiveRow54', 'alterar', 'id: 8, designation, Banaxx, descriptionMuito bom', 'id: 8, designation, Banaxx, descriptionMuito bom', '', '2023-11-08 13:29:58'),
(125686, 'productivtrow_54', 'eliminar', 'id: 8, designation, Banaxx, descriptionMuito bom', '', '', '2023-11-08 13:34:27'),
(125687, 'productivtrow_54', 'eliminar', 'id: 7, designation, Gado bovino, descriptionFalar é fácil.', '', '', '2023-11-08 13:34:27'),
(125688, 'productivtrow_54', 'eliminar', '', '', '', '2023-11-08 13:34:31'),
(125689, 'productivtrow_54', 'eliminar', '', '', '', '2023-11-08 13:34:31'),
(125690, 'legislação', 'inserir', '', 'id5titlelegislation_typeLei do câncer', '', '2023-11-09 09:06:51'),
(125691, '../documents/legislationPhoto//', 'image', '', '', '', '2023-11-09 09:18:22'),
(125692, '../documents/legislationPhoto/5/', 'image', '', '', '', '2023-11-09 09:19:19'),
(125693, '../documents/legislationPhoto/5/', 'image', '', '', '', '2023-11-09 09:19:57'),
(125694, '../documents/legislationPhoto/5/', 'image', '', '', '', '2023-11-09 09:20:00'),
(125695, '../documents/legislationPhoto/5/', 'image', '', '', '', '2023-11-09 09:20:17'),
(125696, 'legislação', 'inserir', '', 'id6titleLândanalegislation_typeLei dos bonês!', '', '2023-11-09 09:41:28'),
(125697, 'legislação', 'alterar', 'id6titleLândanalegislation_typeLei dos bonês!', 'id6titleLândanagggglegislation_typeLei dos bonês!', '', '2023-11-09 09:44:31'),
(125698, 'legislação', 'eliminar', 'id5titlelegislation_typeLei do câncer', '', '', '2023-11-09 09:49:40'),
(125699, 'legislação', 'eliminar', 'id6titleLândanagggglegislation_typeLei dos bonês!', '', '', '2023-11-09 09:49:40'),
(125700, 'legislação', 'inserir', '', 'id7titleAmarallegislation_typeLei do câncer', '', '2023-11-09 09:57:43'),
(125701, 'event', 'inserir', '', 'id: 4, title, Amaral, event_date1994-06-21 00:00:00, localAmaral, descriptionAmaral', '', '2023-11-09 13:51:43'),
(125702, '../documents/eventPhoto//', 'image', '', '', '', '2023-11-10 07:52:24'),
(125703, '../documents/eventPhoto/4/', 'image', '', '', '', '2023-11-10 07:53:43'),
(125704, 'event', 'inserir', '', 'id: 5, title, Moisés Laurindo, event_date0000-00-00 00:00:00, localUmpulo, descriptionO Umpulo é uma localização incrível', '', '2023-11-10 08:05:43'),
(125705, 'event', 'inserir', '', 'id: 6, title, Conceição Luís Maurício, event_date1996-05-30 00:00:00, localUmpulo, descriptionO Umpulo é uma localização incrível', '', '2023-11-10 08:07:40'),
(125706, 'event', 'alterar', 'id: 6, title, Conceição Luís Maurício, event_date1996-05-30 00:00:00, localUmpulo, descriptionO Umpulo é uma localização incrível', 'id: 6, title, Amanaxx, event_date1996-05-30 00:00:00, localUmpulo, descriptionO Umpulo é uma localização incrível', '', '2023-11-10 08:44:00'),
(125707, 'event', 'eliminar', 'id: 5, title, Moisés Laurindo, event_date0000-00-00 00:00:00, localUmpulo, descriptionO Umpulo é uma localização incrível', '', '', '2023-11-10 09:02:57'),
(125708, 'event', 'eliminar', 'id: 6, title, Amanaxx, event_date1996-05-30 00:00:00, localUmpulo, descriptionO Umpulo é uma localização incrível', '', '', '2023-11-10 09:02:57'),
(125709, 'legislação', 'inserir', '', 'id8titleAmabrallegislation_typeLei do câncer', '', '2023-11-10 09:38:25'),
(125710, 'legislação', 'inserir', '', 'id9titleAmaggbrallegislation_typeLei do câncer', '', '2023-11-10 09:39:12'),
(125711, 'legislação', 'inserir', '', 'id10titleAmaggbrallegislation_typeLei do câncer', '', '2023-11-10 09:42:09'),
(125712, 'legislação', 'inserir', '', 'id11titleAmaggbrallegislation_typeLei do câncer', '', '2023-11-10 09:44:24'),
(125713, 'legislação', 'inserir', '', 'id12titleAmaggbrallegislation_typeLei do câncer', '', '2023-11-10 09:50:35'),
(125714, 'legislação', 'inserir', '', 'id13titleAmaggfbrallegislation_typeLei do câncer', '', '2023-11-10 09:53:00'),
(125715, 'legislação', 'inserir', '', 'id14titleAmagtgfbrallegislation_typeLei do câncer', '', '2023-11-10 09:53:48'),
(125716, 'legislação', 'inserir', '', 'id15titleAmagtgfbrallegislation_typeLei do câncer', '', '2023-11-10 09:56:10'),
(125717, 'legislação', 'inserir', '', 'id16titleAmagtgfbrttallegislation_typeLei do câncer', '', '2023-11-10 09:56:57'),
(125718, 'legislação', 'inserir', '', 'id17titleAmagtggfbrttallegislation_typeLei do câncer', '', '2023-11-10 09:57:59'),
(125719, 'legislação', 'inserir', '', 'id1titleAmaral', '', '2023-11-10 11:25:05'),
(125720, 'legislação', 'inserir', '', 'id2titleAmaral', '', '2023-11-10 11:27:01'),
(125721, 'legislação', 'inserir', '', 'id3titleAmardal', '', '2023-11-10 11:27:15'),
(125722, '../documents/galleryPhoto/3/', 'image', '', '', '', '2023-11-10 11:36:35'),
(125723, 'legislação', 'inserir', '', 'id4titleConceição Luís Maurício', '', '2023-11-10 13:09:36'),
(125724, 'legislação', 'alterar', 'id4titleConceição Luís Maurício', 'id4titlemamax Lino', '', '2023-11-10 13:12:44'),
(125725, 'legislação', 'eliminar', 'id3titleAmardal', '', '', '2023-11-10 13:25:11'),
(125726, 'legislação', 'eliminar', 'id4titlemamax Lino', '', '', '2023-11-10 13:25:11'),
(125727, 'video', 'inserir', '', 'id2titleBatalha de honralinkwww.google.com.br', '', '2023-11-20 08:52:00'),
(125728, 'Video', 'alterar', 'id2titleBatalha de honralinkwww.google.com.br', 'id2titleBatalha Simpleslinkwww.google.com.br', '', '2023-11-20 08:55:35'),
(125729, 'video', 'eliminar', 'id2titleBatalha Simpleslinkwww.google.com.br', '', '', '2023-11-20 09:02:41'),
(125730, 'video', 'inserir', '', 'id3titleBatalha de honralinkwww.google.com.br', '', '2023-11-21 13:18:01'),
(125731, 'video', 'inserir', '', 'id4titleBataccclinkwww.google.com.br', '', '2023-11-21 13:18:37'),
(125732, 'video', 'inserir', '', 'id5titleBatacchjclinkwww.google.com.br', '', '2023-11-22 13:37:37'),
(125733, 'video', 'inserir', '', 'id6titleBatavvvcchjclinkwww.google.com.br', '', '2023-11-22 13:38:00'),
(125734, 'video', 'inserir', '', 'id7titleBatavvvcddchjclinkwww.google.com.br', '', '2023-11-22 13:38:07'),
(125735, 'ppn', 'inserir', '', '', '', '2023-11-24 07:30:27'),
(125736, 'ppn', 'alterar', '', '', '', '2023-11-24 07:33:16'),
(125737, 'ppn', 'alterar', '', '', '', '2023-11-24 07:33:39'),
(125738, 'ppn', 'alterar', '', '', '', '2023-11-24 07:36:07'),
(125739, 'ppn', 'alterar', '', '', '', '2023-11-24 07:36:22'),
(125740, 'ppn', 'inserir', '', '', '', '2023-11-24 07:37:43'),
(125741, 'ppn', 'inserir', '', '', '', '2023-11-24 07:37:55'),
(125742, 'ppn', 'eliminar', '', '', '', '2023-11-24 07:49:41'),
(125743, 'mpme', 'inserir', '', 'Nome: Desenvolvimento, Sigla: PNP, Descrição: Departamento para o Planeamento, Direção: Bom nas Tecnologias de Informação.', '', '2023-11-24 08:42:02'),
(125744, 'mpme', 'alterar', 'Nome: Desenvolvimento, Sigla: PNP, Descrição: Departamento para o Planeamento, Direção: Bom nas Tecnologias de Informação.', 'Nome: Desenvolvimento, Sigla: PNP, Descrição: Departamento para o Planeamento, Direção: Bom nas Tecnologias de Informação.', '', '2023-11-24 08:43:00'),
(125745, 'mpme', 'inserir', '', '', '', '2023-11-24 08:46:30'),
(125746, 'mpme', 'inserir', '', '', '', '2023-11-24 08:46:31'),
(125747, 'mpme', 'eliminar', '', '', '', '2023-11-24 08:55:53'),
(125748, 'trade balance category', 'inserir', '', 'Identificador: 1, Designação: Padal', 'Internauta', '2023-12-01 19:52:37'),
(125749, 'trade_balance_category', 'alterar', 'Identificador: 1, Designação: Padal', 'Identificador: 1, Designação: Mingal', 'Internauta', '2023-12-01 20:02:45'),
(125750, 'trade balance category', 'inserir', '', 'Identificador: 2, Designação: Mingald', 'Internauta', '2023-12-01 20:05:36'),
(125751, 'trade balance category', 'eliminar', 'Identificador: 1, Designação: Mingal', '', 'Internauta', '2023-12-01 20:09:20'),
(125752, 'productBalance', 'inserir', '', 'Identificador: 1, Designação: Mala, Comment: Comida', '', '2023-12-06 08:56:40'),
(125753, '../documents/productBalancePhoto/1/', 'image', '', '', '', '2023-12-06 09:10:56'),
(125754, 'product balance', 'alterar', '', '', '', '2023-12-06 09:15:58'),
(125755, 'product balance', 'alterar', 'Identificador: 1, Designação: Mala, Comment: Comida', 'Identificador: 1, Designação: Malandro, Comment: Comida', '', '2023-12-06 09:16:14'),
(125756, 'productBalance', 'inserir', '', 'Identificador: 2, Designação: Falanga, Comment: Comida', '', '2023-12-06 09:18:42'),
(125757, 'product_balance', 'eliminar', 'Identificador: 2, Designação: Falanga, Comment: Comida', '', '', '2023-12-06 09:25:27'),
(125758, 'trade balance category', 'inserir', '', 'Identificador: 9, Designação: mn', '', '2023-12-09 17:38:24'),
(125759, 'trade_balance_category', 'alterar', 'Identificador: 9, Designação: mn', 'Identificador: 9, Designação: manal', '', '2023-12-09 17:40:06'),
(125760, 'trade balance category', 'inserir', '', 'Identificador: 10, Designação: mghn', '', '2023-12-09 17:40:44'),
(125761, 'trade balance category', 'eliminar', 'Identificador: 10, Designação: mghn', '', '', '2023-12-09 17:47:43'),
(125762, 'trade balance', 'inserir', '', '', '', '2023-12-09 18:10:30'),
(125763, 'trade_balance', 'alterar', '', '', '', '2023-12-09 18:15:25'),
(125764, 'trade balance', 'inserir', '', '', '', '2023-12-09 18:15:36'),
(125765, 'trade balance', 'eliminar', '', '', '', '2023-12-09 18:22:49'),
(125766, 'productBalance', 'inserir', '', 'Identificador: 3, Designação: Falanga, Comment: Comida', '', '2023-12-11 13:02:40'),
(125767, 'trade balance', 'inserir', '', 'Identificador: 12, Designação: Pecuária', '', '2023-12-13 09:55:00'),
(125768, 'trade balance', 'inserir', '', 'Identificador: 13, Designação: Cereais', '', '2023-12-13 09:55:06'),
(125769, 'trade balance', 'inserir', '', 'Identificador: 14, Designação: Frutos', '', '2023-12-13 09:56:13'),
(125770, 'trade balance', 'inserir', '', 'Identificador: 15, Designação: Leguminosa', '', '2023-12-13 10:08:07'),
(125771, 'training_initiative', 'inserir', '', '', '', '2023-12-13 10:56:11'),
(125772, 'training_initiative', 'alterar', '', '', '', '2023-12-13 11:00:48'),
(125773, 'training_initiative', 'inserir', '', '', '', '2023-12-13 11:01:20'),
(125774, 'training_initiative', 'inserir', '', '', '', '2023-12-13 11:01:28'),
(125775, 'training_initiative', 'alterar', '', '', '', '2023-12-13 11:01:55'),
(125776, 'training_initiative', 'eliminar', '', '', '', '2023-12-13 11:07:20'),
(125777, 'training_initiative', 'eliminar', '', '', '', '2023-12-13 11:07:20'),
(125778, 'training_initiative', 'eliminar', '', '', '', '2023-12-13 11:12:12'),
(125779, 'training_initiative', 'eliminar', '', '', '', '2023-12-13 11:12:12'),
(125780, 'training', 'inserir', '', '', '', '2023-12-13 11:23:10'),
(125781, 'training', 'alterar', '', '', '', '2023-12-13 11:27:05'),
(125782, 'training', 'inserir', '', '', '', '2023-12-13 11:27:16'),
(125783, 'training', 'inserir', '', '', '', '2023-12-13 11:27:18'),
(125784, 'training', 'eliminar', '', '', '', '2023-12-13 11:31:44'),
(125785, 'training', 'eliminar', '', '', '', '2023-12-13 11:31:44'),
(125786, 'training_initiative', 'inserir', '', '', '', '2023-12-15 08:58:02'),
(125787, 'training_initiative', 'inserir', '', '', '', '2023-12-15 08:58:19'),
(125788, 'training_initiative', 'inserir', '', '', '', '2023-12-15 08:58:34'),
(125789, 'training_initiative', 'inserir', '', '', '', '2023-12-15 08:58:53'),
(125790, 'training_initiative', 'inserir', '', '', '', '2023-12-15 08:59:19'),
(125791, 'training_initiative', 'inserir', '', '', '', '2023-12-15 09:00:42'),
(125792, 'training_initiative', 'inserir', '', '', '', '2023-12-15 09:00:57'),
(125793, 'training_initiative', 'inserir', '', '', '', '2023-12-15 09:01:11'),
(125794, 'training', 'inserir', '', '', '', '2023-12-15 09:10:22'),
(125795, 'training', 'inserir', '', '', '', '2023-12-15 09:11:09'),
(125796, 'training', 'alterar', '', '', '', '2023-12-15 10:39:54'),
(125797, 'training', 'alterar', '', '', '', '2023-12-15 10:40:33'),
(125798, 'training', 'alterar', '', '', '', '2023-12-15 10:40:34'),
(125799, 'training', 'inserir', '', '', '', '2023-12-15 10:41:04'),
(125800, 'training', 'inserir', '', '', '', '2023-12-15 10:50:52'),
(125801, 'training', 'alterar', '', '', '', '2023-12-15 11:09:59'),
(125802, 'training', 'alterar', '', '', '', '2023-12-15 11:10:01'),
(125803, 'training', 'alterar', '', '', '', '2023-12-15 11:10:02'),
(125804, 'training', 'alterar', '', '', '', '2023-12-15 11:10:03'),
(125805, 'training', 'inserir', '', '', '', '2023-12-15 11:11:14'),
(125806, 'training', 'inserir', '', '', '', '2023-12-15 11:11:37'),
(125807, 'training', 'inserir', '', '', '', '2023-12-15 11:12:42'),
(125808, 'training', 'inserir', '', '', '', '2023-12-15 11:13:16'),
(125809, 'training', 'inserir', '', '', '', '2023-12-15 11:13:45'),
(125810, 'training', 'inserir', '', '', '', '2023-12-15 12:23:46'),
(125811, 'training', 'inserir', '', '', '', '2023-12-15 12:26:59'),
(125812, 'training', 'inserir', '', '', '', '2023-12-15 12:27:25');

-- --------------------------------------------------------

--
-- Estrutura da tabela `banner`
--

DROP TABLE IF EXISTS `banner`;
CREATE TABLE IF NOT EXISTS `banner` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(80) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `banner`
--

INSERT INTO `banner` (`id`, `title`, `description`) VALUES
(4, 'Azaeer', 'É bom amar'),
(5, 'Az67er', 'É bom amar'),
(10, 'Azwrteee67er', 'É bom amar'),
(11, 'Amaral', 'É bom amar');

-- --------------------------------------------------------

--
-- Estrutura da tabela `department`
--

DROP TABLE IF EXISTS `department`;
CREATE TABLE IF NOT EXISTS `department` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `acronym` varchar(30) NOT NULL,
  `description` varchar(100) NOT NULL,
  `id_direction` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_direction` (`id_direction`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `department`
--

INSERT INTO `department` (`id`, `name`, `acronym`, `description`, `id_direction`) VALUES
(1, 'Desenvolvimento', 'PNP', 'Departamento para o Planeamento', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `direction`
--

DROP TABLE IF EXISTS `direction`;
CREATE TABLE IF NOT EXISTS `direction` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `acronym` varchar(30) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `direction`
--

INSERT INTO `direction` (`id`, `name`, `acronym`, `description`) VALUES
(1, 'GTI', '', 'Bom nas Tecnologias de Informação.'),
(5, 'Direcção Nacional do Ambiente do Negócio.', 'DNAP', 'Um departamento que cria oportunidades para o crescimento do País.'),
(7, ' do Negócio.', 'DNAP', 'Um departamento que cria oportunidades para o crescimento do País.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `event`
--

DROP TABLE IF EXISTS `event`;
CREATE TABLE IF NOT EXISTS `event` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `event_date` datetime NOT NULL,
  `local` varchar(100) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `event`
--

INSERT INTO `event` (`id`, `title`, `event_date`, `local`, `description`) VALUES
(1, 'Novembro azulax', '2023-11-01 00:00:00', 'Luanda', 'Uma data para conscientização do cancro da próstata!'),
(4, 'Amaral', '1994-06-21 00:00:00', 'Amaral', 'Amaral');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fair`
--

DROP TABLE IF EXISTS `fair`;
CREATE TABLE IF NOT EXISTS `fair` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_province` int NOT NULL,
  `held_fair` int NOT NULL,
  `quant_productor` int NOT NULL,
  `quant_fisherman` int NOT NULL,
  `quant_marketer` int NOT NULL,
  `quant_activity` int NOT NULL,
  `quant_partnership` int NOT NULL,
  `quant_former` int NOT NULL,
  `quant_sponsor` int NOT NULL,
  `quant_ocd` int NOT NULL,
  `made_contract` int NOT NULL,
  `transactional_amount` int NOT NULL,
  `catering_operator` int NOT NULL,
  `service_provider` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_province` (`id_province`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `fair`
--

INSERT INTO `fair` (`id`, `id_province`, `held_fair`, `quant_productor`, `quant_fisherman`, `quant_marketer`, `quant_activity`, `quant_partnership`, `quant_former`, `quant_sponsor`, `quant_ocd`, `made_contract`, `transactional_amount`, `catering_operator`, `service_provider`) VALUES
(2, 16, 2000, 7, 9, 2, 8, 2, 7, 7, 9, 9, 12, 90, 0),
(5, 3, 2000, 7, 9, 2, 8, 2, 7, 7, 9, 9, 12, 90, 78);

-- --------------------------------------------------------

--
-- Estrutura da tabela `gallery`
--

DROP TABLE IF EXISTS `gallery`;
CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `gallery`
--

INSERT INTO `gallery` (`id`, `title`) VALUES
(2, 'Amaral');

-- --------------------------------------------------------

--
-- Estrutura da tabela `group_user`
--

DROP TABLE IF EXISTS `group_user`;
CREATE TABLE IF NOT EXISTS `group_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `group_user`
--

INSERT INTO `group_user` (`id`, `name`, `description`) VALUES
(1, 'Directores', 'Este é um grupo que colecciona todos os directores.'),
(2, 'Quadros Sênior', 'Este é um grupo que colecciona todos os directores.'),
(3, 'Secretários do Estados', 'Este é um grupo que colecciona todos os directores.');

-- --------------------------------------------------------

--
-- Estrutura da tabela `legislation`
--

DROP TABLE IF EXISTS `legislation`;
CREATE TABLE IF NOT EXISTS `legislation` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `legislation_type` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `legislation`
--

INSERT INTO `legislation` (`id`, `title`, `legislation_type`) VALUES
(4, 'd', 'd'),
(15, 'Amagtgfbral', 'Lei do câncer'),
(16, 'Amagtgfbrttal', 'Lei do câncer'),
(17, 'Amagtggfbrttal', 'Lei do câncer');

-- --------------------------------------------------------

--
-- Estrutura da tabela `mpme`
--

DROP TABLE IF EXISTS `mpme`;
CREATE TABLE IF NOT EXISTS `mpme` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_province` int NOT NULL,
  `quant_micro` int NOT NULL,
  `quant_small` int NOT NULL,
  `quant_average` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_province` (`id_province`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `mpme`
--

INSERT INTO `mpme` (`id`, `id_province`, `quant_micro`, `quant_small`, `quant_average`) VALUES
(1, 1, 200, 3, 45),
(2, 1, 2, 3, 45);

-- --------------------------------------------------------

--
-- Estrutura da tabela `office`
--

DROP TABLE IF EXISTS `office`;
CREATE TABLE IF NOT EXISTS `office` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `description` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `office`
--

INSERT INTO `office` (`id`, `name`, `description`) VALUES
(1, 'office', 'c1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ppn`
--

DROP TABLE IF EXISTS `ppn`;
CREATE TABLE IF NOT EXISTS `ppn` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_province` int NOT NULL,
  `agricuture` int NOT NULL,
  `livestock` int NOT NULL,
  `fishing` int NOT NULL,
  `production_industry` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_province` (`id_province`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `ppn`
--

INSERT INTO `ppn` (`id`, `id_province`, `agricuture`, `livestock`, `fishing`, `production_industry`) VALUES
(2, 1, 30, 3, 45, 7),
(3, 1, 2, 2, 45, 7);

-- --------------------------------------------------------

--
-- Estrutura da tabela `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(40) NOT NULL,
  `customs_tariff` varchar(30) NOT NULL,
  `prodesi_product` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=343 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `product`
--

INSERT INTO `product` (`id`, `designation`, `customs_tariff`, `prodesi_product`) VALUES
(1, 'Trigo', '1101.00.10', 'sim'),
(2, 'Arroz', '1006.20.00', 'sim'),
(5, 'Milho', '1104.23.00', 'sim'),
(6, 'Massambala', '1007.90.00', 'Sim'),
(9, 'Cana-de-açúcar', '1212.93.00', 'sim'),
(10, 'Batata rena', '0710.10.00', 'sim'),
(11, 'Mandioca', '0714.10.00', 'sim'),
(12, 'Batata-doce', '0714.20.00', 'sim'),
(13, 'Feijão', '0713.90.00', 'sim'),
(14, 'Amendoim', '2008.11.00', 'sim'),
(15, 'Soja', '1208.10.00', 'sim'),
(16, 'Dendem', '0601.20.00', 'sim'),
(18, 'Coco', '0801.11.00', 'sim'),
(19, 'Ginguba ', '1202.41.00', 'sim'),
(20, 'Café', '0901.90.00', 'sim'),
(21, 'Cacau', '0901.90.00', 'sim'),
(22, 'Chá', '2101.20.00', 'sim'),
(23, 'Gengibre', '0910.12.00', 'sim'),
(24, 'Pimenta', '0709.60.00', 'sim'),
(25, 'Gindungo', '0709.99.00', 'sim'),
(26, 'Canela', '0906.11.00', 'sim'),
(27, 'Banana', '0803.90.00', 'sim'),
(28, 'Manga', '0804.50.00', 'sim'),
(29, 'Abacaxi', '0804.30.00', 'sim'),
(30, 'Abacate', '0804.40.00', 'sim'),
(31, 'Citrinos', '', 'sim'),
(32, 'Maçã', '0808.10.00', 'sim'),
(33, 'Tangerina', '0805.22.00', 'sim'),
(34, 'Laranja', '080510.00', 'sim'),
(35, 'Limão', '0805.50.00', 'sim'),
(36, 'Pêra', '0808.30.00', 'sim'),
(37, 'Morango', '0810.10.00', 'sim'),
(38, 'Goiaba', '0804.50.00', 'sim'),
(39, 'Mamão', '0807.20.00', 'sim'),
(40, 'Melancia', '0807.11.00', 'sim'),
(41, 'Tomate', '0702.00.00', 'sim'),
(42, 'Pepino', '0707.00.00', 'sim'),
(43, 'Cenoura', '0706.10.10', 'sim'),
(44, 'Cebola', '0703.10.00', 'sim'),
(45, 'Couve', '0704.90.00', 'sim'),
(46, 'Alho', '0703.20.00', 'sim'),
(47, 'Repolho', '0705.11.00', 'sim'),
(48, 'Alface', '0705.19.00', 'sim'),
(49, 'Gimboa', '', 'sim'),
(50, 'Beringela', '0709.30.00', 'sim'),
(51, 'Nabo', '', 'sim'),
(52, 'Quiabo', '0706.90.00', 'sim'),
(53, 'Pimento', '0709.60.00', 'sim'),
(56, 'Galinha rija', '', 'sim'),
(57, 'Frango', '', 'sim'),
(58, 'Ovos', '0407.90.00', 'sim'),
(59, 'Tilapia', '0302.71.00', 'sim'),
(60, 'Bagre (Clárias Gariepinus)', '', 'sim'),
(62, 'Carapau do Cunene', '0302.45.00', 'sim'),
(63, 'Sardinela maderensis(palheta)', '0302.43.00', 'sim'),
(64, 'Sardinela aurita (lambula)', '0302.43.00', 'sim'),
(65, 'Sardinha do Reino', '', 'sim'),
(66, 'Atum', '0302.39.00', 'sim'),
(67, 'Caxuxu', '', 'sim'),
(68, 'Corvinas', '', 'sim'),
(69, 'Pescadas', '', 'sim'),
(70, 'Roncadores', '', 'sim'),
(71, 'Linguado', '0302.23.00', 'sim'),
(72, 'Peixe-Espada', '', 'sim'),
(73, 'Choco', '0307.42.00', 'sim'),
(74, 'Lulas', '', 'sim'),
(75, 'Polvos', '0307.51.00', 'sim'),
(76, 'Camarão', '0306.95.00', 'sim'),
(77, 'Caranguejo', '0306.93.00', 'sim'),
(80, 'Cimento', '3816.00.00', 'sim'),
(82, 'Argamassa', '3824.50.00', 'sim'),
(83, 'Rebocos', '', 'sim'),
(84, 'Tintas para Construção', '3210.00.00', 'sim'),
(85, 'Gesso', '2520.20.00', 'sim'),
(86, 'Óleo Alimentar de Soja', '1507.90.00', 'sim'),
(87, 'Caldo de cana', '', 'sim'),
(92, 'Bombó', '1901.90.90', 'sim'),
(95, 'Água de Mesa', '2201.90.00', 'sim'),
(96, 'Sumos', '2009.89.00', 'sim'),
(103, 'Rochas Ornamentais', '6802.10.00', 'sim'),
(107, 'Calçados ', '00', 'sim'),
(115, 'Hotel', '', 'sim'),
(116, 'Aldeamento Turístico, Estalagem e Motel', '', 'sim'),
(117, 'Pensão e Hospedaria', '', 'sim'),
(118, 'Aparthotel', '', 'sim'),
(119, 'Restaurantes e Similares', '', 'sim'),
(124, 'Maracujá', '0810.90.00', 'Sim'),
(127, 'Suínos ', '0', 'Sim'),
(128, 'Ovinos ', '0', 'Sim'),
(129, 'Caprinos', '0', 'Sim'),
(131, 'Búfalos', '0', 'Sim'),
(132, 'Açúcar ', '1701.1390', 'Sim'),
(133, 'Caju ', '0', 'Sim'),
(140, 'Carne de ovino ', '0204.10.00', 'Sim'),
(143, 'Cerveja', '2203.00.00', 'Sim'),
(144, 'Fuba de bombó', '1108.14.00', 'Sim'),
(145, 'Iogurte', '0', 'Sim'),
(146, 'Leite em pó', '0402.10', 'Sim'),
(147, 'Manteiga', '0', 'Sim'),
(148, 'Manteiga', '0', 'Sim'),
(150, 'Massa esparguete', '1902.19.00', 'Sim'),
(151, 'Mel', '0409.00.00', 'Sim'),
(152, 'Óleo de algodão', '0', 'Sim'),
(153, 'Óleo de coco', '0', 'Sim'),
(154, 'Óleo de dendê  ', '0', 'Sim'),
(155, 'Óleo de milho', '0', 'Sim'),
(156, 'Óleo de palma', '1511.90.00', 'Sim'),
(157, 'Óleo de peixe', '0', 'Sim'),
(158, 'Óleo de girassol', '1512.19.00', 'Sim'),
(160, 'Outros tipos de carne', '0', 'Sim'),
(161, 'Peixe seco ', '0305.49.00', 'Sim'),
(162, 'Queijo', '0', 'Sim'),
(163, 'Refrigerantes', '2202.10.00', 'Sim'),
(164, 'Sal', '2501.00.10', 'Sim'),
(165, 'Detergente líquido', '3401.20.10', 'Sim'),
(166, 'Detergente líquido', '0', 'Sim'),
(167, 'Empresas de Transporte Mercadorias', '0', 'Sim'),
(168, 'Detergente sólido ', '3401.20.90', 'Sim'),
(169, 'Embalagem de vidro', '0', 'Sim'),
(170, 'Fertilizantes ', '3101.00.00', 'Sim'),
(171, 'Empresas de Transporte Passageiros', '0', 'Sim'),
(173, 'Lixivia ', '2829.90.00', 'Sim'),
(174, 'Medicamentos ', '0', 'Sim'),
(175, 'Plásticos ', '0', 'Sim'),
(176, 'Plásticos ', '0', 'Sim'),
(177, 'Sabão azul', '3401.20.90', 'Sim'),
(178, 'Vidro temperado ', '7007.19.00', 'Sim'),
(179, 'Embalagens de cartão', '0', 'Sim'),
(180, 'Guardanapos de papel', '4515.30.00', 'Sim'),
(181, 'Mobílias  ', '0', 'Sim'),
(182, 'Papel higiénico ', '0', 'Sim'),
(183, 'Pensos higiénicos', '9619.00.10', 'Sim'),
(184, 'Rolos de papel de cozinha', '4818.90.00', 'Sim'),
(185, 'Decorações ', '0', 'Sim'),
(186, 'Desportivo ', '0', 'Sim'),
(187, 'Moda', '0', 'Sim'),
(188, 'Uniformes ', '0', 'Sim'),
(189, 'Alfaias ', '0', 'Sim'),
(190, 'Alumínio   ', '0', 'Sim'),
(191, 'Barras de aço', '7214.99.00', 'Sim'),
(192, 'Materiais ', '0', 'Sim'),
(193, 'Utensílios domésticos ', '0', 'Sim'),
(194, 'Varão de aço', '7325.90.00', 'Sim'),
(195, 'Cimento cola', '2524.90.00', 'Sim'),
(197, 'Areias naturais', '2505.90.00', 'Sim'),
(198, 'Bauxite ', '2606.00.00', 'Sim'),
(199, 'Brita', '2517.10.00', 'Sim'),
(201, 'Burgal', '2517.10.00', 'Sim'),
(202, 'Calcário ', '2521.00.00', 'Sim'),
(203, 'Diamante', '7102.10.00', 'Sim'),
(204, 'Gesso natural', '2520.20.00', 'Sim'),
(206, 'Granito', '6802.23.00', 'Sim'),
(207, 'Mármore ', '6802.21.00', 'Sim'),
(208, 'Quartzo', '2506.10.00', 'Sim'),
(210, 'Gás natural', '2705.00.00', 'Sim'),
(211, 'Petróleo ', '2709.00.00', 'Sim'),
(212, 'Produção de gases industriais ', '0', 'Sim'),
(215, 'Arroz com casca', '1006.10.00', 'Sim'),
(216, 'Trigo de semente', '1001.11.00', 'Sim'),
(217, 'Beterraba', ' 0706.99.00', 'Sim'),
(218, 'Yame', '0714.30.30', 'Sim'),
(219, 'Feijão semente', '0713.33.00', 'Sim'),
(220, 'Girassol', '1206.00.00', 'Sim'),
(221, 'Algodão', '6102.20.00', 'Sim'),
(222, 'Ginguba semente', '12.02.30', 'Sim'),
(223, 'Soja semente', '1201.10.00', 'Sim'),
(224, 'Castanha de cajú', '0801.31.00', 'Sim'),
(225, 'Banana pão', '0803.10.00', 'Sim'),
(226, 'Fruta pinha', '0810.90.00', 'Sim'),
(227, 'Lima', '0805.50.00', 'Sim'),
(228, 'Garoupa', '0303.89.00', 'Sim'),
(229, 'Açucar', '1701.1390', 'Sim'),
(232, 'Massango', '1102.90.90', 'Sim'),
(233, 'Transitários Serviços', '00', 'Sim'),
(234, 'Transitários Serviços', '00', 'Sim'),
(235, 'Feijão Frade', '0713.35.00', 'Sim'),
(236, 'Abóbora ', '0709.9300', 'Sim'),
(237, 'Endívias', '0705.29.00', 'Sim'),
(240, 'Carne de porco', '0203.19.00', 'Sim'),
(241, 'Fraldas descartavéis', '1101.00.10', 'Sim'),
(242, 'Fuba de milho', '1102.20.90', 'Sim'),
(243, 'Óleo de amendoim', '1508.90.00', 'Sim'),
(244, 'Croeira', '0', 'Sim'),
(249, 'Produção de Sementes', '00', 'Sim'),
(256, 'Agência de Viagem', '00', 'Sim'),
(260, 'Casa de Chá', '00', 'Sim'),
(261, 'Centro Recreativo', '00', 'Sim'),
(262, 'Cervejaria', '00', 'Sim'),
(263, 'Dancing', '00', 'Sim'),
(264, 'Discoteca', '00', 'Sim'),
(265, 'Serviços de Catering', '00', 'Sim'),
(266, 'Geladaria', '00', 'Sim'),
(267, 'Lanchonete', '00', 'Sim'),
(268, 'Pastelaria', '00', 'Sim'),
(269, 'Restaurante', '00', 'Sim'),
(270, 'Snack Bar', '00', 'Sim'),
(271, 'Taverna', '00', 'Sim'),
(272, 'Guia Turístico', '00', 'Sim'),
(273, 'Empresas de Proteção e Vigilância', '00', 'Sim'),
(274, 'Agencias Seguradoras', '00', 'Sim'),
(275, 'Mediadoras de Seguro', '00', 'Sim'),
(276, 'Agencias Bancarias', '00', 'Sim'),
(277, 'Empresa Estatal de Água e Eletricidade', '00', 'Sim'),
(278, 'Empresa de Limpeza e Saneamento', '00', 'Sim'),
(279, 'Clínicas', '00', 'Sim'),
(280, 'Farmácias', '00', 'Sim'),
(281, 'Farmácias', '00', 'Sim'),
(282, 'Hospitais', '00', 'Sim'),
(285, 'Empresas de Telecomunicações', '00', 'Sim'),
(286, 'Empresas de Avião', '00', 'Sim'),
(289, 'Bar', '00', 'Sim'),
(290, 'Botequim', '00', 'Sim'),
(291, 'Café', '00', 'Sim'),
(292, 'Postos e Centro de Saúde', '00', 'Sim'),
(293, 'Bovinos', '00', 'Sim'),
(294, 'Ovinos', '00', 'Sim'),
(295, 'Caprinos', '00', 'Sim'),
(296, 'Suinos', '00', 'Sim'),
(297, 'Cavalos', '00', 'Sim'),
(298, 'Búfalos', '00', 'Sim'),
(299, 'Palmeira-Dendém', '08.90.33', 'Sim'),
(300, 'Semente de Algodão', '1207.21.00', 'Sim'),
(301, 'Ananás', '00', 'Sim'),
(302, 'Castanha de Cajú', '00', 'Sim'),
(305, 'Óleo de Algodão', '00', 'Sim'),
(307, 'Refrigerantes', '00', 'Sim'),
(309, 'Pensos Higiênicos', '00', 'Sim'),
(310, 'Decorações', '00', 'Sim'),
(311, 'Desportivo', '00', 'Sim'),
(312, 'Moda', '00', 'Sim'),
(313, 'Uniformes', '00', 'Sim'),
(316, 'Matérias de Construção', '00', 'Sim'),
(317, 'Sílica', '2811.22.00', 'Sim'),
(318, 'Carne de Bovino', '0201.30.00', 'Sim'),
(319, 'Carne de Búfalo', '00', 'Sim'),
(320, 'Carne de Cavalo', '0205.00.00', 'Sim'),
(321, 'Carne de Cabrito', '0204.50.00', 'Sim'),
(322, 'Carne de Ovino', '00', 'Sim'),
(323, 'Leite Fresco', '0402.91.10', 'Sim'),
(324, 'Leite ', '0401.10.10', 'Sim'),
(325, 'Clínquer', '00', 'Sim'),
(326, 'Ouro', '7108.11.00', 'Sim'),
(327, 'Carne de Frango', '00', 'Sim'),
(328, 'Carne Seca de Vaca', '00', 'Sim'),
(329, 'Cera', '000', 'Sim'),
(330, 'Farinha de Trigo', '000', 'Sim'),
(331, 'Óleo Vegetal', '0000', 'Sim'),
(335, 'N/A', '', ''),
(338, 'Maladax', '23', 'yes'),
(342, 'Sexta 13', '23', 'yes');

-- --------------------------------------------------------

--
-- Estrutura da tabela `production`
--

DROP TABLE IF EXISTS `production`;
CREATE TABLE IF NOT EXISTS `production` (
  `id` int NOT NULL AUTO_INCREMENT,
  `registration_date` date NOT NULL,
  `quantity` decimal(14,2) NOT NULL,
  `comment` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `available_area` decimal(14,2) NOT NULL,
  `action_type` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `exploration_type` varchar(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_province` int NOT NULL,
  `id_production_period` int NOT NULL,
  `id_productive_row` int NOT NULL,
  `id_productive_sub_row` int NOT NULL,
  `id_product` int NOT NULL,
  `id_sub_product` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_productive_row` (`id_productive_row`,`id_productive_sub_row`,`id_product`,`id_sub_product`),
  KEY `id_province` (`id_province`),
  KEY `id_campaign` (`id_production_period`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `production`
--

INSERT INTO `production` (`id`, `registration_date`, `quantity`, `comment`, `available_area`, `action_type`, `exploration_type`, `id_province`, `id_production_period`, `id_productive_row`, `id_productive_sub_row`, `id_product`, `id_sub_product`) VALUES
(1, '2023-09-20', '0.00', 'Uma inserção normal', '41197.00', 'Semear', 'EAE', 1, 1, 1, 1, 335, 1),
(2, '2023-09-20', '42871.00', 'Uma inserção normal', '39627.00', 'colher', 'EAE', 1, 1, 1, 1, 335, 1),
(3, '2023-09-20', '0.00', 'Uma inserção normal', '359931.00', 'Semear', 'EAE', 2, 1, 1, 1, 335, 1),
(4, '2023-09-20', '342014.00', 'Uma inserção normal', '354873.00', 'Colher', 'EAE', 2, 1, 1, 1, 335, 1),
(6, '2023-09-20', '0.00', 'Uma inserção normal', '423197.00', 'Semear', 'EAE', 3, 1, 1, 1, 335, 1),
(7, '2023-09-20', '500330.00', 'Uma inserção normal', '418256.00', 'Colher', 'EAE', 3, 1, 1, 1, 335, 1),
(8, '2023-09-20', '0.00', 'Uma inserção normal', '3845.00', 'Semear', 'EAE', 4, 1, 1, 1, 335, 1),
(9, '2023-09-20', '2048.00', 'Uma inserção normal', '3688.00', 'Colher', 'EAE', 4, 1, 1, 1, 335, 1),
(10, '2023-09-20', '0.00', 'Uma inserção normal', '41671.00', 'Semear', 'EAE', 1, 2, 1, 1, 335, 1),
(11, '2023-09-20', '44094.00', 'Uma inserção normal', '38883.00', 'Colher', 'EAE', 1, 2, 1, 1, 335, 1),
(12, '2023-09-20', '0.00', 'Uma inserção normal', '363342.00', 'Semear', 'EAE', 2, 2, 1, 1, 335, 1),
(13, '2023-09-20', '356742.00', 'Uma inserção normal', '354122.00', 'Colher', 'EAE', 2, 2, 1, 1, 335, 1),
(14, '2023-09-20', '0.00', 'Uma inserção normal', '425622.00', 'Semear', 'EAE', 3, 2, 1, 1, 335, 1),
(15, '2023-09-20', '520240.00', 'Uma inserção normal', '419226.00', 'Colher', 'EAE', 3, 2, 1, 1, 335, 1),
(16, '2023-09-20', '0.00', 'Uma inserção normal', '3850.00', 'Semear', 'EAE', 4, 2, 1, 1, 335, 1),
(17, '2023-09-20', '2113.00', 'Uma inserção normal', '3701.00', 'Colher', 'EAE', 4, 2, 1, 1, 335, 1),
(18, '2023-10-11', '1.00', 'Muito bom', '0.00', 'N/A', 'EAE', 12, 2, 10, 1, 335, 1),
(19, '2023-10-18', '2.00', 'Valeu', '0.00', 'N/A', 'EAF', 12, 1, 10, 1, 335, 1),
(20, '2023-10-19', '987655.00', 'ghjjytj', '0.00', 'N/A', 'EAF', 2, 4, 17, 1, 198, 1),
(22, '2023-10-11', '234.00', 'sdfghjk', '0.00', 'N/A', 'EAE', 2, 4, 17, 1, 198, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `production_period`
--

DROP TABLE IF EXISTS `production_period`;
CREATE TABLE IF NOT EXISTS `production_period` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(60) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `observation` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `production_period`
--

INSERT INTO `production_period` (`id`, `designation`, `start_date`, `end_date`, `observation`) VALUES
(1, '2020/2021', '2020-01-01 00:00:00', '2021-01-01 00:00:00', 'Campanha da agricultura'),
(2, '2021/2022', '2021-01-01 00:00:00', '2022-01-01 00:00:00', 'Campanha da agricultura'),
(3, '2020', '2020-01-01 00:00:00', '0000-00-00 00:00:00', 'Campanha da agricultura'),
(4, '2021', '2021-01-01 00:00:00', '0000-00-00 00:00:00', 'Campanha da agricultura');

-- --------------------------------------------------------

--
-- Estrutura da tabela `productive_row`
--

DROP TABLE IF EXISTS `productive_row`;
CREATE TABLE IF NOT EXISTS `productive_row` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(60) NOT NULL,
  `comment` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_type_production` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_type_production` (`id_type_production`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `productive_row`
--

INSERT INTO `productive_row` (`id`, `designation`, `comment`, `id_type_production`) VALUES
(1, 'Cereais', 'Fruto ou semente comestível da família das gramíneas que pode ser utilizado como alimento', 1),
(2, 'Cana de papa', 'Boa fileira', 1),
(3, 'Raízes e Tubérculos', 'Raízes e Tubérculos', 1),
(5, 'Leguminosas ', 'Leguminosas ', 1),
(6, 'Oleaginosa', 'Oleaginosa', 1),
(7, 'Especiarias', 'Especiarias', 1),
(8, 'Fruteiras', 'Fruteiras', 1),
(9, 'Hortícolas', 'Hortícolas. Muito saudável.', 1),
(10, 'Gado bovino', 'Gado bovino', 2),
(11, 'Gado suíno', 'Gado suíno', 2),
(12, 'Gado ovino', 'Gado ovino', 2),
(13, 'Gado caprino', 'Gado caprino', 2),
(14, 'Cavalos', 'Cavalos', 2),
(15, 'Búfalos', 'Búfalos', 2),
(17, 'Aves', 'Aves', 2),
(18, 'Fluvial', 'Fluivial', 3),
(19, 'Marítima', 'Marítima', 3),
(20, 'Frutos do mar', 'Frutos do mar', 3),
(21, 'Pesada', 'Pesada', 4),
(22, 'Transformadora', 'Transformadora', 4),
(23, 'Extractiva', 'Extractiva', 4),
(24, 'Têxtil', 'Têxtil', 4),
(25, 'Hotelaria', 'Hospedagem e Alimentação (Turismo)', 5),
(26, 'Protecção e Vigilância', 'Protecção e Vigilância', 5),
(27, 'Seguros', 'Seguros', 5),
(28, 'Bancos', 'Bancários', 5),
(29, 'Água e Electricidade', 'Água e Electricidade', 5),
(30, 'Ensino e Formação', 'Ensino e Formação', 5),
(32, 'Limpeza e Saneamento', 'Limpeza e Saneamento', 5),
(33, 'Restaurantes e Similares', 'Manutenção e Conservação', 5),
(34, 'Saúde', 'Saúde', 5),
(35, 'Telecomunicações', 'Telecomunicação', 5),
(36, 'Transportação', 'Transportação', 5),
(38, 'Sementes', '', 1),
(39, 'Agências de Viagens e Turismo', '', 5),
(42, 'Mingal', 'Boa Fileirahffffffffffffffffufufuurrufjfjfjffufuufudffyfufuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuweybntjuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuyiy7ik7iygfththbtnnnnnnnbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrfrjkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkiu', 1),
(43, 'Padal', 'Boa fileira', 1),
(44, 'Pandal', 'Boa fileira', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `productive_row_54`
--

DROP TABLE IF EXISTS `productive_row_54`;
CREATE TABLE IF NOT EXISTS `productive_row_54` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(80) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `productive_row_54`
--

INSERT INTO `productive_row_54` (`id`, `designation`, `description`) VALUES
(1, 'Cereais', 'Muito bom.'),
(9, 'Gado bovino', 'uijhgyuyh');

-- --------------------------------------------------------

--
-- Estrutura da tabela `productive_sub_row`
--

DROP TABLE IF EXISTS `productive_sub_row`;
CREATE TABLE IF NOT EXISTS `productive_sub_row` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(60) NOT NULL,
  `comment` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `productive_sub_row`
--

INSERT INTO `productive_sub_row` (`id`, `designation`, `comment`) VALUES
(1, 'N/A', 'N/A'),
(2, 'Metalúrgica', 'Metalúrgica'),
(3, 'Construção', 'Construção'),
(4, 'Alimentar', 'Alimentar'),
(5, 'Química', 'Química'),
(6, 'Celulose', 'Celulose'),
(7, 'Minerais Não-Metálicos', 'Minerais Não-Metálicos'),
(8, 'Confecções', 'Confecções'),
(9, 'Empreendimentos Hoteleiros', 'Empreendimentos Hoteleiros'),
(11, 'Ensino Pré-Escolar e Ensino Primário', 'Ensino Pré-Escolar e Ensino Primário'),
(12, 'Ensino Secundário', 'Ensino Secundário'),
(13, 'Ensino Superior', 'Ensino Superior'),
(14, 'Técnico-Professional', 'Técnico-Professional'),
(15, 'Aviação Civil', ''),
(16, 'Transportes Terrestres ', 'Transportes Terrestres '),
(19, 'Construção', ''),
(20, 'Mineral', ''),
(23, 'Sul', 'Boa fileira'),
(24, 'Cultivado', ''),
(26, 'Pandal', 'Boa fileira'),
(27, 'Malabal', 'Boa fileira');

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_5_4`
--

DROP TABLE IF EXISTS `product_5_4`;
CREATE TABLE IF NOT EXISTS `product_5_4` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(80) NOT NULL,
  `id_productive_row` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_productive_row` (`id_productive_row`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `product_5_4`
--

INSERT INTO `product_5_4` (`id`, `designation`, `id_productive_row`) VALUES
(1, 'O arroz é bom!', 9),
(3, 'Fuba', 1),
(4, 'Banana', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_balance`
--

DROP TABLE IF EXISTS `product_balance`;
CREATE TABLE IF NOT EXISTS `product_balance` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `product_balance`
--

INSERT INTO `product_balance` (`id`, `designation`, `comment`) VALUES
(4, 'Sardinha', 'dd'),
(5, 'Carapau', 'sd'),
(6, '', ''),
(7, 'Bovino', 'as'),
(8, 'Ovino', 'i'),
(9, 'Caprino', 'd'),
(10, 'Suíno', 'j'),
(11, 'Aves', 'f'),
(12, 'Milho', 'Milho'),
(13, 'Arroz', 'jh');

-- --------------------------------------------------------

--
-- Estrutura da tabela `profile`
--

DROP TABLE IF EXISTS `profile`;
CREATE TABLE IF NOT EXISTS `profile` (
  `id_user` int NOT NULL,
  `id_role` int NOT NULL,
  `observation` text NOT NULL,
  PRIMARY KEY (`id_user`,`id_role`),
  KEY `id_role` (`id_role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `province`
--

DROP TABLE IF EXISTS `province`;
CREATE TABLE IF NOT EXISTS `province` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(30) NOT NULL,
  `id_region` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_region` (`id_region`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `province`
--

INSERT INTO `province` (`id`, `designation`, `id_region`) VALUES
(1, 'Bengo', 1),
(2, 'Benguela', 3),
(3, 'Bié', 3),
(4, 'Cabinda', 1),
(5, 'Cuando-Cubango', 4),
(6, 'Cuanza Norte', 1),
(7, 'Cuanza Sul', 3),
(8, 'Cunene', 4),
(9, 'Huambo', 3),
(10, 'Huíla', 4),
(11, 'Luanda', 1),
(12, 'Lunda Norte', 1),
(13, 'Lunda Sul', 1),
(14, 'Malanje', 1),
(15, 'Moxico', 3),
(16, 'Namibe', 4),
(17, 'Uíge', 1),
(18, 'Zaire', 1),
(20, 'N/A', 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `region`
--

DROP TABLE IF EXISTS `region`;
CREATE TABLE IF NOT EXISTS `region` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `region`
--

INSERT INTO `region` (`id`, `designation`, `description`) VALUES
(1, 'Norte', 'Composta por sete províncias: Cabinda, Uíge, Zaire, Bengo Luanda, Cuanza Norte e Malanje. '),
(3, 'Centro', 'Composta por quatro províncias: Benguela, Cuanza Sul, Huambo e Bié.'),
(4, 'Sul', 'Composta por quatro províncias: Cuando Cubango, Huíla, Namibe e Cunene'),
(5, 'Leste', 'Composta por três províncias: Moxico, Lunda Sul, e Lunda Norte'),
(6, 'N/A', 'Nenhuma província');

-- --------------------------------------------------------

--
-- Estrutura da tabela `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `acronym` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `role`
--

INSERT INTO `role` (`id`, `name`, `acronym`, `designation`) VALUES
(1, 'MFD', 'sd', 'eerr'),
(2, 'jfd', 'jure', 'lku');

-- --------------------------------------------------------

--
-- Estrutura da tabela `service`
--

DROP TABLE IF EXISTS `service`;
CREATE TABLE IF NOT EXISTS `service` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(100) NOT NULL,
  `acronym` varchar(100) NOT NULL,
  `domain_name` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `service`
--

INSERT INTO `service` (`id`, `designation`, `acronym`, `domain_name`, `comment`) VALUES
(1, 'Serviço Normal', 'SN', 'www.pt.ao', 'Valeu bué'),
(8, 'Crazy service', 'Crazal', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `structure_national_production`
--

DROP TABLE IF EXISTS `structure_national_production`;
CREATE TABLE IF NOT EXISTS `structure_national_production` (
  `id_productive_row` int NOT NULL,
  `id_productive_sub_row` int NOT NULL,
  `id_product` int NOT NULL,
  `id_sub_product` int NOT NULL,
  `id_unit_measure` int NOT NULL,
  PRIMARY KEY (`id_productive_row`,`id_productive_sub_row`,`id_product`,`id_sub_product`),
  KEY `id_product` (`id_product`),
  KEY `id_productive_sub_row` (`id_productive_sub_row`),
  KEY `id_sub_product` (`id_sub_product`),
  KEY `id_unit_measure` (`id_unit_measure`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `structure_national_production`
--

INSERT INTO `structure_national_production` (`id_productive_row`, `id_productive_sub_row`, `id_product`, `id_sub_product`, `id_unit_measure`) VALUES
(1, 1, 335, 1, 3),
(10, 1, 335, 1, 3),
(17, 1, 198, 1, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `sub_product`
--

DROP TABLE IF EXISTS `sub_product`;
CREATE TABLE IF NOT EXISTS `sub_product` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(60) NOT NULL,
  `customs_tariff` varchar(30) NOT NULL,
  `prodesi_product` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `sub_product`
--

INSERT INTO `sub_product` (`id`, `designation`, `customs_tariff`, `prodesi_product`) VALUES
(1, 'N/A', '', 'não'),
(2, 'Verão de aço', '', 'sim'),
(3, 'Barras de aço', '', 'sim'),
(4, 'Utensílios doméstico', '', 'sim'),
(7, 'Materiais de construção', '', 'sim'),
(8, 'Alumínio', '', 'sim'),
(9, 'Clínquer', '', 'sim'),
(10, 'Cimento', '', 'sim'),
(11, 'Tintas para construçao', '', 'sim'),
(12, 'Gesso', '', 'sim'),
(13, 'Óleo de Amendoim', '', 'sim'),
(14, 'Óleo Vegetal', '', 'sim'),
(15, 'Óleo de coco', '', 'sim'),
(16, 'Óleo de milho', '', 'sim'),
(17, 'Óleo de algodão', '', 'sim'),
(18, 'Óleo de dendem', '', 'sim'),
(19, 'Óleo de girassol ', '', 'sim'),
(20, 'Óleo de peixe', '', 'sim'),
(21, 'Açucar-de-cana', '', 'sim'),
(22, 'Peixe seco', '', 'sim'),
(23, 'Carne seca de vaca', '', 'sim'),
(24, 'Farinha de trigo', '', 'sim'),
(25, 'Fuba de milho', '', 'sim'),
(26, 'Fuba de bombó', '', 'sim'),
(27, 'Iorgurte', '', 'sim'),
(28, 'Manteiga', '', 'sim'),
(29, 'Queijo', '', 'sim'),
(30, 'Mel', '', 'sim'),
(31, 'Cera', '', 'sim'),
(32, 'Água de mesa', '', 'sim'),
(33, 'Frutas', '', 'sim'),
(34, 'Refrigerantes', '', 'sim'),
(35, 'Cerveja', '', 'sim'),
(36, 'Lixívia', '', 'sim'),
(37, 'Detergente líquido', '', 'sim'),
(38, 'Detergente sólido', '', 'sim'),
(39, 'Fertilizantes', '', 'sim'),
(40, 'Plásticos', '', 'sim'),
(41, 'Embalagens de vidro', '', 'sim'),
(42, 'Vidro temperado', '', 'sim'),
(43, 'Medicamentos', '', 'sim'),
(44, 'Pensos higiénico', '', 'sim'),
(45, 'Fraldas descartáveis', '', 'sim'),
(46, 'Papel higiênico', '', 'sim'),
(47, 'Guardanapos de papel', '', 'sim'),
(48, 'Rolos de papel de cozinha', '', 'sim'),
(49, 'Embalagens de cartão', '', 'sim'),
(50, 'Mobília ', '', 'sim'),
(51, 'Granito', '', 'sim'),
(52, 'Diamante', '', 'sim'),
(53, 'Mármore', '', 'sim'),
(54, 'Brita', '', 'sim'),
(55, 'Burgau', '', 'sim'),
(56, 'Petróleo', '', 'sim'),
(57, 'Gás natural', '', 'sim'),
(58, 'Moda', '', 'sim'),
(59, 'Desportivo', '', 'sim'),
(60, 'Uniformes', '', 'sim'),
(61, 'Sapatos', '', 'sim'),
(62, 'Decorações', '', 'sim'),
(66, 'Cabax01', '23', 'yes'),
(68, 'Pambalax', '23', 'yes');

-- --------------------------------------------------------

--
-- Estrutura da tabela `trade_balance`
--

DROP TABLE IF EXISTS `trade_balance`;
CREATE TABLE IF NOT EXISTS `trade_balance` (
  `id` int NOT NULL AUTO_INCREMENT,
  `year` int NOT NULL,
  `value` decimal(10,0) NOT NULL,
  `quant` decimal(10,0) NOT NULL,
  `acronym` varchar(80) NOT NULL,
  `trade_balance_type` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `id_product_balance` int NOT NULL,
  `id_trade_balance_category` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `trade_balance_ibfk_2` (`id_trade_balance_category`),
  KEY `id_product_balance` (`id_product_balance`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `trade_balance`
--

INSERT INTO `trade_balance` (`id`, `year`, `value`, `quant`, `acronym`, `trade_balance_type`, `id_product_balance`, `id_trade_balance_category`) VALUES
(3, 2022, '505', '250', 'CIF', 'Importação', 4, 11),
(4, 2023, '131', '58', 'CIF', 'Importação', 4, 11),
(5, 2022, '505', '250', 'CIF', 'Importação', 5, 11),
(6, 2023, '2', '2', 'CIF', 'Importaçã', 5, 11),
(7, 2022, '48608', '17894', 'CIF', 'Importação', 7, 12),
(8, 2023, '55', '20', 'CIF', 'Importação', 7, 12),
(9, 2022, '382', '60', 'CIF', 'Importação', 9, 12),
(10, 2023, '122', '31', 'CIF', 'Importação', 9, 12),
(12, 2022, '34', '7', 'FOB', 'Exportação', 4, 11),
(14, 2023, '2000', '8', 'FOB', 'Exportação', 4, 11);

-- --------------------------------------------------------

--
-- Estrutura da tabela `trade_balance_category`
--

DROP TABLE IF EXISTS `trade_balance_category`;
CREATE TABLE IF NOT EXISTS `trade_balance_category` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `trade_balance_category`
--

INSERT INTO `trade_balance_category` (`id`, `designation`, `comment`) VALUES
(11, 'Aquicultura', 'jj'),
(12, 'Pecuária', 'f'),
(13, 'Cereais', 'd'),
(14, 'Frutos', 's'),
(15, 'Leguminosa', 'as'),
(16, 'Diversos', 'j');

-- --------------------------------------------------------

--
-- Estrutura da tabela `training`
--

DROP TABLE IF EXISTS `training`;
CREATE TABLE IF NOT EXISTS `training` (
  `id` int NOT NULL AUTO_INCREMENT,
  `quant_male` int NOT NULL,
  `quant_female` int NOT NULL,
  `quant_formation` int NOT NULL,
  `id_training_initiative` int NOT NULL,
  `id_province` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_training_initiative` (`id_training_initiative`),
  KEY `id_province` (`id_province`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `training`
--

INSERT INTO `training` (`id`, `quant_male`, `quant_female`, `quant_formation`, `id_training_initiative`, `id_province`) VALUES
(2, 32, 14, 0, 4, 1),
(3, 11, 50, 0, 5, 1),
(4, 1077, 728, 0, 6, 1),
(5, 87, 14, 0, 7, 1),
(6, 740, 1941, 0, 8, 1),
(7, 8, 327, 0, 9, 1),
(8, 2594, 2388, 0, 10, 1),
(9, 236, 179, 0, 11, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `training_initiative`
--

DROP TABLE IF EXISTS `training_initiative`;
CREATE TABLE IF NOT EXISTS `training_initiative` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(80) NOT NULL,
  `comment` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `training_initiative`
--

INSERT INTO `training_initiative` (`id`, `designation`, `comment`) VALUES
(4, 'AgroPRODESI (MyCoop)​', ''),
(5, 'AgriPREI​​', ''),
(6, 'Mais Cidadania (IAJ)​​', ''),
(7, 'Empreendedorismo​​', ''),
(8, 'Cooperativismo (FMEA)​​', ''),
(9, 'Empreendedorismo e Gestão de empresas, Empreendedorismo e Cooperativismo, Mais C', ''),
(10, 'Envolver​​​', ''),
(11, 'Gestão de Cooperativas (MYCOOP) e Agriprei​', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `type_production`
--

DROP TABLE IF EXISTS `type_production`;
CREATE TABLE IF NOT EXISTS `type_production` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(60) NOT NULL,
  `comment` varchar(120) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `type_production`
--

INSERT INTO `type_production` (`id`, `designation`, `comment`) VALUES
(1, 'Vegetal', 'Produções e manejo de culturas'),
(2, 'Animal', 'Incrementar a produção com o aprimoramento da nutrição'),
(3, 'Pesca', 'Extração de organismos no ambiente aquático'),
(4, 'Indústria', 'Transformar matéria-prima em produtos comercializáveis'),
(5, 'Serviço', 'Actividade que satisfaz a uma necessidade, sem assumir a forma de um bem material'),
(7, 'Florestal', 'Boa produção');

-- --------------------------------------------------------

--
-- Estrutura da tabela `unit_measure`
--

DROP TABLE IF EXISTS `unit_measure`;
CREATE TABLE IF NOT EXISTS `unit_measure` (
  `id` int NOT NULL AUTO_INCREMENT,
  `designation` varchar(30) NOT NULL,
  `symbol` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `unit_measure`
--

INSERT INTO `unit_measure` (`id`, `designation`, `symbol`) VALUES
(1, 'Kilograma', 'Kg'),
(2, 'Litro', 'L'),
(3, 'Tonelada', 'Ton(s)'),
(4, 'Unidade', 'Un'),
(7, 'Mil Un', 'Mil Un'),
(8, 'Metro cúbico', 'm³'),
(9, 'Qlts', 'Qlts'),
(10, 'Praida', 'nm'),
(14, 'Hectares', 'ha'),
(15, 'Metro quadrados', 'm³');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` text NOT NULL,
  `telephone` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `telephone`) VALUES
(1, 'Moisés Laurindo', 'moises@gmail.com', '12345678', 932204920),
(9, 'José Eduardo dos Santos', 'manacial@gmail.com', '$2y$10$JlCOAY6UY7q7scHCppehw.l/MrXqACnGxrWyY0Y6.sASCBW73iS.y', 932204920),
(15, 'Adminin', 'admin@gmail.com', '$2y$10$q6K7S5QOooy9842orUjh2eSf73y5GYpPHEl6Cv21c54ZfP7Gt8xDW', 932204920),
(24, 'manule', 'cmhgg@gmail.com', '$2y$10$y0ZxuvVlMLlWAXGWgJaR8uqrYXowcM3SWteGVIQbQ7gIpQeN6d0GO', 932204920);

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_connected`
--

DROP TABLE IF EXISTS `user_connected`;
CREATE TABLE IF NOT EXISTS `user_connected` (
  `id` int NOT NULL AUTO_INCREMENT,
  `token_created` text NOT NULL,
  `date_created` datetime NOT NULL,
  `id_user` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=913 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `user_connected`
--

INSERT INTO `user_connected` (`id`, `token_created`, `date_created`, `id_user`) VALUES
(570, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1MDU2MDQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.LbPCQ1ekVargMNenrdtd3xR9F7OmTmQ9N9/xIQATgqM=', '2023-10-05 00:00:00', 24),
(571, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1MDk2MzEsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.G+h0Z/Yj+xunztK3rW8vfYp1DvZ83ctm3S64RcUXke4=', '2023-10-05 00:00:00', 15),
(572, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1MDk2NDEsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.0CbrdnYmw5ytQN6qMr9gRa1egzdk6xlvZTSPUHEKLD0=', '2023-10-05 00:00:00', 15),
(573, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1MTAwNjYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.9/gBpZQe0WopsF8FUYx8dsDAKH526nviVmcMDnm+DSA=', '2023-10-05 00:00:00', 24),
(574, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1MTM2NDksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.8L7ZniVBs7jDwOsxZhPJS3darQC/7Fo5CoHdF11LYvY=', '2023-10-05 00:00:00', 24),
(575, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1Nzg0NDksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.KqIB8ceXK1eYT7LrPs3U0iS+HG/ETa1CsD5hu/y5B9g=', '2023-10-06 00:00:00', 24),
(576, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1Nzg0NTAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.MuLS+uvC7j2U/3Vbyh4wwcwP7D8/2p3JV6RoLaAdjxY=', '2023-10-06 00:00:00', 24),
(577, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1Nzg0NTEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.vSnujxUixaXk0giGF2qsnwKk/e64GA2UGH1gycuqoTo=', '2023-10-06 00:00:00', 24),
(578, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1Nzk2MTcsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.SqP0TZAZtGhS64TlORKxwlXhwSyBoiZdxkw7ahvYrvo=', '2023-10-06 00:00:00', 15),
(579, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1Nzk2MjEsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.4abOvcWJtwEIaOVVUDRkLWJGghoZ/rxsbgEEM6tl1o0=', '2023-10-06 00:00:00', 15),
(580, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1Nzk2MjIsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.pO65dgbXvHF1LXg99ylJ4rg4UmcPWlnBHZIwF6+KfYE=', '2023-10-06 00:00:00', 15),
(581, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1Nzk2MjMsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.6Cg8vFHB87DfJtnvwm/hqbCEOXuaGgFKHV2mO9NOetg=', '2023-10-06 00:00:00', 15),
(582, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1Nzk2MjQsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.EebBi7MboN/izOtizzRfm/soKnqXPQdokdKRRVl2cCs=', '2023-10-06 00:00:00', 15),
(583, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1ODAwNzEsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.p6GuMntGhT6/M04xsdsaXViDytY73X7WvQkqH+wwNjQ=', '2023-10-06 00:00:00', 15),
(584, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1ODIwMTksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.CXe25blYItmFQlwbDURgHd/nThxK5/WVN8UbTwEOWk0=', '2023-10-06 00:00:00', 24),
(585, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1ODIwMjEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==./Kxal/J9sLQP4ESxALg0breVdTS956dSxM+dKLFDO7c=', '2023-10-06 00:00:00', 24),
(586, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1ODIwMjIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.1adQsAacfgFUIBMWujfCNPWNKZePdv+FXxopQ4kToTE=', '2023-10-06 00:00:00', 24),
(587, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTY1ODIwMjMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.pL6nz361B8cjYe1+Z+vyQmz8krOD0X8oL5htp8HumCk=', '2023-10-06 00:00:00', 24),
(588, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcwMTg2NzYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.pHLS3Cj+lL0op88hNF6FbBftrTPqqerBuxurHOxZlHc=', '2023-10-11 00:00:00', 24),
(589, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcwMTg2NzgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.Ltfhz/MwwXOB2r7Vi+N3ZbQhJ14icztDp+br7yE1hBc=', '2023-10-11 00:00:00', 24),
(590, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcwMTg2NzksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.x41t8W/YRS7x9hRhRihH7KwlI81MsuLAU+BUiYoFxaU=', '2023-10-11 00:00:00', 24),
(591, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcwMTg2ODAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.T9I7cgQLA/VCX8ve0UtGHTtKd7OTdZBkQY+51vINC/0=', '2023-10-11 00:00:00', 24),
(592, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcwMTg2ODEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.Sd7TFqFuhZV8t/jSaQxIE7DPdYpHqjYshSOOOwYEQp0=', '2023-10-11 00:00:00', 24),
(593, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcwMTk5NDMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.aUuedzjvRdCu3QF0xgxksONaW7K2pVali5Tlhy/zRTI=', '2023-10-11 00:00:00', 24),
(594, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcwMTk5NDUsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.DGc+3zksqEzgbNCZAF5fO5dzEca47R7eSRTihzESeos=', '2023-10-11 00:00:00', 24),
(595, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcwMTk5NDYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.vKjRpJNNbSu43+ZmKgHSS68zbhFFByrTy0A20p+rNSk=', '2023-10-11 00:00:00', 24),
(596, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcwMTk5NDcsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.8FDeSVbrs2Pq1p3jsrKoy95ymldAydraLi7ins8ODmU=', '2023-10-11 00:00:00', 24),
(597, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcwMTk5NDgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.+A8hs/50/mbS6XhDX6WQInw9KJPQHTzr0C+Jajv/qq0=', '2023-10-11 00:00:00', 24),
(598, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcwMjM3NjMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.OC76cpMRODk6bCQcs+ELQsNm+unyM8CCDXOY1OZkH60=', '2023-10-11 00:00:00', 24),
(599, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcwMjc0MTYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.E3G+4wIVqahjRdPQPKLtBNacuFHBZUcwbCEStsa39yc=', '2023-10-11 00:00:00', 24),
(600, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcwMjc0MTcsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.p70fYrpFp5RkRmsx0Mr7VElyJkjeVQmY3KmDoyhJSHM=', '2023-10-11 00:00:00', 24),
(601, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMDA1MjUsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.kUV79YUUc2zgG72Ms2u4+TjubzOyPG07fRuRy6xpj1k=', '2023-10-12 00:00:00', 15),
(602, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMDA1MjcsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.FqxfIpSaxFL0LGjHoLfKGd+CAtgmT7LB171eRsNAvSw=', '2023-10-12 00:00:00', 15),
(603, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMDA5OTcsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.ENMix2B75yoHArB9li7vj1v3VZ3Au5OmQLPSmTu9OXA=', '2023-10-12 00:00:00', 15),
(604, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMDEwNzksInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.SfubcZmotd6X5rz+gbqF9SXIKY8nhhEICjVt+cv3UzY=', '2023-10-12 00:00:00', 15),
(605, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMDExMjUsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.NJC2BT+sk5tVFj0w9K8YB7xtGVpno2ScuJmKnGF1R04=', '2023-10-12 00:00:00', 15),
(606, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMDExNDgsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.wKk2ajrs5TaE1tOcqgcGu7K06T0F53HlT2feKQ0pbOE=', '2023-10-12 00:00:00', 15),
(607, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMDExNzksInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.jQ3ZTiCBrBYHNhJgHkYYbelQPuQXLzSa6I7chgLnkcw=', '2023-10-12 00:00:00', 15),
(608, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMDEyNDMsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.fgVbtE2JMIKk+j1RT4VTpMpxawXTFXJ3dH0fd1ftwJ0=', '2023-10-12 00:00:00', 15),
(609, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMDEyOTcsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.bdBF5Toouo2Y8NF7MiPL6kC7HkH/1c3cBFnuOY4AzZg=', '2023-10-12 00:00:00', 15),
(610, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMDE0NTksInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.9pXfkHI18JDza2gC0F8rQNAJB2j8m/THWLvgTJbCnRc=', '2023-10-12 00:00:00', 15),
(611, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMDE0NjAsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.497kVZFRJhkNCQ+5Q9SyXgk0AlDqILlRBlAlPERWUAI=', '2023-10-12 00:00:00', 15),
(612, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMDE5MDMsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.sJWn2eSgfDD38K5D2l6Q5SfPj2x/ex0yXz5ZCx/ULyY=', '2023-10-12 00:00:00', 15),
(613, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMDE5MDQsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.UyWwCRXRyVCg/i0tVrOKXofb0u/TJZnnzC3pGPNxJAE=', '2023-10-12 00:00:00', 15),
(614, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMDQ1NjEsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.5g9QTEpOoJYQzQ+lBJhP3FvhGUax2EgllzzwhiD70H0=', '2023-10-12 00:00:00', 15),
(615, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMDQ1NjIsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.+ixGsdt2PA9lMAvlbh7SsRjK3wF2RTJWGdBwjQ2dY80=', '2023-10-12 00:00:00', 15),
(616, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMDQ1NjMsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.xTzCMdBLXbdB2RYY9DCx8mooWu7if2XrHPZ1CZwWoa4=', '2023-10-12 00:00:00', 15),
(617, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMDQ1NjQsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.s8FeLTRisywnwK9VzfunD4rTHxLmxXwUREjNWtpPWfM=', '2023-10-12 00:00:00', 15),
(618, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMDQ1NjUsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.sl6Ld20d1LB5GQTpDsOXX5wOOA+5ESQM858YmnzpMfE=', '2023-10-12 00:00:00', 15),
(619, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMDY0ODYsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.J57Xw3I8EdY7EKH/ZqX38tBOe+E1gHhs+HRGvCocP7A=', '2023-10-12 00:00:00', 15),
(620, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMDY0ODcsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.BPA3iHLoYOjJQO9nGJkwlPSLbVCYvvVtSTKQezmbzkA=', '2023-10-12 00:00:00', 15),
(621, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMDY0ODgsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.oOK12zqAH79ap3yn8vvzaPiMk9wA+X4sPoQry8mxdCs=', '2023-10-12 00:00:00', 15),
(622, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMTUzOTYsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.PXlfM5c5dGHj/pPc1tOnlyOiI6lcC2y/BdIo0gCyZFI=', '2023-10-12 00:00:00', 15),
(623, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMTUzOTksInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.sdwfwy5BRSFMfixTOzPsZj6b/YZf7LmhYgmJpV8VDLY=', '2023-10-12 00:00:00', 15),
(624, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMTU2NzksInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.45x1zA5scN1h/WG+CpHRcQp+hOUVf7dHckDosJVyn1g=', '2023-10-12 00:00:00', 15),
(625, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMTU2ODAsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.8VCsOsQHdSRVPfNJ6yGhHuupFlmL/4txZPM8Lru1ABg=', '2023-10-12 00:00:00', 15),
(626, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMTcyMTMsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.ev9GmL4BH1hZec/rWEPzyjOkD89frll1kqJQFkYcCpU=', '2023-10-12 00:00:00', 15),
(627, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMTcyMTQsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.pz4JBuBgQM/+k4NRRJ/2w1ttWuvFFF5pytvmCOSUsG4=', '2023-10-12 00:00:00', 15),
(628, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMTczOTUsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.oJvku/8XQwOURray8CPmfU3f7xzPiS3ZV4rUCSd0/Aw=', '2023-10-12 00:00:00', 15),
(629, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMTczOTYsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.m3jZRf+zc7OX6quiRHIlHBR2N3mznpyzfYuucJ8//5A=', '2023-10-12 00:00:00', 15),
(630, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMTc1MjEsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.1XyUhTYlwlBa0dfuG+qlaCNb4JObT+5w4vhXrxjqmSA=', '2023-10-12 00:00:00', 15),
(631, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMTc1NjgsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.qFSSNFJL3VT25SEO7oDYjNw4iJ0NBIPKIFUYVexL9B0=', '2023-10-12 00:00:00', 15),
(632, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMTg5MzksInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.Pe34dlOuQczQVWj8qbDIoi407du712PEDehbZw2XeXs=', '2023-10-12 00:00:00', 15),
(633, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMTg5NDAsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.f0PeesypaPiUVVJ5lyz8+c7rRybAXdGrZwMfYwBtPyw=', '2023-10-12 00:00:00', 15),
(634, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMTg5NDEsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.mUZ2oW6iNVxyNquzt165F/osigbGJgbfuXOG3eWtrjk=', '2023-10-12 00:00:00', 15),
(635, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMTg5NDIsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.Q85yQ0613U/mZOfEjnN+MyihPkHRPO1ZrjktLpi9Wag=', '2023-10-12 00:00:00', 15),
(636, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxMTg5NDMsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.xgYJwEfnJaw/pEZx3CXHOhP/uhrS4bTm5khWHmnJaiA=', '2023-10-12 00:00:00', 15),
(637, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxODIyMzIsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.6icPAuMn6oyK/aEOe/nZL+J1sqiWMZymF0fVAq17kXQ=', '2023-10-13 00:00:00', 15),
(638, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxODIyMzMsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.6CuDam1f8neNC5TrFupowGMC6LFAlXhhD4x1BP2TLtg=', '2023-10-13 00:00:00', 15),
(639, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxODIyMzQsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.H3bmcHrlXrrNCew4mfEcSNnk5i3+++eAMva/sBgMFao=', '2023-10-13 00:00:00', 15),
(640, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxODIyMzUsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.T7jg8Zh30jE2FwSMV/27O0o3Wrn+YbzKtjcHZxjn5dE=', '2023-10-13 00:00:00', 15),
(641, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxODIyMzYsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.5RLjei4ckGM1PnkKIeYAQBIuRbmVv87128pXEkHONQM=', '2023-10-13 00:00:00', 15),
(642, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxODIyMzcsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.it0itu12I3xhMhOpVpG+NUT6XgCCpaIsUmUQkeQhkUE=', '2023-10-13 00:00:00', 15),
(643, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxODIyMzgsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.DlulMb7wfgWa70Ww57E2yzJI0wKBSOg4rGcM38IoYR4=', '2023-10-13 00:00:00', 15),
(644, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxODI0MDQsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.faIB+EA5uV/zEvr+KDBv0yTX7LztPbpk6irwQs14mwg=', '2023-10-13 00:00:00', 15),
(645, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxODI0MDUsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.3P2yVjeoXa4dLyRH26NIg5h6TOJ2pZfxGFGyh8sARaQ=', '2023-10-13 00:00:00', 15),
(646, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxODQzODcsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.xoS+iOJlzKFCrxCVX9nwG+tmlkDWviYjS47uibCswRQ=', '2023-10-13 00:00:00', 15),
(647, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxODY3MTcsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.9vUOz252U6qB/D+AiANHN0Tbkv+KfiSfh/ZEANjuvWc=', '2023-10-13 00:00:00', 15),
(648, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxODY3MTcsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.9vUOz252U6qB/D+AiANHN0Tbkv+KfiSfh/ZEANjuvWc=', '2023-10-13 00:00:00', 15),
(649, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxODY3MTgsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.iD9SIbPR+fy6o3NdwilWLgf/Q7wipeN5zrDUs9DrU28=', '2023-10-13 00:00:00', 15),
(650, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxODY3MTksInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.NPHh4vBXvh3wiiIYY5AK3yZOETGsPkuQiAp2g1k8a10=', '2023-10-13 00:00:00', 15),
(651, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxODcyOTcsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.Vjcx5xtfaYUlTscxjY+c3UeerG7OGBYHeJhfLa0KNLI=', '2023-10-13 00:00:00', 15),
(652, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxODc5MDMsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.qKI2Cd7PfIJeIv7sz65h37Hc435yLMCDhVy+1Bp0ll4=', '2023-10-13 00:00:00', 15),
(653, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxODc5MDQsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.DURnkgMb+TJKzJWLlQPKFIVDluNmLSxtA85FLvpdSKo=', '2023-10-13 00:00:00', 15),
(654, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxODgxMDgsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.Tw5HdlIccL4MmTDXF2A4ESF+6svluF46DI3vPohAS4Y=', '2023-10-13 00:00:00', 15),
(655, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxODg0NDcsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.QeNdFNdKKAIiiZhpV0xZG+pAZZb+/7S9oTnkq0QMR98=', '2023-10-13 00:00:00', 15),
(656, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxOTk1NzIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.Dfgs766bDwi4cLc3yuRNEx0Z6oKTB8j0H0ZSbGzC7kY=', '2023-10-13 00:00:00', 24),
(657, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcxOTk1NzMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.w0qrw19OxMnKW8/kNW8BOrs+UGF2MtctqANUaHT6+LQ=', '2023-10-13 00:00:00', 24),
(658, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTcyMDAzNjYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.PAj+7r0lFSl6WCr29NedImXIsLxB1TAqLsdVBw22QBU=', '2023-10-13 00:00:00', 24),
(659, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc0NTM4NzksInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.Scr7/NP5UcOq+g9pe8tivVUrWUFseh1hlHVKjhaZkI8=', '2023-10-16 00:00:00', 15),
(660, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc0NTM4ODIsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.m+/jXR7XKvZ/7RYX3txVtcljg1eNWpgVTZeZyOIZYjo=', '2023-10-16 00:00:00', 15),
(661, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc0NTM4OTMsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.z8JsQqpdKzapEZGB1sezuFpJZxKcJGcDAMaCVMFlqmw=', '2023-10-16 00:00:00', 15),
(662, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc0NTM4OTQsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.dk2KW6Nj63LmmH7lb1H6fxqaXyBWvVAwWJCnHOp3xfw=', '2023-10-16 00:00:00', 15),
(663, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc0NTM4OTUsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.l3Uf+thnXXZZDQh5VljJ4WWEHHf4g1DeLMuKUa3FIAs=', '2023-10-16 00:00:00', 15),
(664, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc0NTM4OTUsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.l3Uf+thnXXZZDQh5VljJ4WWEHHf4g1DeLMuKUa3FIAs=', '2023-10-16 00:00:00', 15),
(665, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc0NTM4OTYsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.sj+UCPsUoEv6h0BsJsK4omXaxGP8LsYDGfHa0Pkmz9k=', '2023-10-16 00:00:00', 15),
(666, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc0NTM4OTcsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.wk/5607JdP1Kjh5WDVMACdPnaIUVmxVgc8BXsunoxvY=', '2023-10-16 00:00:00', 15),
(667, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc0NTM4OTgsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.qrtgIsaeMYwW3SZIYVBbHpJgtYEo9qv9I48VMIMVVXA=', '2023-10-16 00:00:00', 15),
(668, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc0NTM4OTksInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.f5GNQdTOUeAY6t3yqY2pS5m9CNUMZOLagOuIMt0SwrQ=', '2023-10-16 00:00:00', 15),
(669, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc0NTM5MDAsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.FR4v/HVkuWa8nzqLRk75+oP134d3O8xqri8aa6s4lPc=', '2023-10-16 00:00:00', 15),
(670, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc0NTM5MDEsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.dl6+CQkVj6Vxo7j0gv3Lz/i2C9sBq/doh3j1krg4OM0=', '2023-10-16 00:00:00', 15),
(671, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc0NTM5MDIsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.wRNq8Khj7HKhM+P3uPocXMvvKpeG16xNAxY7EMbpsIM=', '2023-10-16 00:00:00', 15),
(672, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc0NTM5MDMsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.gRP4XaxhXqnPXCsO2cTVKICvPBwmR5i0VNHlYlW/Ai4=', '2023-10-16 00:00:00', 15),
(673, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc0NTQ1NjAsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.AW1Mf94ZYJa6rzqLOBS24EoeYgQqhC+N920FEUuYRfA=', '2023-10-16 00:00:00', 15),
(674, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc0NTQ1NjMsInVpZCI6IjE1IiwiZW1haWwiOiJhZG1pbkBnbWFpbC5jb20ifQ==.0JqNpznFtQp6K03O4822cAFy343Mvb85H2RrFomXAMM=', '2023-10-16 00:00:00', 15),
(675, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc3MTEzOTUsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.jEne9C167nWI74t+puHi0qf4OmMijGmAY/nEWlU7Dfs=', '2023-10-19 00:00:00', 24),
(676, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc3MTEzOTYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.k8Ys3W0IQXvEN2P7F63EshofK8AcH2GvtZd0XC6RVIA=', '2023-10-19 00:00:00', 24),
(677, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc3OTE3MzgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.HF/0qmIPuD841CSZELJiAOQ0c7ueytyCUUPKDkCbH+E=', '2023-10-20 00:00:00', 24),
(678, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc3OTI5MjQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==./TfKFyCXrpdIJK3BV+C3vj5r1vrSYzSvdnR6xUna1rg=', '2023-10-20 00:00:00', 24),
(679, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc3OTczNTgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.OOHz9cFxxoTVPl4NqgeAtLtd3WodvH5jei3I5D94RbA=', '2023-10-20 00:00:00', 24),
(680, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc3OTczNTksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.bDCkvL5au0Pq1sBrfinRhLXAfZfcRGcuJ8iayKlpzxk=', '2023-10-20 00:00:00', 24),
(681, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTc3OTczNjAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.4tlHikrOikcXd+856s2RV9eFYyu+ibMzzQ9EBjFHx+U=', '2023-10-20 00:00:00', 24),
(682, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTgwNTEzNDEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.nnbkieZC/UcDV53G817Ib+PUKrWUmm+CfgHJuorv+y0=', '2023-10-23 00:00:00', 24),
(683, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTgwNTEzNzMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.eGthFPYXlDLA6qPamAVpLpYR7GwRdKeu0wq0ijdMoIo=', '2023-10-23 00:00:00', 24),
(684, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTgwNTEzODMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.AhUpuZPWbl65Be/vQR6d6X4IkhE3z1/QsjqBYYxArd0=', '2023-10-23 00:00:00', 24),
(685, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTgwNTEzOTEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.ItP7n8HpyiE83EhVevuR//3jC+IAHdKkFq8VH7RWUD0=', '2023-10-23 00:00:00', 24),
(686, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTgwNTEzOTIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.xuFzllsWtKZqcOVhpY+8wqEUo36T34CRAK2usQDeKm8=', '2023-10-23 00:00:00', 24),
(687, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTgwNTE3NzksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.stTkSs6E3gZPRZiGxvLAniw02XgfmoCpg/mXs324MUY=', '2023-10-23 00:00:00', 24),
(688, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTgwNTE3OTIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.br7zc0PPeKuTSwUJWgkP/oPjJjtz+QjB8/M8uc7sHWc=', '2023-10-23 00:00:00', 24),
(689, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTgzOTExOTcsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.ddujQROBZxj1DFFQrNSHtopuI0jfqlk5zjPuvHlIVQw=', '2023-10-27 00:00:00', 24),
(690, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTgzOTMyNDcsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.um1dswokkJY5EHkZ6lfrnfSNhwuZJ6Dbmi5tVkweHw0=', '2023-10-27 00:00:00', 24),
(691, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTgzOTMyNDgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.vipAfkVSS5BnwnKrlvJlQVj0/v7lfDFKHnb9Vqp1JBQ=', '2023-10-27 00:00:00', 24),
(692, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTgzOTMyNDksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.S2O5Zmt/BNxRhWrN6Jg/wHZtTZSBQZVu2jrtklSdmB4=', '2023-10-27 00:00:00', 24),
(693, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTgzOTMyNTAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.1DcbKhLPmJrpO+ERYPewQMFETUUz8TS9UpDzpyqlCyE=', '2023-10-27 00:00:00', 24),
(694, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTg0MDQ2ODUsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.4f59Hin5dGV8bu+fF8tb+iAwYrIuMedqm7sqNXIKBAA=', '2023-10-27 00:00:00', 24),
(695, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTg2NTUwNDYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.gyjQq13BvNw0FKQs925ZL4s8K4UzAs2qq1Cb/KzIa1o=', '2023-10-30 00:00:00', 24),
(696, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTg2NTUwNTEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.AtLlfJGs5WLspuPUI18VjEozMz5nola0uxjgFSz6hdo=', '2023-10-30 00:00:00', 24),
(697, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTg2NTk1OTEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.5QnkKUIa/b7EpVKTcDQKxmj96dujLS3+QjVyqRfSfDk=', '2023-10-30 00:00:00', 24),
(698, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTg2NTk1OTQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.u/fW+BV1ZqoTFMOC6IlYZUbr1acoIe61r3K/1q6dbcg=', '2023-10-30 00:00:00', 24),
(699, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTg2NTk1OTksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.x/2kC6T8UKUYAdUvoY+YpyDkBApGIc0zB1ldS9txlH8=', '2023-10-30 00:00:00', 24),
(700, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTg2NTk5MDMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.ozkdH4vPmhoC8AW65e1BVjSA32kgFoVJ01AiJ1WBMgQ=', '2023-10-30 00:00:00', 24),
(701, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTg2Nzg5MzMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.nvbAoT5tCam/BJQ9cckNDtz4KPoA1Kn+bV1FcUYNQ3A=', '2023-10-30 00:00:00', 24),
(702, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTg2Nzg5MzUsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.N1MNGfDt+dJN6V62xUdVfMvoYgizuwhSTihTsTL6C7c=', '2023-10-30 00:00:00', 24),
(703, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTg2Nzg5MzYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.TMzXwV5SahwrfbRZO82vVBCbt3cwfkqbUBaa9egamXs=', '2023-10-30 00:00:00', 24),
(704, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTg2Nzg5MzgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.tIod26GbZjsxxramMRc4s9IMNpFp4PvrtMMSq/WZL9I=', '2023-10-30 00:00:00', 24),
(705, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTg2Nzg5NDcsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.4Oo/kAn4Oh4BD1k2pImEvMaWpU7Ky/aEPkE3WWIbOAQ=', '2023-10-30 00:00:00', 24),
(706, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTg2Nzg5NDksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.b0RnfHUyb9NkFwUtG935z7eq2igdsnB2QJ+U3AtgalM=', '2023-10-30 00:00:00', 24),
(707, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTg2Nzg5NTMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==./dXZris2J58gdsOyn5t8kzzDqAWHy0fUKWwb65B0aNw=', '2023-10-30 00:00:00', 24),
(708, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTg2Nzg5NzMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.K5gfdh8o0TyJrcGlF52cWx9nINfv+cLTU3ipJN8noiE=', '2023-10-30 00:00:00', 24),
(709, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk0MzA3MzgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.tDRCiksasgfJCMG0LBKC5xHQbervhJxSZ6CJfKt1sHg=', '2023-11-08 00:00:00', 24),
(710, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk0MzE5NzAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.341sT9WDl+q3lZyLcdchPnktdiKg+846mO40wiO7YK0=', '2023-11-08 00:00:00', 24),
(711, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk0MzE5NzEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==./oYLNU4LDzyJv228qIEjxjZO0eK8dKjy4MKftmffGh0=', '2023-11-08 00:00:00', 24),
(712, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk0MzMxNDksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.kfQ2sJRdEKjB62rDvKo8b5jSRWWN6AZkrSSm8msho6g=', '2023-11-08 00:00:00', 24),
(713, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk0MzM1MjksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.D3fFO8tFLYvCEbaQ8FNBrr08ePfpMKLGDMVSs/OmmFM=', '2023-11-08 00:00:00', 24),
(714, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk0NDk4NjcsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.LlpJAKLSVuJjjmhCgC7iIdwMU5xsnscTiLwYkxePisI=', '2023-11-08 00:00:00', 24),
(715, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MjA3NzUsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.wRzezOnMNNrFTe3iDyUXCszScq3n9ZifuEVyxCRHUZE=', '2023-11-09 00:00:00', 24),
(716, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MjEyNTEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.p6f6Hlc/ufM+xIogTx9Z+/N6fMRm6Un0yLuYp/3i0m8=', '2023-11-09 00:00:00', 24),
(717, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MjE0MjQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.ndS4w5xswu96pnwtv/sLsWkdaun3AbJc8BXM3ZiR2JA=', '2023-11-09 00:00:00', 24),
(718, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MjE4MTksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.txp6du69Z8cPeH748INAEuuhv73NzUZoB19pmyqZno0=', '2023-11-09 00:00:00', 24),
(719, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MjMwMDEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.q3rqzXu0NNwCl8URqFjiMD4YXPHTvmuwMnN4Tg/CuXs=', '2023-11-09 00:00:00', 24),
(720, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MjMwMDIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.ZpLCQ7jtBS84ResCArQCOKLMvfEUipz0WAD0daSwXXE=', '2023-11-09 00:00:00', 24),
(721, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MjMyMTcsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.sEtTaK0mxqvz6+54qypoyHXGISOVvsdZP5NQKQ5yK9s=', '2023-11-09 00:00:00', 24),
(722, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MjMyMTgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.Pr1mHtYiens4TICX/ig2yOlHKfXSW9XKxWnjZ6GgkGc=', '2023-11-09 00:00:00', 24),
(723, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MjM0ODYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.eYqMYAyznP8hGY2GSlkRxpeenrrfNlkSw05snoCqL8I=', '2023-11-09 00:00:00', 24),
(724, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MjM0OTQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.4/3AfA7ieSPK5Cz9A/ay+/+nnkVRlvTPa2pLMBNUuMY=', '2023-11-09 00:00:00', 24),
(725, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MjM0OTUsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.JaxehATz52cm4JCnCuGnVyrY+YvmtldKJ7uivDnpPMQ=', '2023-11-09 00:00:00', 24),
(726, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MjM1MjgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.SL8t8FTcLoPC1YiALJxMti7etj7aXP95DpIsdoNS+Vc=', '2023-11-09 00:00:00', 24),
(727, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MjM1NTIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.x2dW0CJS0I/Jouh0FLlaXx16/Og6Go2EOMyoaF6cNvk=', '2023-11-09 00:00:00', 24),
(728, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MjUxMzQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.2TqwaBK7yV/p28c/SyvTUhCmOW0DJg1paLMhqzgeSWU=', '2023-11-09 00:00:00', 24),
(729, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MjUxNDYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.zT9WLYqeCs/kD3siiKjqBR0T830t9G+NZuCsHG6Dqek=', '2023-11-09 00:00:00', 24),
(730, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MjUxNTQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.FLeDrmilrVn8rEzeF1FETq+eU3GP9URePlVRQ3L9T4Y=', '2023-11-09 00:00:00', 24),
(731, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MzU2NzksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.aGnZO2JzYtEqj40FIVsYjdYJzWkx0KI2z568aISJCCE=', '2023-11-09 00:00:00', 24),
(732, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MzU2ODMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.2bN3H2cdQnWUAr7LGvrCDrs7V1butxpR9sSbAH3IDfs=', '2023-11-09 00:00:00', 24),
(733, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MzU2OTksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.EMm8jQj6sUFiLJrdBo11Nq0uzKUy3BMuKtB8vHdKogk=', '2023-11-09 00:00:00', 24),
(734, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MzU3MDAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.T43OQlYweWWHUpxsp/bGxAOrUQSuJD6ne0x1JonRMDw=', '2023-11-09 00:00:00', 24),
(735, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MzY5MjUsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.+uztmBX69Qoo/P9DQeTvuTkvi6ooTf4uiTUNX8oLrsU=', '2023-11-09 00:00:00', 24),
(736, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1MzY5MjYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.i3CtrFB/0gxtyEjkSeQMbOsLtCpuq5PdvLKNvBnJwtE=', '2023-11-09 00:00:00', 24),
(737, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk1Mzc1NTAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.+kB8pqegprteYYP8XLN9csRaLOtd9FJT2rovSjJONvc=', '2023-11-09 00:00:00', 24),
(738, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk2MDE4NTQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.2hqyFAmUne+ndy13zcWutmek6OG/rrPgyiJ1jyHVE2U=', '2023-11-10 00:00:00', 24),
(739, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk2MDE4NTksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.XTNL1k+yZPkXdNhBd8aGs8rinAtk7UolOUo/X/MNIzY=', '2023-11-10 00:00:00', 24),
(740, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk2MDI2ODAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.pLw88HNKHR5SczPaE8Jxwnb+y7rOfKJsp15odPnCWHw=', '2023-11-10 00:00:00', 24),
(741, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk2MDI2ODEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.IC1z7GxmlVHgWehjiFoK1ljvw+C437dGxjOVLg6uWyQ=', '2023-11-10 00:00:00', 24),
(742, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk2MDMwNTMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.iKY9z0Ueyh+JSupP+GCWRKJ31gb8h0c5MZ09LpuYCF8=', '2023-11-10 00:00:00', 24),
(743, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk2MDU3NzAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.cKeaXx5frpjFnQVTzSTyviuNkPqjNYbZ39Bp/UHBcBg=', '2023-11-10 00:00:00', 24),
(744, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk2MDg3MjksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.ZooApjBJjIzKeuYmsIkY6KxEalOY2pdIKGWmUDxpu2s=', '2023-11-10 00:00:00', 24),
(745, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk2MDg3MzEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.SH7+GDz/K7SQJQt0A8iEeGa6dOffJM8A2ZRGUKMDqOc=', '2023-11-10 00:00:00', 24),
(746, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk2MTQ5MjQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.+ZVJLk5/zLXSSLg6A0ee98LjTYPlgYVbiMwWlh/0AMY=', '2023-11-10 00:00:00', 24),
(747, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk2MTUyODIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.mLNC6Nky57Yq2a1AqAHnQqj1IIjFo8Qvf+dDVorqLfM=', '2023-11-10 00:00:00', 24),
(748, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk2MTUyODMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.YvTaLBTJyPqTAJLcwhRe60kiNeMy3U6v1H18GHa1CPk=', '2023-11-10 00:00:00', 24),
(749, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk2MTUyODQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.SCZjpvG51O4cKT+KaNbVPFNFAW537AVK/EzRiLyIwlk=', '2023-11-10 00:00:00', 24),
(750, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk2MTU3MTEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.MbIKuAMzC/9CtfYU4QbqiMRm53KK9dKfSdWoHWSL3CI=', '2023-11-10 00:00:00', 24),
(751, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk2MTU3MTIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==./JBBgHSruV/OSxuuQPbPryAVwD3HvujfS5odAgn69uk=', '2023-11-10 00:00:00', 24),
(752, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk2MjE1MDAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.gpKNSHAMLGmrHCHRUbFWPnRCvue0Ix6UZWu+0kYb848=', '2023-11-10 00:00:00', 24),
(753, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk2MjE1ODEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.vWC9zoJF2OxGgIWPXMlBLhjM02NJNdzUbwElY8YQA7I=', '2023-11-10 00:00:00', 24),
(754, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk2MjE5MjMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.cpk4bkk90bw8mpKOUK/fdXFm99i6WUcdOe/BUf/dH8s=', '2023-11-10 00:00:00', 24),
(755, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk2MjIxNDAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.kBT7uArJ6lJoeNxHyOiSgokUu/U+m7p3JToYZSXrxhA=', '2023-11-10 00:00:00', 24),
(756, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk2MjIzOTYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.aCzRJGOQQGisGoi/Uni28LhkP8l0t3PPGcmU+xY/j5A=', '2023-11-10 00:00:00', 24),
(757, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk2MjI0NzMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.sQ3COIrJ0cSerhcn0dpRZM0EpINIeXdQrxlGiRbr2QE=', '2023-11-10 00:00:00', 24),
(758, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2OTk2MjI4NjAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.1n0o/8cDWPHVoZjABUOMdsR1wlBuB0Nuz41nBoR15Pg=', '2023-11-10 00:00:00', 24),
(759, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA0NjgxNDgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.tCnlftr6k7IY7cc+v3RFmr40lwaVouzxV8H3qNFw214=', '2023-11-20 00:00:00', 24),
(760, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA0NjgxNjEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.eujj7yQtCp9EHMprBYDLbmdKqeXSjq2bnIRJe8RHD2E=', '2023-11-20 00:00:00', 24),
(761, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA0NjgxNjYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.JixWzTB67YmZpGiDlNH9yum8TdnmKtrFv/tn+un1VCM=', '2023-11-20 00:00:00', 24),
(762, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA0NjkwNDcsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.NZnEYjvWrTsDulqzEcz4h70sSwibqWeo4G4TtRO62Jg=', '2023-11-20 00:00:00', 24),
(763, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA0NjkwNDksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.3fCiyu50Ba4C5BLry3gBQC0LDalHnUntLEADlJbMHfQ=', '2023-11-20 00:00:00', 24),
(764, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA0NzA3MjMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.THlqLB2R6FOAl59VKYvujiTM0n5Y07BdDc9DEfkDC3I=', '2023-11-20 00:00:00', 24),
(765, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA0NzA5MTgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.3581QCNwLGEzmhEsjv5Gf4Ow1sQEWDUK24hFlBv7Tys=', '2023-11-20 00:00:00', 24),
(766, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA0NzA5MTksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.Ckii3S12RrkUR2QFNt2ZRU4y/hWh53T7c1V7kzQrZe0=', '2023-11-20 00:00:00', 24),
(767, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA0NzA5MjEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.K3vLh2hcHfcCbyPl1/ANzO9MHbtj0v0+EqJLutNMdzE=', '2023-11-20 00:00:00', 24),
(768, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA0NzUxMzcsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.uVCxO9S2d908oMdpChKWB1bWYf3N1qo/c8+FGc75ciU=', '2023-11-20 00:00:00', 24),
(769, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA1NzI2MDIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.K8Pi1THdx8nDfPQ0KCUK0pj5ipJdW0gP7fvlH6bt7uw=', '2023-11-21 00:00:00', 24),
(770, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA1NzI2MDQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.GKi6cl3Yz5GFwXVtRQ3SjuTpRX+Kf9EGcmjClBIBgnY=', '2023-11-21 00:00:00', 24),
(771, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA1NzI2NTIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.3x2N6t6mECcJJ/cfEn9VLj8daT/j3wRkfpgpwmv9fpI=', '2023-11-21 00:00:00', 24),
(772, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA2Mzc4MjEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.fiWJCAP88Gh3xoRN6nUk2QYXaxFVdYFQ2xtivvk2PlI=', '2023-11-22 00:00:00', 24),
(773, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA2Mzc4MjIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.PE7RX6hXORdN9S9L9Thzis8kfvCaiBEuINZ7BrZHP+E=', '2023-11-22 00:00:00', 24),
(774, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA2Mzc4MjgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.St0u+R0pZbt09c4Gk9wb6NcZSY9w19jj7MEX+Vk1PNM=', '2023-11-22 00:00:00', 24),
(775, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA2MzkzOTAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.3+cmDP65tn5m4rkQnoPd102FSjv+HA/eatvbeuFw4WU=', '2023-11-22 00:00:00', 24),
(776, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA2MzkzOTEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.MitCuepNXzZIZN8dbTpiCPjRvjizJ5pTp1IMDMGr8VA=', '2023-11-22 00:00:00', 24),
(777, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA2NDAxMzcsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.braBZKGYmDfASueL2GlTdjQQk694+yXcksR06qRQ3pU=', '2023-11-22 00:00:00', 24),
(778, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA2NDAxNDIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.0Ck2nXTeN4TEQwtROCGkujn92I3r14ua890t7PDZM60=', '2023-11-22 00:00:00', 24),
(779, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA2NDE1NzUsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.CA0esX8rFc/B3YG2WA1GqKigY6EVaWmcJ/SRl6Tz/9M=', '2023-11-22 00:00:00', 24),
(780, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA2NDE5NjAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.GY4vx8XjSQlugzZpTuk5QmIhASf2ArY7Fg8YfJCScMg=', '2023-11-22 00:00:00', 24),
(781, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA2NjAxNzgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.puA0Zlxa/hHzttMw6JARizjQQ+jahxdPuyFkRZDlWsc=', '2023-11-22 00:00:00', 24),
(782, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA2NjAxODAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.SM8LBQGv+5Rd/PNiqlE+/DPfSkgBa1lmRN5yKCxvrkc=', '2023-11-22 00:00:00', 24),
(783, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA3MzQxNjcsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.pGz2hMv4Q+DyHCPvXja0evIH7bAu2sH9B6sHmsjJakw=', '2023-11-23 00:00:00', 24),
(784, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA3MzQxNjgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.kDIu/xaIrQfQm5NpnYBdP5SPpn6MaBgit6908i82q1o=', '2023-11-23 00:00:00', 24),
(785, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA3NDQwODksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.n7/E5u4eUnwiABIFIw4lsRH87+kO46BJEpjyu3qBtKw=', '2023-11-23 00:00:00', 24),
(786, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA3NDczNzIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.FwKMHds/3kMCRkahVVdiyO/cMPG4v9AAePqZRQxVCpc=', '2023-11-23 00:00:00', 24),
(787, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA3NDczNzMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.V/TS7yQUzTkqFwAuOfPIeJUgDIaO0jMnWB35uvUDlZM=', '2023-11-23 00:00:00', 24),
(788, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA3NDc2NjgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.MNR8Vl8YbvU48xMnX5wYlQv/3bc87OObA2bjFnMtAWU=', '2023-11-23 00:00:00', 24),
(789, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA3NDc2NjksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.JDdbpkoCnHMmYLv2Xb+Es+mw1D+nc8aY0719+SED+BI=', '2023-11-23 00:00:00', 24),
(790, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA3NDc2NzEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.CW8gmRtHxsCqSVmf55sNX01pU9T8RprN57NCUI7RPcM=', '2023-11-23 00:00:00', 24),
(791, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTA3NTEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.d8WS7i1/ecd6VNiYSDUmuhdb7d2YkQTkkJm0VnM1QDQ=', '2023-11-24 00:00:00', 24),
(792, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTA3NTIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.dV2QyUN7EmtW7edd+Pp1tyDhCAGo+H4C3TzHpq3yRfc=', '2023-11-24 00:00:00', 24),
(793, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTA3NjQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.HVt9K/gCYFVFoMXhZmz8he1xj5I4BECvJSq3g5IbA8g=', '2023-11-24 00:00:00', 24),
(794, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTA3NjUsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.mJdpmr2Wnt9x3Rg+epKjeeT4d4HQHMr9q/x3EEh1kTo=', '2023-11-24 00:00:00', 24),
(795, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTA3NjgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.t4dgGovaa1SFlmiuUqqmPH26EQfIn/Qubq7Kr9CcxMQ=', '2023-11-24 00:00:00', 24),
(796, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTA3NjksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.FvGK+XDReG6dEWZqX2Z2MkLe1AR2oUcIB+DS4P15E2Q=', '2023-11-24 00:00:00', 24),
(797, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTA3NzEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.diXYPPf7+FcRVMe3MM3Z7A67lnQblI/PxHuCUjBlFws=', '2023-11-24 00:00:00', 24),
(798, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTEwMDMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.EPSutJn1roYLR6B+knSHqaZeHpsA4DqBPsUDoeRfiDM=', '2023-11-24 00:00:00', 24),
(799, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTEwMDQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.QyQuONcrxNznixaGPZ7yCF5eoc8fq4NDXVjmbX6V9WA=', '2023-11-24 00:00:00', 24),
(800, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTEwMDUsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.xFRkzeqoKJri8BQxDKwxxKSBeQekOvVon12B4gXPqYs=', '2023-11-24 00:00:00', 24),
(801, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTExMjIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.luYccIzcNeqQyBqOyJ9tVUd6I3KttMph6s9LVCmQvfA=', '2023-11-24 00:00:00', 24),
(802, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTExMjMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.LKm+zRYO7uW3yGu0DQSAnWWlPAuXfwwmes1p8zY0MQw=', '2023-11-24 00:00:00', 24),
(803, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTExMjQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.1yE+p5yOYyZLBfHfy6xs9cJKQoTiIbES81kF1K1zEjk=', '2023-11-24 00:00:00', 24),
(804, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTIxMjksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.YJkYqya1UFJsabWa4vIDyY0DCS5XHr1oVV+KWwUhfv0=', '2023-11-24 00:00:00', 24),
(805, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTIxMzAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.JEsDEVaOGR+aL99a3oN3f8zZUz2TIQj4jgzsPBUQ+tM=', '2023-11-24 00:00:00', 24),
(806, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTI2NzEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.fePLSQhEq5VRFWrsss6pIyd7kiqFBtVKylHTZBhm2sE=', '2023-11-24 00:00:00', 24),
(807, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTI2NzIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.hWtNAVb3EGeoy/kjOosr4E86cAqRnCxh6UWBvY4XSF8=', '2023-11-24 00:00:00', 24),
(808, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTUxNTQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.B/HmvFlcRbbJgiIuqb9uYQQzx+Z83LuXMytss/vupUo=', '2023-11-24 00:00:00', 24),
(809, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTUxNTYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.vSPhYUcRllDVMJ3PYbIpHJN7C0FF3hYABUvlbw4vhXc=', '2023-11-24 00:00:00', 24),
(810, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTUyODYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.wFJJYwGp5cAcj+BVI2EDCHZePol8hMjcvq1wU8pIZrY=', '2023-11-24 00:00:00', 24),
(811, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTUyODcsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.hdEkSvCUeXYL8vl8ZLN0QVP+3RfoYiuBIaTQBXL0UNA=', '2023-11-24 00:00:00', 24),
(812, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTU1NzMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.qzkcKXyIFP1D6ggRhaHqwV2hFdU9KNq/kWp1zi4Xf0A=', '2023-11-24 00:00:00', 24),
(813, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTU1NzYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.OGBYrwn8o8WzFBwhbCpxZw/LurSyriW2fFyKlwx1vVU=', '2023-11-24 00:00:00', 24),
(814, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTU5ODksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.G2KsH/g2wfjY/AXOWmwjI1VC9sI2D/9/HfQkmlLcFos=', '2023-11-24 00:00:00', 24),
(815, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDA4MTU5OTAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.waE/ofjQ6NiVU33M2XL6Fjkec1R4PjrYv0HwmAmlkrA=', '2023-11-24 00:00:00', 24),
(816, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDE0MjUzMTgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.e916lfCoy0/uk1BfoNoKMRaDyCOoOGB+3niK+qkfsTQ=', '2023-12-01 00:00:00', 24),
(817, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDE0MjUzMTksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.hzjwWMjl3Dx1O9NkpBE3us3HDsCRzyYO3PPpV62FYdY=', '2023-12-01 00:00:00', 24),
(818, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDE0MjU0MTYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.rJfUXZzHzlEQQwnTgw6EZKzTAEtMhys6A4m73cT+4kM=', '2023-12-01 00:00:00', 24),
(819, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDE4NTI0MTYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.oPVjXyFwUdjS8R57T+qauYOS2qFYENvUS3r6meMlbGU=', '2023-12-06 00:00:00', 24),
(820, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDE4NTI0MjIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.vKS0td2wa0UYn24/qlS0+A9Eb5CFAENftj/DXmKkBpY=', '2023-12-06 00:00:00', 24),
(821, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDE4NTI0MjMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.JD407Tv01bDVHuWucJFM7igQSbjWV+I2jmYnnYEmHs4=', '2023-12-06 00:00:00', 24),
(822, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDE4NTI0MjgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.4Z0N2fsbUN88FrUrZ3hxaWCPdjIh/FHuwWMHHHGJTgI=', '2023-12-06 00:00:00', 24),
(823, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDE4NTI0MjksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.EG6d61Umqm+W/1FOL56AKKdWfc/QFz4Wy6B+hePhsmE=', '2023-12-06 00:00:00', 24),
(824, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDE4NTI0MzEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.VwuVTT1rUJep6KdEjXHBcxAUL2Ggv+N7mv9sqkA+Bkg=', '2023-12-06 00:00:00', 24),
(825, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDE4NTI0MzIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.nt+qW86vCylKsCsDiBZvwNSLku2w0fYgDLC8AszJ0tA=', '2023-12-06 00:00:00', 24),
(826, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDE4NTI0MzMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.JUwg56pmYhSfnConbQDBm69g/BxIpookz4/0XIFC6qM=', '2023-12-06 00:00:00', 24);
INSERT INTO `user_connected` (`id`, `token_created`, `date_created`, `id_user`) VALUES
(827, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDE4NTI2NjgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.BQYNiPJknDuyMr3MuQeiAeNvYST+wZmSsNvQobOo/Hk=', '2023-12-06 00:00:00', 24),
(828, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDE4NTMzOTEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.yuT2xphSButDkEG2Vbsv+s7KPjAXCw8cj7AulzK2dK0=', '2023-12-06 00:00:00', 24),
(829, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDE4NTM4NjksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.r9yfM72dRa2fIZvgpyqvtqZrJIeqXAkKzKWoS7V2kZg=', '2023-12-06 00:00:00', 24),
(830, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDE4NTM5MTYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.CMns59+umF3FDhW8C/uExtg2zl+tK/qpezYDeH6sfSA=', '2023-12-06 00:00:00', 24),
(831, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDE4NTQyODIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.XnEf7Dt8fYfpEU91FGzq9vTwwW00ul22fhxLWTQvtJk=', '2023-12-06 00:00:00', 24),
(832, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDE4NTQ1NDIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.hgOj2lVMZzmRkr8VFcE3MmZx95VnePLn6PF+KSoB1hQ=', '2023-12-06 00:00:00', 24),
(833, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDE4NTQ1NzAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.mTi2LtK7jKH/Ltj9RcPHlIccejJs5rZfu6RKPiKw6sY=', '2023-12-06 00:00:00', 24),
(834, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDE4NTQ1NzEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.mxWJwCYiC85vEv2MOz1cZW/8GIvMluLsYYvaCi45/7o=', '2023-12-06 00:00:00', 24),
(835, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDE4NjIwODQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.KWNHRPOyAc7QH1o13oEzZTq6ANBIc4pH1JcOoS/iTiA=', '2023-12-06 00:00:00', 24),
(836, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDE4NjIwOTEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.il2dW6cmLR719pb3z3GAhv4iHT01j4YXKBnBo50Vtj0=', '2023-12-06 00:00:00', 24),
(837, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDIxNDI0NDAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.vGJ6uUyHDN1cabN8wppqneGjKL+oKlN6ENV3CtP+SxU=', '2023-12-09 00:00:00', 24),
(838, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDIxNDI0NDYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.gD/ivl6zVpwn2a+yWjKioSYsl8CCIRAaMnVt1E6ZsFA=', '2023-12-09 00:00:00', 24),
(839, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDIxNDI5ODAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.cjx6MQbXMvGUVRaH9i5dzbM4VmTw67Iuyd506Su+fPo=', '2023-12-09 00:00:00', 24),
(840, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDIxNDQ3NjIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.Jdrp9AwzJvknYL+M5Sn6rAWDwwkgn4RtwgICW0LYatQ=', '2023-12-09 00:00:00', 24),
(841, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDIxNDYwNTksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.8EqUE5ygr77f0jNVML/URR8UCmY0SNRNySbEheZIvLU=', '2023-12-09 00:00:00', 24),
(842, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDIyMjkzODIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.P/C+LxEw1T+2AVYegiVhExEjngsDCuEwPxF90kK7OHI=', '2023-12-10 00:00:00', 24),
(843, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDIyOTk0NTAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.f5jfrudxo5cLVRZruyUldu28PcZlLsI/CP8pBYc0e0M=', '2023-12-11 00:00:00', 24),
(844, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDIyOTk0NjMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.u2KOpLQQOJWWwpotSLy0a55cMZHNTOujf+Zq6IiHlPI=', '2023-12-11 00:00:00', 24),
(845, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDIyOTk2NTMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.AwzxYk+gT7rwEDQj0spHiW5BDDK8RHXzdT/GUgApqBc=', '2023-12-11 00:00:00', 24),
(846, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDIyOTk3MTIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.guRt1u6Nv6VpzVv7rMylOMgXIOQWTxMnqiCa/GOYqL8=', '2023-12-11 00:00:00', 24),
(847, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDIyOTk4MTIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.wW4hUkhyUrkKIsiRyhP0fwRE/zI/i/EQDpv2+3zZNAk=', '2023-12-11 00:00:00', 24),
(848, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDIzMDUxMjIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.vjGtHvuRNMlmlOD4HGr6SBHfPd8NDm5qLoOPQF2e5nk=', '2023-12-11 00:00:00', 24),
(849, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDIzMDUxMzQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.Z39zd3pB5S4h4f+3ovmUbV1B+n4AAGXVM26MGPsCJDs=', '2023-12-11 00:00:00', 24),
(850, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDIzMDkyODQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.fXMZSdH/0kS8t80LPCaymugcSprfhlzN7Ls3GgfuzPM=', '2023-12-11 00:00:00', 24),
(851, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NTkyNDMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.RFYEvBCyPnI2UBhgLdWpMsCtpyljeSzJ46/lip/pyUI=', '2023-12-13 00:00:00', 24),
(852, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NTkyNTAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.AwolIhHUw18EoqvmzT+Ua3PiDTf4x6AFiaOWT87MOfs=', '2023-12-13 00:00:00', 24),
(853, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjEwNTUsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.Xyirjewj45KjaTQABPXkcjCe+0QXAk5fpokSBM07YbM=', '2023-12-13 00:00:00', 24),
(854, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjEwNTYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.UOVP5K7a9NbaSyHA5IS+p+9GGPuYZCqqKMxwtQv1sl4=', '2023-12-13 00:00:00', 24),
(855, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjEwNTcsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.w/G6h7Z17SWQCriw7Dg8e6Av+brga20OCqfV9g/62Og=', '2023-12-13 00:00:00', 24),
(856, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjEwNjEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.JVpe0IQd6feWJrLW9bcLYmBQbsm6lE2JcqQ19Raym7g=', '2023-12-13 00:00:00', 24),
(857, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjMzMzksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.+IvZ7p0oRL6f68xkYO9FHSojhehycjkCbCEgx94yFOc=', '2023-12-13 00:00:00', 24),
(858, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjMzNDAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.1EK9nmXdsx8XdG2pI5X04GRqpWze+b5CObQxnFCORWY=', '2023-12-13 00:00:00', 24),
(859, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjMzNDEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.f8UUyDf0gUjHj3HVYd11NuuVGBheEsGUYLpd98V3v2w=', '2023-12-13 00:00:00', 24),
(860, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjQ3NTQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.st7ikdPS0cjC3j8/lNgwnnD6PFwOY0ax/2qapIta6Bo=', '2023-12-13 00:00:00', 24),
(861, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjQ3NTUsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.a2nkbxHvxhCCTo1xgYTVzupM3+Zf8PZ89hiDQGGoN0U=', '2023-12-13 00:00:00', 24),
(862, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjQ3NTgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==./MEHH1z98RDMpqxpLysDyAXANiBWRNgfm4zaGn6QUDo=', '2023-12-13 00:00:00', 24),
(863, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjQ3NTksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.q0jx0CNC4U3TowiOXbIeirX0JWMzkzxH3iC6Dx8WbvI=', '2023-12-13 00:00:00', 24),
(864, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjQ3NjEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.hIRQin6YpMsn87CIco2IErTzffaC26rAFAVczSHMkzg=', '2023-12-13 00:00:00', 24),
(865, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjQ3NjIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.JMA8GrwC6Ef6/arJI9qfBaAapRf1i7HgKF9vFB/R91M=', '2023-12-13 00:00:00', 24),
(866, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjQ3NjQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.dCwYWyoWbpmdSa04Qr7d3x01MyKn4ktvlLUvYdrT+wQ=', '2023-12-13 00:00:00', 24),
(867, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjU1MDMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.pHIk9665vNjawiayLtCYXoNHwv64I8meKFFO3MEWZqY=', '2023-12-13 00:00:00', 24),
(868, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjU5NTQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.0XXtDkWMrZCQLMv83dSXPxghpGTn1uLlda8pqYjX4Fs=', '2023-12-13 00:00:00', 24),
(869, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjU5NjEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.CXtc1xfOHCgJ0vFs341aq5IMTVg3zjjMluvDveeiqk0=', '2023-12-13 00:00:00', 24),
(870, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjU5OTUsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.aMClFi2hCud4l94ASdQr55E3LKl3EzE1lFEV1Jmmz+s=', '2023-12-13 00:00:00', 24),
(871, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjU5OTYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.LXZtNiF5PDspPA7opyhNu0gEmGSN/IzeDFWVgvH2hw4=', '2023-12-13 00:00:00', 24),
(872, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjU5OTgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.C54vU/YhAcHKlxmZ6Q3P7T2KEOz2ngZxEevi0gYAn6c=', '2023-12-13 00:00:00', 24),
(873, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjU5OTksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.hccjlgwtgttiCMO+wfDvd5VJK114yLULw6RVmQekI4U=', '2023-12-13 00:00:00', 24),
(874, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjYwMDAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.lwlb8KLD7wjCXRKcQ1DaX1yqL/zzg9jY1Zw6Xkx4Jro=', '2023-12-13 00:00:00', 24),
(875, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjYwMDIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.hfM6MI2yBx5XVTUC/p18wgr+thAvDq0SY2YTflG9qvs=', '2023-12-13 00:00:00', 24),
(876, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjYzNTIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.mhZ//k8dbkOyPjwfjCOKyzKyKO57TPOHqKfdA9QtpWk=', '2023-12-13 00:00:00', 24),
(877, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI0NjYzNTMsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.gdkHVKx4OAHwzj7z2eSkub3lrWK8xa5Tc2+SnN/L21s=', '2023-12-13 00:00:00', 24),
(878, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzA1NTksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.IC/mFeqhHCHJrLybdNghLHB6NmpjdUy3hFxXRCdk6+k=', '2023-12-15 00:00:00', 24),
(879, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzA1NjEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.5Y/Y0+NoDkikn9r/kU18i33XMyxoZ2tlDKWgRQpzJ00=', '2023-12-15 00:00:00', 24),
(880, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzA1NjksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.nv0jwIUfYb+nwqB+/iK2caJZlqY4g148PmE40cmkDWU=', '2023-12-15 00:00:00', 24),
(881, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzA2MjEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.rlIxhHNlnzbRAMPF7cuhtkKbWgtO27cCRyiy4f2CFx0=', '2023-12-15 00:00:00', 24),
(882, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzEyMDYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.z8z/9cxhfs23h6TU2g3AVLVOTpgbnI9YPHDDBE0iTNg=', '2023-12-15 00:00:00', 24),
(883, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzQwOTksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.Y4qDGEjp3XHyBFix4LdKGTpzNKibjJ6ShHfomwpTYYM=', '2023-12-15 00:00:00', 24),
(884, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzQxMDAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.4UHyuvNJXhXS7msesyS98c8FzKtWbSbTs+/lqCt7R3w=', '2023-12-15 00:00:00', 24),
(885, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzQxMDEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.Ued9VsHSiLT8bqic2fS0aJw25DET1/YbF5BSPHmahF8=', '2023-12-15 00:00:00', 24),
(886, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzQxMDYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.uM7RiAII2E1yvQwNT5Q8XLY4h/QVGNbc1anIE2WMGzM=', '2023-12-15 00:00:00', 24),
(887, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzQxMDcsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.Rxx9iylIVSluPL4hrnfAkJuOzMSxlmXEo+ob7ByO8cc=', '2023-12-15 00:00:00', 24),
(888, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzQxMDgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.HZrtrv4a3zHCKhp5Bs/5YkRRAJPFVKZxcRRJQ4jtuDY=', '2023-12-15 00:00:00', 24),
(889, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzQxMTAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.6By/1RmaeFsnK0qyqkku5FcjjfoYH/VVhLwLKERTROc=', '2023-12-15 00:00:00', 24),
(890, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzQxMTEsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.1W7q3Fo3PiLslD0ZMWUERgu3bGKcM7fTX2E/WjMbvPg=', '2023-12-15 00:00:00', 24),
(891, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzQyMDIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.Yf13E2vTsz36YVmjW9BnnYFLfkWYi+x7lhtViyMNEgM=', '2023-12-15 00:00:00', 24),
(892, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzQ3OTksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.Fk6iQCJe+PTF5/wkv/2RWIdFRKx3imI9hus6nhmcHfU=', '2023-12-15 00:00:00', 24),
(893, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzQ4MDAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.Y3GBxnBJNeVI7LXVWybzn9gq4/v51UAV2oMV5ejpm8g=', '2023-12-15 00:00:00', 24),
(894, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzQ4MjQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.dFDbEw3LhTTAiI1XbFwwm2/E+Wik7PFx0wCYZUOaQlk=', '2023-12-15 00:00:00', 24),
(895, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzQ4MjYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.1hHDLPSJVDFrEfOxveDBeYv3+rlUi3TxDoTYwmrIM1o=', '2023-12-15 00:00:00', 24),
(896, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzQ4MjcsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.elSFUcpXwChxqOM4ZsbKawrLWyy0Rr6lbIH/51+qKgE=', '2023-12-15 00:00:00', 24),
(897, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzUwOTcsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.2v352IEl8MZ2Sh3YTiirgQK2cyp1slv/4KL7YkQlfl4=', '2023-12-15 00:00:00', 24),
(898, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzUwOTgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.VtMg4GYhjB9+5LoePgPK3aVRxNUdld50pfes5ZgHNK4=', '2023-12-15 00:00:00', 24),
(899, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzUxMTYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.7C3fgo3XqfjdjBNX7IBhghXVchIcWY7xQkVixccYG9s=', '2023-12-15 00:00:00', 24),
(900, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzU0MjksInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.p0wrSHar8kpnB3Bt25gLZaBR2WP94T9o3teH9tF25yM=', '2023-12-15 00:00:00', 24),
(901, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzY3MzIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.6WUC4uVNVNFB6zqeaRM05hKhD3GB+MPON1UMCfqczAY=', '2023-12-15 00:00:00', 24),
(902, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzgxNDUsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.Ksnfau8/dTB85L3aZnuSnFhbenExFRvRHms3vEkFrzc=', '2023-12-15 00:00:00', 24),
(903, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzgxNDYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.+7lqtrTEL5k/7VsrYZYyntWqhPyZzeTWeu+DoTcvYD4=', '2023-12-15 00:00:00', 24),
(904, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2MzgxNDcsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.SfB9/6DcENEGE+y1nnYGTNo48vydxlP/cdDAGyB9QC4=', '2023-12-15 00:00:00', 24),
(905, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2Mzg4MzAsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.wq95Z7CgFjTMbBTaTIq9tlwZIOPRxSMqU9fx+HiWxfU=', '2023-12-15 00:00:00', 24),
(906, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2NDI5MjYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.KObzMw1J4wCLfBd+p05oHls9RxfTgmvMv5kVuEqqSf0=', '2023-12-15 00:00:00', 24),
(907, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2NDI5MjgsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.2WmX6nVbn2WmTIxwk8+3kw+c2dpOGzRI4n1i3KkLtWg=', '2023-12-15 00:00:00', 24),
(908, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2NDYxNDQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.fXqucLmARPbTthSTivaD9Lv8cGhF85d02nhu9UzSRt8=', '2023-12-15 00:00:00', 24),
(909, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2NDYxNDUsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.O+A3fQlOnx3HyyZm+wJxh5j6NAmDSNSHG4TvrKIFBYI=', '2023-12-15 00:00:00', 24),
(910, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDI2NDYxNDYsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.XscHeIQ9DHbs4VZaaUsqyNLcsD3YLBmPcgZAuEveMfU=', '2023-12-15 00:00:00', 24),
(911, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDMwNjU2MzIsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.bno/QpsLCusf4LeM8wcvoAJm0q0Zpd0BRA9xKZ4MsiQ=', '2023-12-20 00:00:00', 24),
(912, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDMwNjU2MzQsInVpZCI6IjI0IiwiZW1haWwiOiJjbWhnZ0BnbWFpbC5jb20ifQ==.syTyFgZ9G6NRZvbV4nwApJdALI7E6hul5roi3GrXOVw=', '2023-12-20 00:00:00', 24);

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_grouping`
--

DROP TABLE IF EXISTS `user_grouping`;
CREATE TABLE IF NOT EXISTS `user_grouping` (
  `id_user` int NOT NULL,
  `id_group_user` int NOT NULL,
  `observation` text NOT NULL,
  PRIMARY KEY (`id_user`,`id_group_user`),
  KEY `id_group_user` (`id_group_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `user_grouping`
--

INSERT INTO `user_grouping` (`id_user`, `id_group_user`, `observation`) VALUES
(1, 2, 'Muito bom'),
(1, 3, 'Muito bom');

-- --------------------------------------------------------

--
-- Estrutura da tabela `video`
--

DROP TABLE IF EXISTS `video`;
CREATE TABLE IF NOT EXISTS `video` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `link` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Extraindo dados da tabela `video`
--

INSERT INTO `video` (`id`, `title`, `link`) VALUES
(1, 'Batalha de honra.', 'www.google.com.br'),
(3, 'Batalha de honra', 'www.google.com.br'),
(4, 'Bataccc', 'www.google.com.br'),
(5, 'Batacchjc', 'www.google.com.br'),
(6, 'Batavvvcchjc', 'www.google.com.br'),
(7, 'Batavvvcddchjc', 'www.google.com.br');

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `department`
--
ALTER TABLE `department`
  ADD CONSTRAINT `department_ibfk_1` FOREIGN KEY (`id_direction`) REFERENCES `direction` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `fair`
--
ALTER TABLE `fair`
  ADD CONSTRAINT `fair_ibfk_1` FOREIGN KEY (`id_province`) REFERENCES `province` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `mpme`
--
ALTER TABLE `mpme`
  ADD CONSTRAINT `mpme_ibfk_1` FOREIGN KEY (`id_province`) REFERENCES `province` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `ppn`
--
ALTER TABLE `ppn`
  ADD CONSTRAINT `ppn_ibfk_1` FOREIGN KEY (`id_province`) REFERENCES `province` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `production`
--
ALTER TABLE `production`
  ADD CONSTRAINT `production_ibfk_2` FOREIGN KEY (`id_productive_row`,`id_productive_sub_row`,`id_product`,`id_sub_product`) REFERENCES `structure_national_production` (`id_productive_row`, `id_productive_sub_row`, `id_product`, `id_sub_product`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `production_ibfk_3` FOREIGN KEY (`id_province`) REFERENCES `province` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `production_ibfk_4` FOREIGN KEY (`id_production_period`) REFERENCES `production_period` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `productive_row`
--
ALTER TABLE `productive_row`
  ADD CONSTRAINT `productive_row_ibfk_1` FOREIGN KEY (`id_type_production`) REFERENCES `type_production` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `product_5_4`
--
ALTER TABLE `product_5_4`
  ADD CONSTRAINT `product_5_4_ibfk_1` FOREIGN KEY (`id_productive_row`) REFERENCES `productive_row_54` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `profile_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `profile_ibfk_2` FOREIGN KEY (`id_role`) REFERENCES `role` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `province`
--
ALTER TABLE `province`
  ADD CONSTRAINT `province_ibfk_1` FOREIGN KEY (`id_region`) REFERENCES `region` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `structure_national_production`
--
ALTER TABLE `structure_national_production`
  ADD CONSTRAINT `structure_national_production_ibfk_1` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `structure_national_production_ibfk_2` FOREIGN KEY (`id_productive_row`) REFERENCES `productive_row` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `structure_national_production_ibfk_3` FOREIGN KEY (`id_productive_sub_row`) REFERENCES `productive_sub_row` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `structure_national_production_ibfk_4` FOREIGN KEY (`id_sub_product`) REFERENCES `sub_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `structure_national_production_ibfk_5` FOREIGN KEY (`id_unit_measure`) REFERENCES `unit_measure` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `trade_balance`
--
ALTER TABLE `trade_balance`
  ADD CONSTRAINT `trade_balance_ibfk_2` FOREIGN KEY (`id_trade_balance_category`) REFERENCES `trade_balance_category` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `trade_balance_ibfk_3` FOREIGN KEY (`id_product_balance`) REFERENCES `product_balance` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `training`
--
ALTER TABLE `training`
  ADD CONSTRAINT `training_ibfk_1` FOREIGN KEY (`id_training_initiative`) REFERENCES `training_initiative` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `training_ibfk_2` FOREIGN KEY (`id_province`) REFERENCES `province` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

--
-- Limitadores para a tabela `user_grouping`
--
ALTER TABLE `user_grouping`
  ADD CONSTRAINT `user_grouping_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT `user_grouping_ibfk_2` FOREIGN KEY (`id_group_user`) REFERENCES `group_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
