<?php

require_once 'Auditing.php';
require_once 'DocumentStorage.php';

class Eventt {

    public $id;
    public $title;
    public $event_date;
    public $local;
    public $description;
    public $eventPhoto;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create event 
    function registerEvent() {
        $cons = "INSERT INTO event VALUES(?, ?, ?, ?, ?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->title);
        $prep->bindparam(3, $this->event_date);
        $prep->bindparam(4, $this->local);
        $prep->bindparam(5, $this->description);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            $lastId = $this->dbh->lastInsertId();
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfeterAction($lastId);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('event', 'inserir', '', $dataAfterExecution);
            return $lastId;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

    // Read all event
    function readEvent() {
        /* require_once "Adherent.php";
          require_once "CategoryProduct.php";
          require_once "Seal.php"; */
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM event";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['title'] = $reg->title;
                $arrayData[$i]['event_date'] = $reg->event_date;
                $arrayData[$i]['local'] = $reg->local;
                $arrayData[$i]['description'] = $reg->description;
                //Getting the productiveRow54 photo              
                $productPhoto = new DocumentStorage('eventPhoto', $reg->id, $this->dbh);
                $productPhoto = $productPhoto->getDocument();
                $arrayData[$i]['photo'] = $productPhoto;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined event
    function readDeterminedEvent() {
        /* require_once "Adherent.php";
          require_once "CategoryProduct.php";
          require_once "Seal.php"; */
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM event WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['title'] = $reg->title;
                $arrayData[$i]['event_date'] = $reg->event_date;
                $arrayData[$i]['local'] = $reg->local;
                $arrayData[$i]['description'] = $reg->description;
                //Getting the productiveRow54 photo              
                $productPhoto = new DocumentStorage('eventPhoto', $reg->id, $this->dbh);
                $productPhoto = $productPhoto->getDocument();
                $arrayData[$i]['photo'] = $productPhoto;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return $e->getMessage();
            //return false;
        }
    }

    // Update event
    function updateEvent() {
        $cons = "UPDATE event SET title = ?, event_date = ?, local = ?, description = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->title);
        $prep->bindparam(2, $this->event_date);
        $prep->bindparam(3, $this->local);
        $prep->bindparam(4, $this->description);
        $prep->bindparam(5, $this->id);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfeterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfeterAction($this->id);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('event', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Delete event 
    function deleteEvent() {
        $cons = "DELETE FROM event WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfeterAction($this->id);
        // instance the class user
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('event', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get determined type of production
    function getDeterminedTypeProduction() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM type_production WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idTypeProduction, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['comment'] = $reg->comment;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return $e->getMessage();
            //return false;
        }
    }

    // Get productive row from determined type of production
    function getProductiveRowFromDeterminedTypeProduction() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM productive_row WHERE id_type_production = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idTypeProduction, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['comment'] = $reg->comment;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return $e->getMessage();
            //return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfeterAction($DataId) {
        $i = 0;
        $produciveData = "";
        $cons = "SELECT * from event WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $produciveData = "id: " . $reg->id . ", title, " . $reg->title . ", event_date" . $reg->event_date . ", local" . $reg->local . ", description" . $reg->description;
            }
            //Format data of the system element

            return $produciveData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

///
    function checkEvent() {
        $i = 0;
        $cons = "SELECT * FROM event WHERE title = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->title, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }

            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

}

?>