<?php

require_once 'Auditing.php';

class ProductionPeriod {

    public $id;
    public $designation;
    public $startDate;
    public $endDate;
    public $observation;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create production_period
    function registerProductionPeriod() {
        $cons = "INSERT INTO production_period VALUES(?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->designation);
        $prep->bindparam(3, $this->startDate);
        $prep->bindparam(4, $this->endDate);
        $prep->bindparam(5, $this->observation);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of department before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('departamento', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read all production_period
    function readProductionPeriod() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM production_period";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['start_date'] = $reg->start_date;
                $arrayData[$i]['end_date'] = $reg->end_date;
                $arrayData[$i]['observation'] = $reg->observation;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined production_period
    function readDeterProductionPeriod() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM production_period WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['start_date'] = $reg->start_date;
                $arrayData['end_date'] = $reg->end_date;
                $arrayData['observation'] = $reg->observation;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update production_period
    function updateProductionPeriod() {
        $cons = "UPDATE production_period SET designation = ?, start_date = ?, end_date = ?, observation = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation);
        $prep->bindparam(2, $this->startDate);
        $prep->bindparam(3, $this->endDate);
        $prep->bindparam(4, $this->observation);
        $prep->bindparam(5, $this->id);
        //$prep->execute();
        // Get data of department before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of department before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('departamento', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            return $e->getMessage();
        }
    }

    // Delete production_period
    function deleteProductionPeriod() {
        $cons = "DELETE FROM production_period WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of department before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class department
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('departamento', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data of a specific production_period
    function getDataProductionPeriod($id) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM production_period WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['start_date'] = $reg->start_date;
                $arrayData['end_date'] = $reg->end_date;
                $arrayData['observation'] = $reg->observation;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $stringData = "";
        $dataReceivedFormated = '';
        $cons = "SELECT * FROM production_period WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $stringData = "Designation: " . $reg->designation . ", Start date: " . $reg->start_date . ", End date: " . $reg->end_date . ", Observation: " . $reg->observation;
            }

            return $stringData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

    function checkProductionPeriod() {
        $i = 0;
        $cons = "SELECT * FROM production_period WHERE designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }

            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    //
    function checkService2($acronym) {
        $i = 0;
        $cons = "SELECT * FROM service WHERE acronym = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $acronym, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }

            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

}

?>