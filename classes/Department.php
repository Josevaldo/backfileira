<?php

require_once 'Auditing.php';
require_once 'Direction.php';

class Department {

    public $id;
    public $name;
    public $acronym;
    public $description;
    public $idDirection;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create department
    function registerDepartment() {
        $cons = "INSERT INTO department VALUES(?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->name);
        $prep->bindparam(3, $this->acronym);
        $prep->bindparam(4, $this->description);
        $prep->bindparam(5, $this->idDirection);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of department before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('departamento', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read all department
    function readDepartment() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM department";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['acronym'] = $reg->acronym;
                $arrayData[$i]['description'] = $reg->description;
                //$arrayData[$i]['id_direction'] = $reg->id_direction;
                $directionData = new Direction($this->dbh);
                $arrayDirectionData = $directionData->getDataDirection($reg->id_direction);
                $arrayData[$i]['direction'] = $arrayDirectionData;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined department
    function readDeterminedDepartment() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM department WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['name'] = $reg->name;
                $arrayData['acronym'] = $reg->acronym;
                $arrayData['description'] = $reg->description;
                $directionData = new Direction($this->dbh);
                $arrayDirectionData = $directionData->getDataDirection($reg->id_direction);
                $arrayData['direction'] = $arrayDirectionData;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update department
    function updateDepartment() {
        $cons = "UPDATE department SET name = ?, acronym = ?, description = ?, id_direction = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->name);
        $prep->bindparam(2, $this->acronym);
        $prep->bindparam(3, $this->description);
        $prep->bindparam(4, $this->idDirection);
        $prep->bindparam(5, $this->id);
        //$prep->execute();
        // Get data of department before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();

            //record update
            // Get data of department before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('departamento', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
            //true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            return $e->getMessage();
        }
    }

    // Delete department
    function deleteDepartment() {
        $cons = "DELETE FROM department WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of department before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class department
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('departamento', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data of a specific department
    function getDataDepartment($id) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM department WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['name'] = $reg->name;
                //$arrayData['acronym'] = $reg->acronym;
                $arrayData['description'] = $reg->description;
                $directionData = new Direction($this->dbh);
                $arrayDirectionData = $directionData->getDataDirection($reg->id_direction);
                $arrayData['direction'] = $arrayDirectionData;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data of a direction from department
    function getDataDirectionFromDepartment($id) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM department WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $this->idDirection = $reg->id_direction;
                $i++;
            }
            // Get data of a specific department
            require_once 'Direction.php';
            $dataDirection = new Direction($this->dbh);
            $arrayData = $dataDirection->getDataDirection($this->idDirection);
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT department.name AS dep_name,department.acronym AS dep_acronym,department.description AS dep_desc,direction.description AS dir_desc FROM department 
				JOIN direction ON department.id_direction = direction.id WHERE department.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                //$arrayData['id'] = 'Identificador: '.$reg->id;
                $arrayData['name'] = 'Nome: ' . $reg->dep_name;
                $arrayData['acronym'] = 'Sigla: ' . $reg->dep_acronym;
                $arrayData['description'] = 'Descrição: ' . $reg->dep_desc;
                $arrayData['Direction'] = 'Direção: ' . $reg->dir_desc;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

    function checkDepartment() {
        $i = 0;
        $cons = "SELECT * FROM department WHERE name = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->name, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }

            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }
    //
     function checkDepartment2($name) {
        $i = 0;
        $cons = "SELECT * FROM department WHERE name = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $name, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }

            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

}

?>