<?php

require_once 'Auditing.php';
require_once 'TradeBalanceCategory.php';
require_once 'ProductBalance.php';

class TradeBalance {

    public $id;
    public $year;
    public $value;
    public $quant;
    public $acronym;
    public $tradeBalanceType;
    public $idProductBalance;
    public $idTradeBalaCategory;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create trade_balance trade balance
    function registerTradeBalance() {
        $cons = "INSERT INTO trade_balance VALUES(?,?,?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->year);
        $prep->bindparam(3, $this->value);
        $prep->bindparam(4, $this->quant);
        $prep->bindparam(5, $this->acronym);
        $prep->bindparam(6, $this->tradeBalanceType);
        $prep->bindparam(7, $this->idProductBalance);
        $prep->bindparam(8, $this->idTradeBalaCategory);

        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfeterAction($lastId);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('trade balance', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read all trade_balance
    function readTradeBalance() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM trade_balance";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['year'] = $reg->year;
                $arrayData[$i]['value'] = $reg->value;
                $arrayData[$i]['quant'] = $reg->quant;
                $arrayData[$i]['acronym'] = $reg->acronym;
                $arrayData[$i]['trade_balance_type'] = $reg->trade_balance_type;
                //Instancing the Product54 class
                $productBalance = new ProductBalance($this->dbh);
                $productBalance->id = $reg->id_product_balance;
                $arrayData[$i]['product'] = $productBalance->readDetermProductBalance();
                //Instancing the TradeBalanceCategory
                $tradeBalanceCate = new TradeBalanceCategory($this->dbh);
                $tradeBalanceCate->id = $reg->id_trade_balance_category;
                $arrayData[$i]['category'] = $tradeBalanceCate->readDetermTradeBalaCate();

                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined trade_balance
    function readDetermTradeBalance() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM trade_balance WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['year'] = $reg->year;
                $arrayData['value'] = $reg->value;
                $arrayData['quant'] = $reg->quant;
                $arrayData['acronym'] = $reg->acronym;
                $arrayData['trade_balance_type'] = $reg->trade_balance_type;
                //Instancing the ProductBalance class
                $productBalance = new ProductBalance($this->dbh);
                $productBalance->id = $reg->id_product_balance;
                $arrayData[$i]['product'] = $productBalance->readDetermProductBalance();
                //Instancing the TradeBalanceCategory
                $tradeBalanceCate = new TradeBalanceCategory($this->dbh);
                $tradeBalanceCate->id = $reg->id_trade_balance_category;
                $arrayData['category'] = $tradeBalanceCate->readDetermTradeBalaCate();
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update trade_balance
    function updateTradeBalance() {
        $cons = "UPDATE trade_balance SET year = ?, value = ? , quant = ?, acronym = ?, trade_balance_type = ?, id_product_balance = ?, id_trade_balance_category = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->year);
        $prep->bindparam(2, $this->value);
        $prep->bindparam(3, $this->quant);
        $prep->bindparam(4, $this->acronym);
        $prep->bindparam(5, $this->tradeBalanceType);
        $prep->bindparam(6, $this->idProductBalance);
        $prep->bindparam(7, $this->idTradeBalaCategory);
        $prep->bindparam(8, $this->id);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfeterAction($this->id);
        try {
            $prep->execute();

            //record update
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfeterAction($this->id);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('trade_balance', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Delete trade_balance
    function deleteTradeBalance() {
        $naProvince = 20;
        $cons = "DELETE FROM trade_balance WHERE id = ?";
//        $cons = "DELETE FROM province WHERE id = ? AND id != ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
//        $prep->bindparam(2, $naProvince, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfeterAction($this->id);
        // instance the class user
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('trade balance', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfeterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT * FROM trade_balance_category WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['id'] = 'Identificador: ' . $reg->id;
                $arrayData['designation'] = 'Designação: ' . $reg->designation;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

//
    function checkTradeBalance() {
        $i = 0;
        $cons = "SELECT * FROM trade_balance WHERE designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }

            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

}

?>
