<?php

require_once 'Auditing.php';
require_once 'DocumentStorage.php';

class Gallery {

    public $id;
    public $title;
    public $galleryPhoto;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create gallery
    function registerGallery() {
        $cons = "INSERT INTO gallery VALUES(?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->title);

        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of direction before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('legislação', 'inserir', '', $dataAfterExecution);
            return $lastId;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read all gallery 
    function readGallery() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM gallery ";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['title'] = $reg->title;
                $document = new DocumentStorage("galleryPhoto", $reg->id, $this->dbh);
                $arrayData[$i]['photo'] = $document->getDocument();

                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined gallery
    function readDetermGallery() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM gallery WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['title'] = $reg->title;
                $document = new DocumentStorage("galleryPhoto", $reg->id, $this->dbh);
                $arrayData[$i]['photo'] = $document->getDocument();
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update gallery
    function updateGallery() {
        $cons = "UPDATE gallery SET title = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->title);
        $prep->bindparam(2, $this->id);

        //$prep->execute();
        // Get data of direction before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of direction before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('legislação', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            return $e->getMessage();
        }
    }

    // Delete gallery
    function deletegallery() {
        $cons = "DELETE FROM gallery WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of direction before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class direction
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('legislação', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data of a specific gallery
    function getGallery($id) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM gallery WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['title'] = $reg->title;
                $document = new DocumentStorage("galleryPhoto", $reg->id, $this->dbh);
                $arrayData[$i]['photo'] = $document->getDocument();
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $galleryData = '';
        $cons = "SELECT * FROM gallery WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $galleryData = "id" . $reg->id . "title" . $reg->title;
                $i++;
            }

            return $galleryData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

    function checkGallery() {
        $i = 0;
        $cons = "SELECT * FROM gallery WHERE title = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->title, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }

            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

}

?>