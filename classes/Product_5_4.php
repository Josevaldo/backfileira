<?php

require_once 'Auditing.php';
require_once 'ProductiveRow54.php';
require_once 'DocumentStorage.php';

class Product_5_4 {

    public $id;
    public $designation;
    public $idProductiveRow;
    public $product_5_4_Photo;
    //public $filePathForDownload = 'http://be.ppn.mep.gov.ao/documents';
    //public $filePathForDownload = 'http://192.168.10.83/documents';
    public $filePathForDownload = 'https://be.ppn.gov.ao/documents';
    public $filePathLocal = '../documents/images/products';
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create product_5_4
    function registerProduct_5_4() {
        $cons = "INSERT INTO product_5_4 VALUES(?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->designation);
        $prep->bindparam(3, $this->idProductiveRow);

        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            $this->id = $this->dbh->lastInsertId();
            $lastId = $this->dbh->lastInsertId();
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfeterAction($lastId);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('product_5_4', 'inserir', '', $dataAfterExecution);
            return $this->id;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return $e->getMessage();
            //return false;
        }
    }

    // Read all product_5_4
    function readProduct_5_4() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM product_5_4";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                //Instancing the class productiveRow54
                $productiveRow = new ProductiveRow54($this->dbh);
                $productiveRow->id = $reg->id_productive_row;
                $productiveRow = $productiveRow->readDeterminedProductiveRow54();
                $arrayData[$i]['productive_type'] = $productiveRow['designation'];

                //Getting the product photo              
                $productPhoto = new DocumentStorage('product_5_4_Photo', $reg->id, $this->dbh);
                $productPhoto = $productPhoto->getDocument();
                $arrayData[$i]['photo'] = $productPhoto;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined productproduct_5_4
    function readDeterminedProduct_5_4() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM product_5_4 WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                //Instancing the class productiveRow54
                $productiveRow = new ProductiveRow54($this->dbh);
                $productiveRow->id = $reg->id_productive_row;
                $productiveRow = $productiveRow->readDeterminedProductiveRow54();
                $arrayData[$i]['productive_type'] = $productiveRow['designation'];
                //Getting the product photo              
                $productPhoto = new DocumentStorage('product_5_4_Photo', $reg->id, $this->dbh);
                $productPhoto = $productPhoto->getDocument();
                $arrayData[$i]['photo'] = $productPhoto;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

    // Update productproduct_5_4
    function updateProduct_5_4() {
        $cons = "UPDATE product_5_4 SET designation = ?,id_productive_row = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation);
        $prep->bindparam(2, $this->idProductiveRow);
        $prep->bindparam(3, $this->id);

        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfeterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfeterAction($this->id);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('produto', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Delete productproduct_5_4
    function deleteProduct_5_4() {
        $cons = "DELETE FROM product_5_4 WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfeterAction($this->id);
        // instance the class user
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('produto', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Create an identification for a product
    function createIdProduct_5_4($size) {
        $keys = array_merge(range(0, 9));
        for ($i = 0; $i < $size; $i++) {
            $this->id .= $keys[array_rand($keys)];
        }
        return $this->id;
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfeterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT * FROM product_5_4 WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['id'] = 'Identificador: ' . $reg->id;
                $arrayData['designation'] = 'Designação: ' . $reg->designation;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

    ///
    function checkProduct_5_4() {
        $i = 0;
        $cons = "SELECT * FROM product_5_4 WHERE designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }

            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

}

?>
