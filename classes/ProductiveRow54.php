<?php

require_once 'Auditing.php';
require_once 'DocumentStorage.php';

class ProductiveRow54 {

    public $id;
    public $designation;
    public $description;
    public $rowPhoto54;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create productiverow54
    function registerProductiveRow54() {
        $cons = "INSERT INTO productive_row_54 VALUES(?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->designation);
        $prep->bindparam(3, $this->description);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            $lastId = $this->dbh->lastInsertId();
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfeterAction($lastId);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('productive row54', 'inserir', '', $dataAfterExecution);
            return $lastId;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

    // Read all productive row54
    function readProductiveRow54() {
        /* require_once "Adherent.php";
          require_once "CategoryProduct.php";
          require_once "Seal.php"; */
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM productive_row_54";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['description'] = $reg->description;
                //Getting the productiveRow54 photo              
                $productPhoto = new DocumentStorage('rowPhoto54', $reg->id, $this->dbh);
                $productPhoto = $productPhoto->getDocument();
                $arrayData[$i]['photo'] = $productPhoto;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined productive row54
    function readDeterminedProductiveRow54() {
        /* require_once "Adherent.php";
          require_once "CategoryProduct.php";
          require_once "Seal.php"; */
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM productive_row_54 WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['description'] = $reg->description;
                //Getting the productiveRow54 photo              
                $productPhoto = new DocumentStorage('rowPhoto54', $reg->id, $this->dbh);
                $productPhoto = $productPhoto->getDocument();
                $arrayData['photo'] = $productPhoto;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return $e->getMessage();
            //return false;
        }
    }

    // Update productive row54
    function updateProductiveRow54() {
        $cons = "UPDATE productive_row_54 SET designation = ?, description = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation);
        $prep->bindparam(2, $this->description);
        $prep->bindparam(3, $this->id);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfeterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfeterAction($this->id);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('productiveRow54', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Delete productive row54
    function deleteProductiveRow54() {
        $cons = "DELETE FROM productive_row_54 WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfeterAction($this->id);
        // instance the class user
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('productivtrow_54', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get determined type of production
    function getDeterminedTypeProduction() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM type_production WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idTypeProduction, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['comment'] = $reg->comment;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return $e->getMessage();
            //return false;
        }
    }

    // Get productive row from determined type of production
    function getProductiveRowFromDeterminedTypeProduction() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM productive_row WHERE id_type_production = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idTypeProduction, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['comment'] = $reg->comment;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return $e->getMessage();
            //return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfeterAction($DataId) {
        $i = 0;
        $produciveData = "";
        $cons = "SELECT * from productive_row_54 WHERE productive_row_54.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $produciveData = "id: " . $reg->id . ", designation, " . $reg->designation . ", description" . $reg->description;
            }
            //Format data of the system element

            return $produciveData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

///
    function checkProductiveRow54() {
        $i = 0;
        $cons = "SELECT * FROM productive_row_54 WHERE designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }

            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

}

?>