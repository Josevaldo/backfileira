<?php

require_once 'Auditing.php';
require_once 'DocumentStorage.php';

class ProductiveRow {

    public $id;
    public $designation;
    public $comment;
    public $idTypeProduction;
    public $rowPhoto;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create productive row
    function registerProductiveRow() {
        $cons = "INSERT INTO productive_row VALUES(?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->designation);
        $prep->bindparam(3, $this->comment);
        $prep->bindparam(4, $this->idTypeProduction);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            $lastId = $this->dbh->lastInsertId();
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfeterAction($lastId);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('fileira', 'inserir', '', $dataAfterExecution);
            return $lastId;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

    // Read all productive row
    function readProductiveRow() {
        /* require_once "Adherent.php";
          require_once "CategoryProduct.php";
          require_once "Seal.php"; */
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM productive_row";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['comment'] = $reg->comment;
                // Get determined type of production
                $this->idTypeProduction = $reg->id_type_production;
                $arrayTypeProduction = $this->getDeterminedTypeProduction();
                $arrayData[$i]['type_production'] = $arrayTypeProduction;
                //Getting the productiveRow photo              
                $productPhoto = new DocumentStorage('rowPhoto', $reg->id, $this->dbh);
                $productPhoto = $productPhoto->getDocument();
                $arrayData[$i]['photo'] = $productPhoto;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined productive row
    function readDeterminedProductiveRow() {
        /* require_once "Adherent.php";
          require_once "CategoryProduct.php";
          require_once "Seal.php"; */
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM productive_row WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['comment'] = $reg->comment;
                // Get determined type of production
                $this->idTypeProduction = $reg->id_type_production;
                $arrayTypeProduction = $this->getDeterminedTypeProduction();
                $arrayData['type_production'] = $arrayTypeProduction;
                //Getting the productiveRow photo              
                $productPhoto = new DocumentStorage('rowPhoto', $reg->id, $this->dbh);
                $productPhoto = $productPhoto->getDocument();
                $arrayData[$i]['photo'] = $productPhoto;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return $e->getMessage();
            //return false;
        }
    }

    // Update productive row
    function updateProductiveRow() {
        $cons = "UPDATE productive_row SET designation = ?,comment = ?,id_type_production = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation);
        $prep->bindparam(2, $this->comment);
        $prep->bindparam(3, $this->idTypeProduction);
        $prep->bindparam(4, $this->id);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfeterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfeterAction($this->id);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('fileira', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Delete productive row
    function deleteProductiveRow() {
        $cons = "DELETE FROM productive_row WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfeterAction($this->id);
        // instance the class user
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('fileira', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get determined type of production
    function getDeterminedTypeProduction() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM type_production WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idTypeProduction, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['comment'] = $reg->comment;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return $e->getMessage();
            //return false;
        }
    }

    // Get productive row from determined type of production
    function getProductiveRowFromDeterminedTypeProduction() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM productive_row WHERE id_type_production = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idTypeProduction, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['comment'] = $reg->comment;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return $e->getMessage();
            //return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfeterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT productive_row.id AS id_productive_row,productive_row.designation AS desi_productive_row,productive_row.comment AS comment,id_type_production,type_production.designation AS desi_type_production 
				FROM productive_row 
				JOIN type_production ON productive_row.id_type_production = type_production.id
				WHERE productive_row.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['id_productive_row'] = 'Identificador: ' . $reg->id_productive_row;
                $arrayData['desi_productive_row'] = 'Fileira: ' . $reg->desi_productive_row;
                $arrayData['comment'] = 'Comentário: ' . $reg->comment;
                $arrayData['id_type_production'] = 'Identificador Tipo produção: ' . $reg->id_type_production;
                $arrayData['desi_type_production'] = 'Tipo produção: ' . $reg->desi_type_production;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

///
    function checkProductiveRow() {
        $i = 0;
        $cons = "SELECT * FROM productive_row WHERE designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }

            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

}

?>