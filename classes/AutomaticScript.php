<?php
require_once 'Auditing.php';
class AutomaticScript
{
	public $id;
	public $yesterday;
	public $filePath;
	public $dbh;
	
	function __construct($dbh)
	{
		$this->dbh = $dbh;
	}
	
	// Delete user connected 
	function deleteUserConnected()
	{
        $this->yesterday = date('Y-m-d',strtotime("-1 days"));
        //$this->dateCreated = $yesterday;
		$cons = "DELETE FROM user_connected WHERE date_create = ?";
		$prep = $this->dbh->prepare($cons);
		//$prep->bindparam(1, $this->dateCreated, PDO::PARAM_STR);
		$prep->bindparam(1, $this->yesterday, PDO::PARAM_STR);
		$prep->execute();
	}
	
	// Delete a CSV file 
	function deleteCsvFile()
	{
        $i = 0;
		$this->filePath = '../documents/csvDocument';
		if(file_exists($this->filePath)){
			$files = scandir($this->filePath);
			foreach($files as $file){
				if(($file != '.') AND ($file != '..')){
					$fName = $this->filePath.'/'.$file;
					if($fName) unlink($fName);
					$i++;
				} 
			}
		}else $i = 0;
		$r = $i;
	}
}
?>
