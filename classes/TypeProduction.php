<?php

class TypeProduction {

    public $id;
    public $designation;
    public $comment;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create type of Production
    function registerTypeProduction() {
        $cons = "INSERT INTO type_production VALUES(?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->designation);
        $prep->bindparam(3, $this->comment);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read all type of Production
    function readTypeProduction() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM type_production";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['comment'] = $reg->comment;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined type of Production
    function readDeterminedTypeProduction() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM type_production WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['comment'] = $reg->comment;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update type of Production
    function updateTypeProduction() {
        $cons = "UPDATE type_production SET designation = ?,comment = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation);
        $prep->bindparam(2, $this->comment);
        $prep->bindparam(3, $this->id);
        //$prep->execute();
        try {
            $prep->execute();
            //record update
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Delete typeof production
    function deleteTypeProduction() {
        $cons = "DELETE FROM type_production WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    //
    function checkTypeProduct() {
        $i = 0;
        $cons = "SELECT * FROM type_production WHERE designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }

            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

}

?>