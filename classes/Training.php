<?php

require_once 'Auditing.php';
require_once 'TrainingIniciative.php';
require_once 'Province.php';

class Training {

    public $id;
    public $quantMale;
    public $quantFemale;
    public $quantFormation;
    public $idTrainginInitiative;
    public $idProvince;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create training
    function registerTraining() {
        $cons = "INSERT INTO training VALUES(?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->quantMale);
        $prep->bindparam(3, $this->quantFemale);
        $prep->bindparam(4, $this->quantFormation);
        $prep->bindparam(5, $this->idTrainginInitiative);
        $prep->bindparam(6, $this->idProvince);
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfeterAction($lastId);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('training', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read all training
    function readTraining() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM training";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['quant_male'] = $reg->quant_male;
                $arrayData[$i]['quant_female'] = $reg->quant_female;
                $arrayData[$i]['quant_formation'] = $reg->quant_formation;
                //Instancing the trainingInitiative class
                $trainingInitiative = new TrainingIniciative($this->dbh);
                $trainingInitiative->id = $reg->id_training_initiative;
                $arrayData[$i]['training_initiative'] = $trainingInitiative->readDetermTrainingIniti();

                //Instancing the province class
                $province = new Province($this->dbh);
                $province->id = $reg->id_province;
                $arrayData[$i]['province'] = $province->readDeterminedProvince();

                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined training
    function readDetermTraining() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM training WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                $arrayData['quant_male'] = $reg->quant_male;
                $arrayData['quant_female'] = $reg->quant_female;
                $arrayData['quant_formation'] = $reg->quant_formation;
                //Instancing the trainingInitiative class
                $trainingInitiative = new TrainingIniciative($this->dbh);
                $trainingInitiative->id = $reg->id_training_initiative;
                $arrayData['training_initiative'] = $trainingInitiative->readDetermTrainingIniti();

                //Instancing the province class
                $province = new Province($this->dbh);
                $province->id = $reg->id_province;
                $arrayData['province'] = $province->readDeterminedProvince();
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update training
    function updateTraining() {
        $cons = "UPDATE training SET quant_male = ?, quant_female = ?,quant_formation = ?, id_training_initiative = ?,id_province = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->quantMale);
        $prep->bindparam(2, $this->quantFemale);
        $prep->bindparam(3, $this->quantFormation);
        $prep->bindparam(4, $this->idTrainginInitiative);
        $prep->bindparam(5, $this->idProvince);
        $prep->bindparam(6, $this->id);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfeterAction($this->id);
        try {
            $prep->execute();

            //record update
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfeterAction($this->id);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('training', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Delete training
    function deleteTraining() {
        $naProvince = 20;
        $cons = "DELETE FROM training WHERE id = ?";
//        $cons = "DELETE FROM province WHERE id = ? AND id != ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
//        $prep->bindparam(2, $naProvince, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfeterAction($this->id);
        // instance the class user
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('training', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfeterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT * FROM trainings WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['id'] = 'Identificador: ' . $reg->id;
                $arrayData['designation'] = 'Designação: ' . $reg->designation;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

//
    function checkTraining() {
        $i = 0;
        $cons = "SELECT * FROM training WHERE designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }

            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

}

?>
