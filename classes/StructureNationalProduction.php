<?php

require_once 'Auditing.php';
require_once 'ProductiveRow.php';
require_once 'ProductiveSubRow.php';
require_once 'Product.php';
require_once 'SubProduct.php';
require_once 'UnitMeasure.php';

class StructureNationalProduction {

    public $id;
    public $idProductiveRow;
    public $idProductiveSubRow;
    public $idProduct;
    public $idSubProduct;
    public $idUnitMeasure;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create Structure National Production
    function registerStructureNationalProduction() {
        $cons = "INSERT INTO structure_national_production VALUES(?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idProductiveRow);
        $prep->bindparam(2, $this->idProductiveSubRow);
        $prep->bindparam(3, $this->idProduct);
        $prep->bindparam(4, $this->idSubProduct);
        $prep->bindparam(5, $this->idUnitMeasure);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->idProductiveRow . '-' . $this->idProductiveSubRow . '-' . $this->idProduct . '-' . $this->idSubProduct;
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfeterAction($lastId);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Estrutura nacional de produção', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

    // Read all Structure National Production
    function readStructureNationalProduction() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM structure_national_production";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {

                $arrayData[$i]['id'] = [$reg->id_productive_row, $reg->id_productive_sub_row, $reg->id_product, $reg->id_sub_product];
                //Instancing the ProductiveRow Class
                $productiveRow = new ProductiveRow($this->dbh);
                $productiveRow->id = $reg->id_productive_row;
                $productiveRowData = $productiveRow->readDeterminedProductiveRow();
                $arrayData[$i]['productive_row'] = $productiveRowData;
                //Instancing the ProductiveSubRow Class
                $productiveSubRow = new ProductiveSubRow($this->dbh);
                $productiveSubRow->id = $reg->id_productive_sub_row;
                $productiveSubRowData = $productiveSubRow->readDeterminedProductiveSubRow();
                $arrayData[$i]['productive_sub_row'] = $productiveSubRowData;
                //Instancing the Product Class
                $produc = new Product($this->dbh);
                $produc->id = $reg->id_product;
                $productData = $produc->readDeterminedProduct();
                $arrayData[$i]['product'] = $productData;
                //Instancing the SubProduct Class
                $subProduct = new SubProduct($this->dbh);
                $subProduct->id = $reg->id_sub_product;
                $subProducData = $subProduct->readDeterminedSubProduct();
                $arrayData[$i]['sub_product'] = $subProducData;
                //Instancing the Unit Measure Class
                $UnitMeasure = new UnitMeasure($this->dbh);
                $UnitMeasure->id = $reg->id_unit_measure;
                $UnitMeasureData = $UnitMeasure->readDeterminedUnitMeasure();
                $arrayData[$i]['unit_measure'] = $UnitMeasureData;

                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined Structure National Production
    function readDeterminedStructureNationalProduction() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM structure_national_production 
				WHERE id_productive_row = ?
				AND id_productive_sub_row = ?
				AND id_product = ?
				AND id_sub_product = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idProductiveRow, PDO::PARAM_STR);
        $prep->bindparam(2, $this->idProductiveSubRow, PDO::PARAM_STR);
        $prep->bindparam(3, $this->idProduct, PDO::PARAM_STR);
        $prep->bindparam(4, $this->idSubProduct, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = [$reg->id_productive_row, $reg->id_productive_sub_row, $reg->id_product, $reg->id_sub_product];
                //Instancing the ProductiveRow Class
                $productiveRow = new ProductiveRow($this->dbh);
                $productiveRow->id = $reg->id_productive_row;
                $productiveRowData = $productiveRow->readDeterminedProductiveRow();
                $arrayData['productive_row'] = $productiveRowData;
                //Instancing the ProductiveSubRow Class
                $productiveSubRow = new ProductiveSubRow($this->dbh);
                $productiveSubRow->id = $reg->id_productive_sub_row;
                $productiveSubRowData = $productiveSubRow->readDeterminedProductiveSubRow();
                $arrayData['productive_sub_row'] = $productiveSubRowData;
                //Instancing the Product Class
                $produc = new Product($this->dbh);
                $produc->id = $reg->id_product;
                $productData = $produc->readDeterminedProduct();
                $arrayData['product'] = $productData;
                //Instancing the SubProduct Class
                $subProduct = new SubProduct($this->dbh);
                $subProduct->id = $reg->id_sub_product;
                $subProducData = $subProduct->readDeterminedSubProduct();
                $arrayData['sub_product'] = $subProducData;
                //Instancing the Unit Measure Class
                $UnitMeasure = new UnitMeasure($this->dbh);
                $UnitMeasure->id = $reg->id_unit_measure;
                $UnitMeasureData = $UnitMeasure->readDeterminedUnitMeasure();
                $arrayData['unit_measure'] = $UnitMeasureData;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return $e->getMessage();
            //return false;
        }
    }

    // Update Structure National Production
    function updateStructureNationalProduction() {
        $cons = "UPDATE structure_national_production SET id_productive_row = ?,id_productive_sub_row = ?,id_product = ?,id_sub_product = ?,id_unit_measure = ? WHERE id_productive_row = ? AND id_productive_sub_row = ? AND id_product = ? AND id_sub_product = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idProductiveRow);
        $prep->bindparam(2, $this->idProductiveSubRow);
        $prep->bindparam(3, $this->idProduct);
        $prep->bindparam(4, $this->idSubProduct);
        $prep->bindparam(5, $this->idUnitMeasure);
//        $prep->bindparam(6, $this->id['id_productive_row']);
//        $prep->bindparam(7, $this->id['id_productive_sub_row']);
//        $prep->bindparam(8, $this->id['id_product']);
//        $prep->bindparam(9, $this->id['id_sub_product']);
        $prep->bindparam(6, $this->id[0]);
        $prep->bindparam(7, $this->id[1]);
        $prep->bindparam(8, $this->id[2]);
        $prep->bindparam(9, $this->id[3]);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
//        $id = $this->id['id_productive_row'] . '-' . $this->id['id_productive_sub_row'] . '-' . $this->id['id_product'] . '-' . $this->id['id_sub_product'];
        $id = $this->id[0] . '-' . $this->id[1] . '-' . $this->id[2] . '-' . $this->id[3];
        $dataBeforeExecution = $this->getDataBeforeAfeterAction($id);
        try {
            $prep->execute();
            //record update
            // Get data of communal before and after the execution of an action
            $id = $this->idProductiveRow . '-' . $this->idProductiveSubRow . '-' . $this->idProduct . '-' . $this->idSubProduct;
            $dataAfterExecution = $this->getDataBeforeAfeterAction($id);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('Estrutura nacional de produção', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return false;
            return $e->getMessage();
        }
    }

    // Delete Structure National Production
    function deleteStructureNationalProduction() {
        $cons = "DELETE FROM structure_national_production WHERE id_productive_row = ? AND id_productive_sub_row = ? AND id_product = ? AND id_sub_product = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idProductiveRow, PDO::PARAM_STR);
        $prep->bindparam(2, $this->idProductiveSubRow, PDO::PARAM_STR);
        $prep->bindparam(3, $this->idProduct, PDO::PARAM_STR);
        $prep->bindparam(4, $this->idSubProduct, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $id = $this->idProductiveRow . '-' . $this->idProductiveSubRow . '-' . $this->idProduct . '-' . $this->idSubProduct;
        $dataBeforeExecution = $this->getDataBeforeAfeterAction($id);
        // instance the class user
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('Estrutura nacional de produção', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfeterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cc = explode("-", $DataId);
        $cons = "SELECT productive_row.id AS id_productive_row,productive_row.designation AS desi_productive_row,productive_sub_row.id AS id_productive_sub_row,productive_sub_row.designation AS desi_productive_sub_row,
				product.id AS id_product,product.designation AS desi_product,sub_product.id AS id_sub_product,sub_product.designation AS desi_sub_product,unit_measure.id AS id_unit_measure,unit_measure.symbol AS symbol
				FROM structure_national_production 
				JOIN productive_row ON productive_row.id = structure_national_production.id_productive_row
				JOIN productive_sub_row ON productive_sub_row.id = structure_national_production.id_productive_sub_row
				JOIN product ON product.id = structure_national_production.id_product
				JOIN sub_product ON sub_product.id = structure_national_production.id_sub_product
				JOIN unit_measure ON unit_measure.id = structure_national_production.id_unit_measure
				WHERE productive_row.id = ?
				AND productive_sub_row.id = ?
				AND product.id = ?
				AND sub_product.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $cc[0], PDO::PARAM_STR);
        $prep->bindparam(2, $cc[1], PDO::PARAM_STR);
        $prep->bindparam(3, $cc[2], PDO::PARAM_STR);
        $prep->bindparam(4, $cc[3], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['id_productive_row'] = 'Identificador fileira: ' . $reg->id_productive_row;
                $arrayData['desi_productive_row'] = 'Fileira: ' . $reg->desi_productive_row;
                $arrayData['id_productive_sub_row'] = 'Identificador sub fileira: ' . $reg->id_productive_sub_row;
                $arrayData['desi_productive_sub_row'] = 'Sub fileira: ' . $reg->desi_productive_sub_row;
                $arrayData['id_product'] = 'Identificador produto: ' . $reg->id_product;
                $arrayData['desi_product'] = 'Produto: ' . $reg->desi_product;
                $arrayData['id_sub_product'] = 'Identificador sub prodduto: ' . $reg->id_sub_product;
                $arrayData['desi_sub_product'] = 'Sub produto: ' . $reg->desi_sub_product;
                $arrayData['id_unit_measure'] = 'Identificador unidade de medida: ' . $reg->id_unit_measure;
                $arrayData['symbol'] = 'Símbolo: ' . $reg->symbol;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

}

?>