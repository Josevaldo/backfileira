<?php

require_once 'Auditing.php';
require_once 'User.php';
require_once 'Role.php';
require_once 'Service.php';

class Profile {

    public $id;
    public $idUser;
    public $idRole;
    public $idService;
    public $observation;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create profile
    function registerProfile() {
        $cons = "INSERT INTO profile VALUES(?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idUser);
        $prep->bindparam(2, $this->idRole);
        $prep->bindparam(3, $this->idService);
        $prep->bindparam(4, $this->observation);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            //$lastId = $this->dbh->lastInsertId();
            $this->id = array($this->idUser, $this->idRole, $this->idService);
            // Get data of profile before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class Profile
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('perfil', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return false;
            return $e->getMessage();
        }
    }

    // Read all Profile
    function readProfile() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM profile";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                // Get data of a spefic user
                $userData = new User($this->dbh);
                $arrayUserData = $userData->getDataUser($reg->id_user);
                $arrayData[$i]['user'] = $arrayUserData;
                //$arrayData[$i]['id_role'] = $reg->id_role;
                // Get data of a spefic role
                $roleData = new Role($this->dbh);
                $arrayRoleData = $roleData->getDataRole($reg->id_role);
                $arrayData[$i]['role'] = $arrayRoleData;
                // Get data of a spefic service
                $roleService = new Service($this->dbh);
                $arrayServiceData = $roleService->getDataService($reg->id_service);
                $arrayData[$i]['service'] = $arrayServiceData;
                $arrayData[$i]['observation'] = $reg->observation;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined Profile
    function readDeterminedProfile() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM profile	WHERE id_user = ? AND id_role = ? AND id_service = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        $prep->bindparam(3, $this->id[2], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                // Get data of a spefic user
                $userData = new User($this->dbh);
                $arrayUserData = $userData->getDataUser($reg->id_user);
                $arrayData[$i]['user'] = $arrayUserData;
                //$arrayData[$i]['id_role'] = $reg->id_role;
                // Get data of a spefic role
                $roleData = new Role($this->dbh);
                $arrayRoleData = $roleData->getDataRole($reg->id_role);
                $arrayData[$i]['role'] = $arrayRoleData;
                // Get data of a spefic service
                $roleService = new Service($this->dbh);
                $arrayServiceData = $roleService->getDataService($reg->id_service);
                $arrayData[$i]['service'] = $arrayServiceData;
                $arrayData[$i]['observation'] = $reg->observation;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update Profile
    function updateProfile() {
        $cons = "UPDATE profile SET id_role = ?, id_user = ?, id_service = ?, observation = ? WHERE id_role = ? AND id_user = ? AND id_service = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idRole);
        $prep->bindparam(2, $this->idUser);
        $prep->bindparam(3, $this->idService);
        $prep->bindparam(4, $this->observation);
        $prep->bindparam(5, $this->id[0]);
        $prep->bindparam(6, $this->id[1]);
        $prep->bindparam(7, $this->id[2]);
        //$prep->execute();
        // Get data of profile before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of profile before and after the execution of an action
            $this->id = array($this->idUser, $this->idRole, $this->idService);
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class Profile
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('perfil', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
            //return $e->getMessage();
        }
    }

    // Delete Profile
    function deleteProfile() {
        $cons = "DELETE FROM profile WHERE id_user = ? AND id_role = ? AND id_service = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        $prep->bindparam(3, $this->id[2], PDO::PARAM_STR);
        //$prep->execute();
        // Get data of profile before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class Profile
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('perfil', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get user profile
    function getUserProfile($idUser) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT role.acronym as role_acronym, role.designation as role_designation, service.designation as service_designation, service.acronym as service_acronym, service.domain_name as service_domain, service.comment as service_comment FROM profile JOIN role on profile.id_role=role.id JOIN service on profile.id_service=service.id WHERE profile.id_user = ? ";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $idUser, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['role_acronym'] = $reg->role_acronym;
                $arrayData[$i]['role_designation'] = $reg->role_designation;
                $arrayData[$i]['service_acronym'] = $reg->service_acronym;
                $arrayData[$i]['service_designation'] = $reg->service_designation;
                $arrayData[$i]['service_domain_name'] = $reg->service_domain;
                $arrayData[$i]['service_comment'] = $reg->service_comment;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Assigned roles to user when creating this one, in order to create a new profile
    function createUserProfile($idUserCreated, $role, $service) {
        $arrayData = [];
        if ((is_array($role)) & (is_array($service)) || (!empty($role)) & (!empty($service))) {
            $this->idUser = $idUserCreated;
            $this->observation = "Perfil criado no momento do registo Utilizador";

            foreach ($role as $rr) {
                foreach ($role as $ss) {
                    $this->idRole = $rr;
                    $this->idService = $ss;
                    $this->registerProfile();
                }
            }
        }
    }

    // Get data profile
    function getDataProfile($id) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM profile	WHERE id_user = ? AND id_role = ? AND id_service = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $id[1], PDO::PARAM_STR);
        $prep->bindparam(3, $id[2], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                // Get data of a spefic user
                $userData = new User($this->dbh);
                $arrayUserData = $userData->getDataUser($reg->id_user);
                $arrayData[$i]['user'] = $arrayUserData;
                //$arrayData[$i]['id_role'] = $reg->id_role;
                // Get data of a spefic role
                $roleData = new Role($this->dbh);
                $arrayRoleData = $roleData->getDataRole($reg->id_role);
                $arrayData[$i]['role'] = $arrayRoleData;
                // Get data of a spefic service
                $roleService = new Service($this->dbh);
                $arrayServiceData = $roleService->getDataService($reg->id_service);
                $arrayData[$i]['service'] = $arrayServiceData;
                $arrayData[$i]['observation'] = $reg->observation;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) {
        $i = 0;
        $stringData = "";
        $cons = "SELECT role.acronym as role_acronym, role.designation as role_designation, service.designation as service_designation, service.acronym as service_acronym, service.domain_name as service_domain, service.comment as service_comment FROM profile JOIN role on profile.id_role=role.id JOIN service on profile.id_service=service.id join user on user.id=profile.id_user WHERE profile.id_user = ? AND profile.id_role = ? AND profile.id_service = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId[0], PDO::PARAM_STR);
        $prep->bindparam(2, $DataId[1], PDO::PARAM_STR);
        $prep->bindparam(3, $DataId[2], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $stringData = "service_acronym: " . $reg->service_acronym . ", service_designation: " . $reg->service_designation . ", Domain name: " . $reg->service_domain . ", Comment: " . $reg->service_comment;
                $i++;
            }
            return $stringData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

}

?>