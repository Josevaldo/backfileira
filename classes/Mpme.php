<?php

require_once 'Province.php';

class Mpme {

    public $id;
    public $idProvince;
    public $quantMicro;
    public $quantSmall;
    public $quantAverage;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create mpme
    function registerMpme() {
        $cons = "INSERT INTO mpme VALUES(?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->idProvince);
        $prep->bindparam(3, $this->quantMicro);
        $prep->bindparam(4, $this->quantSmall);
        $prep->bindparam(5, $this->quantAverage);

        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('mpme', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    // Read all mpme
    function readMpme() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM mpme";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                //Stancing the province Class
                $province = new Province($this->dbh);
                $province->id = $reg->id_province;
                $arrayData[$i]['province'] = $province->readDeterminedProvince()['designation'];
                $arrayData[$i]['quant_micro'] = $reg->quant_micro;
                $arrayData[$i]['quant_small'] = $reg->quant_small;
                $arrayData[$i]['quant_average'] = $reg->quant_average;
                $arrayData[$i]['total'] = ($reg->quant_micro + $reg->quant_small + $reg->quant_average);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            return false;
        }
    }

    // Read determined mpme
    function readDeterminedMpme() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM mpme WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                //Stancing the province Class
                $province = new Province($this->dbh);
                $province->id = $reg->id_province;
                $arrayData['province'] = $province->readDeterminedProvince()['designation'];
                $arrayData['quant_micro'] = $reg->quant_micro;
                $arrayData['quant_small'] = $reg->quant_small;
                $arrayData['quant_average'] = $reg->quant_average;
                $arrayData['total'] = ($reg->quant_micro + $reg->quant_small + $reg->quant_average);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            return false;
        }
    }

    // Update mpme
    function updateMpme() {
        $cons = "UPDATE mpme SET id_province = ?, quant_micro = ?, quant_small = ?, quant_average = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idProvince);
        $prep->bindparam(2, $this->quantMicro);
        $prep->bindparam(3, $this->quantSmall);
        $prep->bindparam(4, $this->quantAverage);
        $prep->bindparam(5, $this->id);
//         Get data of department before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();

//            //record update
//            // Get data of ppn before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
//            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('mpme', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
            //true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    // Delete mpme
    function deleteMpme() {
        $cons = "DELETE FROM mpme WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        // Get data of ppn before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class department
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('mpme', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    // Get data of a specific mpme
    function getDataMpme($id) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM mpme WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                //Stancing the province Class
                $province = new Province($this->dbh);
                $province->id = $reg->id_province;
                $arrayData['province'] = $province->readDeterminedProvince()['designation'];
                $arrayData['quant_micro'] = $reg->quant_micro;
                $arrayData['quant_small'] = $reg->quant_small;
                $arrayData['quant_average'] = $reg->quant_average;
                $arrayData['total'] = ($reg->quant_micro + $reg->quant_small + $reg->quant_average);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT department.name AS dep_name,department.acronym AS dep_acronym,department.description AS dep_desc,direction.description AS dir_desc FROM department 
				JOIN direction ON department.id_direction = direction.id WHERE department.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                //$arrayData['id'] = 'Identificador: '.$reg->id;
                $arrayData['name'] = 'Nome: ' . $reg->dep_name;
                $arrayData['acronym'] = 'Sigla: ' . $reg->dep_acronym;
                $arrayData['description'] = 'Descrição: ' . $reg->dep_desc;
                $arrayData['Direction'] = 'Direção: ' . $reg->dir_desc;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

    function checkmpme() {
        $i = 0;
        $cons = "SELECT * FROM mpme WHERE designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }

            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

}

?>