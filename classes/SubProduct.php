<?php

require_once 'Auditing.php';
require_once 'DocumentStorage.php';

class SubProduct {

    public $id;
    public $designation;
    public $customsTariff;
    public $prodesiProduct;
    public $subProductPhoto;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create sub product
    function registerSubProduct() {
        $cons = "INSERT INTO sub_product VALUES(?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->designation);
        $prep->bindparam(3, $this->customsTariff);
        $prep->bindparam(4, $this->prodesiProduct);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            //$arrayData = [];
            //$arrayData['id'] = $this->dbh->lastInsertId();
            //return $arrayData;
            $this->id = $this->dbh->lastInsertId();
            $lastId = $this->dbh->lastInsertId();
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfeterAction($lastId);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('sub produto', 'inserir', '', $dataAfterExecution);
            return $this->id;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

    // Read all sub product
    function readSubProduct() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM sub_product";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['customs_tariff'] = $reg->customs_tariff;
                $arrayData[$i]['prodesi_product'] = $reg->prodesi_product;
                //Getting the product photo              
                $productPhoto = new DocumentStorage('subProductPhoto', $reg->id, $this->dbh);
                $productPhoto = $productPhoto->getDocument();
                $arrayData[$i]['photo'] = $productPhoto;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined sub product
    function readDeterminedSubProduct() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM sub_product WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['customs_tariff'] = $reg->customs_tariff;
                $arrayData['prodesi_product'] = $reg->prodesi_product;
                //Getting the product photo              
                $productPhoto = new DocumentStorage('subProductPhoto', $reg->id, $this->dbh);
                $productPhoto = $productPhoto->getDocument();
                $arrayData[$i]['photo'] = $productPhoto;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return $e->getMessage();
            //return false;
        }
    }

    // Update sub product
    function updateSubProduct() {
        $cons = "UPDATE sub_product SET designation = ?,customs_tariff = ?,prodesi_product = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation);
        $prep->bindparam(2, $this->customsTariff);
        $prep->bindparam(3, $this->prodesiProduct);
        $prep->bindparam(4, $this->id);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfeterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfeterAction($this->id);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('sub produto', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Delete sub product
    function deleteSubProduct() {
        $cons = "DELETE FROM sub_product WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfeterAction($this->id);
        // instance the class user
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('sub produto', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    //
    function getSubProduct($idSubProduct) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM sub_product WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $idSubProduct, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['customs_tariff'] = $reg->customs_tariff;
                $arrayData['prodesi_product'] = $reg->prodesi_product;
                //Getting the product photo              
                $productPhoto = new DocumentStorage('subProductPhoto', $reg->id, $this->dbh);
                $productPhoto = $productPhoto->getDocument();
                $arrayData[$i]['photo'] = $productPhoto;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return $e->getMessage();
            //return false;
        }
    }

    //

    function getDataBeforeAfeterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT * FROM sub_product WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['id'] = 'Identificador: ' . $reg->id;
                $arrayData['designation'] = 'Designação: ' . $reg->designation;
                $arrayData['customs_tariff'] = 'Código pautal: ' . $reg->customs_tariff;
                $arrayData['prodesi_product'] = 'Prodesi: ' . $reg->prodesi_product;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

    ///
    function checkSubPruduct() {
        $i = 0;
        $cons = "SELECT * FROM sub_product WHERE designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }

            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

}

?>
