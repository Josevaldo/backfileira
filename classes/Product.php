<?php

require_once 'Auditing.php';
require_once 'DocumentStorage.php';

class Product {

    public $id;
    public $designation;
    public $customsTariff;
    public $prodesiProduct;
    public $productPhoto;
    //public $filePathForDownload = 'http://be.ppn.mep.gov.ao/documents';
    //public $filePathForDownload = 'http://192.168.10.83/documents';
    public $filePathForDownload = 'https://be.ppn.gov.ao/documents';
    public $filePathLocal = '../documents/images/products';
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create product
    function registerProduct() {
        $cons = "INSERT INTO product VALUES(?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->designation);
        $prep->bindparam(3, $this->customsTariff);
        $prep->bindparam(4, $this->prodesiProduct);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            $this->id = $this->dbh->lastInsertId();
            $lastId = $this->dbh->lastInsertId();
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfeterAction($lastId);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('produto', 'inserir', '', $dataAfterExecution);
            return $this->id;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return $e->getMessage();
            //return false;
        }
    }

    // Read all product
    function readProduct() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM product";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['customs_tariff'] = $reg->customs_tariff;
                $arrayData[$i]['prodesi_product'] = $reg->prodesi_product;
                //Getting the product photo              
                $productPhoto = new DocumentStorage('productPhoto', $reg->id, $this->dbh);
                $productPhoto = $productPhoto->getDocument();
                $arrayData[$i]['photo'] = $productPhoto;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined product
    function readDeterminedProduct() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM product WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['id'] = $reg->id;
                $arrayData['designation'] = $reg->designation;
                $arrayData['customs_tariff'] = $reg->customs_tariff;
                $arrayData['prodesi_product'] = $reg->prodesi_product;
                //Getting the product photo              
                $productPhoto = new DocumentStorage('productPhoto', $reg->id, $this->dbh);
                $productPhoto = $productPhoto->getDocument();
                $arrayData[$i]['photo'] = $productPhoto;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

    // Update product
    function updateProduct() {
        $cons = "UPDATE product SET designation = ?,customs_tariff = ?,prodesi_product = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation);
        $prep->bindparam(2, $this->customsTariff);
        $prep->bindparam(3, $this->prodesiProduct);
        $prep->bindparam(4, $this->id);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfeterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfeterAction($this->id);
            // instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('produto', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Delete product
    function deleteProduct() {
        $cons = "DELETE FROM product WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfeterAction($this->id);
        // instance the class user
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('produto', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Create an identification for a product
    function createIdProduct($size) {
        $keys = array_merge(range(0, 9));
        for ($i = 0; $i < $size; $i++) {
            $this->id .= $keys[array_rand($keys)];
        }
        return $this->id;
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfeterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT * FROM product WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['id'] = 'Identificador: ' . $reg->id;
                $arrayData['designation'] = 'Designação: ' . $reg->designation;
                $arrayData['customs_tariff'] = 'Código pautal: ' . $reg->customs_tariff;
                $arrayData['prodesi_product'] = 'Prodesi: ' . $reg->prodesi_product;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

    ///
    function checkPruduct() {
        $i = 0;
        $cons = "SELECT * FROM product WHERE designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }

            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

}

?>
