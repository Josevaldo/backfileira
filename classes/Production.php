
<?php

require_once 'Auditing.php';
require_once 'Province.php';
require_once 'ProductionPeriod.php';
require_once 'ProductiveRow.php';
require_once 'ProductiveSubRow.php';
require_once 'Product.php';
require_once 'SubProduct.php';
require_once 'TypeProduction.php';
require_once 'TradeBalanceCategory.php';

class Production {

    public $id;
    public $registrationDate;
    public $quantity;
    public $comment;
    public $availableArea;
    public $actionType;
    public $explorationType;
    public $idProvince;
    public $idProdPeriod;
    public $idProductiveRow;
    public $idProductiveSubRow;
    public $idProduct;
    public $idSubProduct;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

// Create production
    function registerProduction() {
        $cons = "INSERT INTO production VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->registrationDate);
        $prep->bindparam(3, $this->quantity);
        $prep->bindparam(4, $this->comment);
        $prep->bindparam(5, $this->availableArea);
        $prep->bindparam(6, $this->actionType);
        $prep->bindparam(7, $this->explorationType);
        $prep->bindparam(8, $this->idProvince);
        $prep->bindparam(9, $this->idProdPeriod);
        $prep->bindparam(10, $this->idProductiveRow);
        $prep->bindparam(11, $this->idProductiveSubRow);
        $prep->bindparam(12, $this->idProduct);
        $prep->bindparam(13, $this->idSubProduct);

//$prep->execute();
        try {
            $prep->execute();
//record inserted
            $lastId = $this->dbh->lastInsertId();
// Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfeterAction($lastId);
// instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('produção', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
//return $e->getMessage();
            return false;
        }
    }

// Read all production
    function readProduction() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM production";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['registration_date'] = $reg->registration_date;
                $arrayData[$i]['quantity'] = $reg->quantity;
                $arrayData[$i]['comment'] = $reg->comment;
                $arrayData[$i]['available_area'] = $reg->available_area;
                $arrayData[$i]['action_type'] = $reg->action_type;
                $arrayData[$i]['exploration_type'] = $reg->exploration_type;
//Instancing the Province Class
                $province = new Province($this->dbh);
                $province->id = $reg->id_province;
                $provinceData = $province->readDeterminedProvince();
                $arrayData[$i]['province'] = $provinceData;
//Instancing the ProductiveRow Class
                $productiveRow = new ProductiveRow($this->dbh);
                $productiveRow->id = $reg->id_productive_row;
                $productiveRowData = $productiveRow->readDeterminedProductiveRow();
                $arrayData[$i]['productive_row'] = $productiveRowData;
//Instancing the ProductiveSubRow Class
                $productiveSubRow = new ProductiveSubRow($this->dbh);
                $productiveSubRow->id = $reg->id_productive_sub_row;
                $productiveSubRowData = $productiveSubRow->readDeterminedProductiveSubRow();
                $arrayData[$i]['productive_sub_row'] = $productiveSubRowData;
//Instancing the Product Class
                $produc = new Product($this->dbh);
                $produc->id = $reg->id_product;
                $productData = $produc->readDeterminedProduct();
                $arrayData[$i]['product'] = $productData;
//Instancing the SubProduct Class
                $subProduct = new SubProduct($this->dbh);
                $subProduct->id = $reg->id_sub_product;
                $subProducData = $subProduct->readDeterminedSubProduct();
                $arrayData[$i]['sub_product'] = $subProducData;
//Instancing the Production Period Class
                $productionPeriod = new ProductionPeriod($this->dbh);
                $productionPeriod->id = $reg->id_production_period;
                $productionPeriodData = $productionPeriod->readDeterProductionPeriod();
                $arrayData[$i]['prod_period'] = $productionPeriodData;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

// Read determined production
    function readDeterminedProduction() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM production WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['registration_date'] = $reg->registration_date;
                $arrayData[$i]['quantity'] = $reg->quantity;
                $arrayData[$i]['comment'] = $reg->comment;
                $arrayData[$i]['available_area'] = $reg->available_area;
                $arrayData[$i]['action_type'] = $reg->action_type;
                $arrayData[$i]['exploration_type'] = $reg->exploration_type;
//Instancing the Province Class
                $province = new Province($this->dbh);
                $province->id = $reg->id_province;
                $provinceData = $province->readDeterminedProvince();
                $arrayData[$i]['province'] = $provinceData;
//Instancing the ProductiveRow Class
                $productiveRow = new ProductiveRow($this->dbh);
                $productiveRow->id = $reg->id_productive_row;
                $productiveRowData = $productiveRow->readDeterminedProductiveRow();
                $arrayData[$i]['productive_row'] = $productiveRowData;
//Instancing the ProductiveSubRow Class
                $productiveSubRow = new ProductiveSubRow($this->dbh);
                $productiveSubRow->id = $reg->id_productive_sub_row;
                $productiveSubRowData = $productiveSubRow->readDeterminedProductiveSubRow();
                $arrayData[$i]['productive_sub_row'] = $productiveSubRowData;
//Instancing the Product Class
                $produc = new Product($this->dbh);
                $produc->id = $reg->id_product;
                $productData = $produc->readDeterminedProduct();
                $arrayData[$i]['product'] = $productData;
//Instancing the SubProduct Class
                $subProduct = new SubProduct($this->dbh);
                $subProduct->id = $reg->id_sub_product;
                $subProducData = $subProduct->readDeterminedSubProduct();
                $arrayData[$i]['sub_product'] = $subProducData;
//Instancing the Production Period Class
                $productionPeriod = new ProductionPeriod($this->dbh);
                $productionPeriod->id = $reg->id_production_period;
                $productionPeriodData = $productionPeriod->readDeterProductionPeriod();
                $arrayData[$i]['prod_period'] = $productionPeriodData;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
//return $e->getMessage();
            return false;
        }
    }

// Update production
    function updateProduction() {
        $cons = "UPDATE production SET quantity = ?,comment = ?,available_area = ?,action_type = ?,exploration_type = ?,id_province  = ?,id_productive_row =?,id_productive_sub_row = ?, id_product = ?, id_sub_product = ?, id_production_period = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->quantity);
        $prep->bindparam(2, $this->comment);
        $prep->bindparam(3, $this->availableArea);
        $prep->bindparam(4, $this->actionType);
        $prep->bindparam(5, $this->explorationType);
        $prep->bindparam(6, $this->idProvince);
        $prep->bindparam(7, $this->idProductiveRow);
        $prep->bindparam(8, $this->idProductiveSubRow);
        $prep->bindparam(9, $this->idProduct);
        $prep->bindparam(10, $this->idSubProduct);
        $prep->bindparam(11, $this->idProdPeriod);
        $prep->bindparam(12, $this->id);
//$prep->execute();
// Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfeterAction($this->id);
        try {
            $prep->execute();
//record update
// Get data of communal before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfeterAction($this->id);
// instance the class user
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('produtção', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
//return $e->getMessage();
            return false;
        }
    }

// Delete production
    function deleteProduction() {
        $cons = "DELETE FROM production WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
//$prep->execute();
// Get data of communal before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfeterAction($this->id);
// instance the class user
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('produção', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
//record deleted
            return true;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

// Get data before and after the execution of an action
    function getDataBeforeAfeterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT production.id AS id_production,date_production,id_municipal,municipal.designation AS desi_municipal,desi_communal,id_productive_row,productive_row.designation AS desi_productive_row,id_productive_sub_row,
				productive_sub_row.designation AS desi_productive_sub_row,id_product,product.designation AS desi_product,id_sub_product,sub_product.designation AS desi_sub_product,quantity,id_productor,
				productor.designation AS desi_productor,production.comment AS comment,production.validation_state AS production_vs 
				FROM production 
				JOIN municipal ON municipal.id = production.id_municipal
				JOIN productive_row ON productive_row.id = production.id_productive_row
				JOIN productive_sub_row ON productive_sub_row.id = production.id_productive_sub_row
				JOIN product ON product.id = production.id_product
				JOIN sub_product ON sub_product.id = production.id_sub_product
				JOIN productor ON productor.id = production.id_productor
				WHERE production.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
//$i++;
                $arrayData['id'] = 'Identificador: ' . $reg->id_production;
                $arrayData['date_production'] = 'Data: ' . $reg->date_production;
                $arrayData['id_municipal'] = 'Identificador do municipio: ' . $reg->id_municipal;
                $arrayData['desi_municipal'] = 'Municipio: ' . $reg->desi_municipal;
                $arrayData['desi_communal'] = 'Communa: ' . $reg->desi_communal;
                $arrayData['id_productive_row'] = 'Identificador Fileira: ' . $reg->id_productive_row;
                $arrayData['desi_productive_row'] = 'Fileira: ' . $reg->desi_productive_row;
                $arrayData['id_productive_sub_row'] = 'Identificador Sub Fileira: ' . $reg->id_productive_sub_row;
                $arrayData['desi_productive_sub_row'] = 'Sub Fileira: ' . $reg->desi_productive_sub_row;
                $arrayData['id_product'] = 'Identificador Product: ' . $reg->id_product;
                $arrayData['desi_product'] = 'Produto: ' . $reg->desi_product;
                $arrayData['id_sub_product'] = 'Identificador Sub Produto: ' . $reg->id_sub_product;
                $arrayData['desi_sub_product'] = 'Sub Produto: ' . $reg->desi_sub_product;
                $arrayData['quantity'] = 'Área produção: ' . $reg->quantity;
                $arrayData['id_productor'] = 'Identificador Productor: ' . $reg->id_productor;
                $arrayData['desi_productor'] = 'Prodductor: ' . $reg->desi_productor;
                $arrayData['comment'] = 'Comentário: ' . $reg->comment;
                $arrayData['production_vs'] = 'Estado validação: ' . $reg->production_vs;
                $i++;
            }
//Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
//            return $dataReceivedFormated;
            return "";
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
//return $e->getMessage();
            return false;
        }
    }

// get Province
    function getProvince() {
        $arrayProvinceNames = array();
        $province = new Province($this->dbh);
        $provinceData = $province->readProvince();
        foreach ($provinceData as $provinceNames) {
            $arrayProvinceNames[] = $provinceNames['designation'];
        }
        return $arrayProvinceNames;
    }

// get Prodution Period
    function getProdPeriod() {

        $arrayPeriodNames = array();
        $period = new ProductionPeriod($this->dbh);
        $periodData = $period->readProductionPeriod();
        foreach ($periodData as $periodNames) {
            $arrayPeriodNames[] = $periodNames['designation'];
        }
        return $arrayPeriodNames;
    }

// getting production action type
    function getActionType() {

        $arraypProdAction = array();
        $production = new Production($this->dbh);
        $productionData = $production->readProduction();
        foreach ($productionData as $productionAction) {
            $arraypProdAction[] = $productionAction['action_type'];
        }
        return $arraypProdAction;
    }

// getting productionRow
    function getProdRow() {

        $arraypProdRow = array();
        $prodRow = new ProductiveRow($this->dbh);
        $prodRowData = $prodRow->readProductiveRow();
        foreach ($prodRowData as $productionRow) {
            $arraypProdRow[] = $productionRow['designation'];
        }
        return $arraypProdRow;
    }

///
    function getGeneralProdRow($province, $produtiveRow, $period) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT production.quantity as production_value, unit_measure.symbol as unitMeasure, production.available_area, production.action_type FROM province join region ON province.id_region=region.id JOIN production ON production.id_province=province.id JOIN production_period on production.id_production_period=production_period.id join structure_national_production ON structure_national_production.id_productive_row=production.id_productive_row and structure_national_production.id_productive_sub_row=production.id_productive_sub_row AND structure_national_production.id_product=production.id_product AND structure_national_production.id_sub_product=production.id_sub_product JOIN product ON product.id=structure_national_production.id_product JOIN sub_product ON sub_product.id=structure_national_production.id_sub_product join productive_row ON productive_row.id=structure_national_production.id_productive_row join productive_sub_row on productive_sub_row.id=structure_national_production.id_productive_sub_row JOIN unit_measure on unit_measure.id=structure_national_production.id_unit_measure WHERE province.designation = ? AND productive_row.id = ? AND production_period.designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $province, PDO::PARAM_STR);
        $prep->bindparam(2, $produtiveRow, PDO::PARAM_STR);
        $prep->bindparam(3, $period, PDO::PARAM_STR);
//        $prep->bindparam(4, $actionType, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['production_value'] = $reg->production_value;
//                $arrayData[$i]['total_production'] = $this->getTotalProduction($period, $reg->action_type, $produtiveRow);
                $arrayData[$i]['percentage_production'] = $this->getPercentage($this->getTotalProdCampaign($period, $reg->action_type, $produtiveRow), $reg->production_value) . "%";
                $arrayData[$i]['unit_measure'] = $reg->unitMeasure;
                $arrayData[$i]['available_area'] = $reg->available_area;
//                $arrayData[$i]['total_area'] = $this->getTotalArea($period, $reg->action_type, $produtiveRow);
                $arrayData[$i]['percentage_area'] = $this->getPercentage($this->getTotalAreaCampaign($period, $reg->action_type, $produtiveRow), $reg->available_area) . "%";
                $arrayData[$i]['action_type'] = $reg->action_type;
//$arrayData[$i]['campaign'] = $reg->campaign;
                $i++;
            }

            return $arrayData;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

///
    function getGeneralProdRow2($produtiveRow) {
        $i = 0;
        $provinceName = $this->getProvince();
        $periodName = $this->getProdPeriod();
        $actionType = $this->getActionType();
        $arrayData = [];
        foreach ($provinceName as $pn) {
            $arrayData [$i]['province'] = $pn;
            foreach ($periodName as $period) {
                $arrayData [$i]['campaign'] = $period;
                $arrayData [$i]['producion_data'] = $this->getGeneralProdRow($pn, $produtiveRow, $period);
                $i++;
            }
        }
        $arrayData [$i]['sow_total_area_20_21'] = $this->getTotalAreaCampaign("2020/2021", "Semear", $produtiveRow);
        $arrayData [$i]['sow_total_area_21_22'] = $this->getTotalAreaCampaign("2021/2022", "Semear", $produtiveRow);
        $arrayData [$i]['sow_area_variation'] = $this->getVariation($this->getTotalAreaCampaign("2021/2022", "Semear", $produtiveRow), $this->getTotalAreaCampaign("2020/2021", "Semear", $produtiveRow)) . "%";

        $arrayData [$i]['harvest_total_area_20_21'] = $this->getTotalAreaCampaign("2020/2021", "Colher", $produtiveRow);
        $arrayData [$i]['harvest_total_area_21_22'] = $this->getTotalAreaCampaign("2021/2022", "Colher", $produtiveRow);
        $arrayData [$i]['harvest_area_variation'] = $this->getVariation($this->getTotalAreaCampaign("2021/2022", "Colher", $produtiveRow), $this->getTotalAreaCampaign("2020/2021", "Colher", $produtiveRow)) . "%";

        $arrayData [$i]['sow_total_production_20_21'] = $this->getTotalProdCampaign("2020/2021", "Semear", $produtiveRow);
        $arrayData [$i]['sow_total_production_21_22'] = $this->getTotalProdCampaign("2021/2022", "Semear", $produtiveRow);
        $arrayData [$i]['sow_rpduction_variation'] = $this->getVariation($this->getTotalProdCampaign("2021/2022", "Semear", $produtiveRow), $this->getTotalProdCampaign("2020/2021", "Semear", $produtiveRow));

        $arrayData [$i]['harvest_total_production_20_21'] = $this->getTotalProdCampaign("2020/2021", "Colher", $produtiveRow);
        $arrayData [$i]['harvest_total_production_21_22'] = $this->getTotalProdCampaign("2021/2022", "Colher", $produtiveRow);
        $arrayData [$i]['harvest_rpduction_variation'] = $this->getVariation($this->getTotalProdCampaign("2021/2022", "Colher", $produtiveRow), $this->getTotalProdCampaign("2020/2021", "Colher", $produtiveRow)) . "%";
        return $arrayData;
    }

    function getTotalAreaCampaign($campaign, $actionType, $productiveRow) {
        $i = 0;
        $totalArea = 0;
        $arrayData = [];
        $cons = "SELECT production.available_area as production_area FROM province join region ON province.id_region=region.id JOIN production ON production.id_province=province.id JOIN production_period on production.id_production_period=production_period.id join structure_national_production ON structure_national_production.id_productive_row=production.id_productive_row and structure_national_production.id_productive_sub_row=production.id_productive_sub_row AND structure_national_production.id_product=production.id_product AND structure_national_production.id_sub_product=production.id_sub_product JOIN product ON product.id=structure_national_production.id_product JOIN sub_product ON sub_product.id=structure_national_production.id_sub_product join productive_row ON productive_row.id=structure_national_production.id_productive_row join productive_sub_row on productive_sub_row.id=structure_national_production.id_productive_sub_row JOIN unit_measure on unit_measure.id=structure_national_production.id_unit_measure WHERE production_period.designation= ? AND production.action_type= ? AND productive_row.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $campaign, PDO::PARAM_STR);
        $prep->bindparam(2, $actionType, PDO::PARAM_STR);
        $prep->bindparam(3, $productiveRow, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $totalArea += $reg->production_area;
                $i++;
            }
            return $totalArea;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    function getTotalProdCampaign($campaign, $actionType, $productiveRow) {
        $i = 0;
        $totalProduction = 0;
        $arrayData = [];
        $cons = "SELECT production.quantity as production_quant FROM province join region ON province.id_region=region.id JOIN production ON production.id_province=province.id JOIN production_period on production.id_production_period=production_period.id join structure_national_production ON structure_national_production.id_productive_row=production.id_productive_row and structure_national_production.id_productive_sub_row=production.id_productive_sub_row AND structure_national_production.id_product=production.id_product AND structure_national_production.id_sub_product=production.id_sub_product JOIN product ON product.id=structure_national_production.id_product JOIN sub_product ON sub_product.id=structure_national_production.id_sub_product join productive_row ON productive_row.id=structure_national_production.id_productive_row join productive_sub_row on productive_sub_row.id=structure_national_production.id_productive_sub_row JOIN unit_measure on unit_measure.id=structure_national_production.id_unit_measure WHERE production_period.designation= ? AND production.action_type= ? AND productive_row.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $campaign, PDO::PARAM_STR);
        $prep->bindparam(2, $actionType, PDO::PARAM_STR);
        $prep->bindparam(3, $productiveRow, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $totalProduction += $reg->production_quant;
                $i++;
            }
            return $totalProduction;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

//Getting percentage
    function getPercentage($generalValue, $partValue) {
        $pernatege = 0;
        try {
            $pernatege = ($partValue / $generalValue) * 100;

            return $pernatege;
        } catch (DivisionByZeroError $ex) {
            
        }
    }

//Getting variation
    function getVariation($currentValue, $beforeValue) {
        $varition = 0;
        try {
            $varition = ($currentValue / $beforeValue) - 1 * 100;
            return $varition;
        } catch (DivisionByZeroError $e) {
//echo 'Valeu!';
        }
    }

//Getting variation
    function getMeasureConvertion($currentValue) {
        $convertion = 0;
        try {
            $convertion = ($currentValue * 1000);
            return $convertion;
        } catch (DivisionByZeroError $e) {
//echo 'Valeu!';
        }
    }

//
    function getGeneralArea($actionType) {
        $i = 0;
        $generalArea = 0;
        $cons = "SELECT sum(production.available_area) as area from production where production.action_type = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $actionType, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $generalArea = $reg->area;
                $i++;
            }
            return $generalArea;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

//
    function getGeneralProduction() {
        $i = 0;
        $generalProduction = 0;
        $cons = "SELECT sum(production.quantity) as quant from production";
        $prep = $this->dbh->prepare($cons);
//        $prep->bindparam(3, $productiveRow, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $generalProduction = $reg->quant;
                $i++;
            }
            return $generalProduction;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

//
    function getGeneralArea2($ationType) {
        $i = 0;
        $generalArea = 0;
        $cons = "SELECT sum(production.available_area) as area from production where production.action_type = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $ationType, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $generalArea = $reg->area;
                $i++;
            }
            return $generalArea;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

//
// getting region
    function getRegion() {
        $arrayRegion = array();
        $region = new Region($this->dbh);
        $regionData = $region->readRegion();
        foreach ($regionData as $regionName) {
            $arrayRegion[] = $regionName['designation'];
        }
        return $arrayRegion;
    }

// getting TypeProduction
    function getTypeProduction() {
        $arrayTypeProd = array();
        $TypeProd = new TypeProduction($this->dbh);
        $TypeProdData = $TypeProd->readTypeProduction();
        foreach ($TypeProdData as $TypeProdName) {
            $arrayTypeProd[] = $TypeProdName['designation'];
        }
        return $arrayTypeProd;
    }

//
    function getProductionRegion($campaign, $actionType, $productiveRow, $region) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT SUM(production.quantity) as quant, SUM(production.available_area) as area FROM province join region ON province.id_region=region.id JOIN production ON production.id_province=province.id JOIN production_period on production.id_production_period=production_period.id join structure_national_production ON structure_national_production.id_productive_row=production.id_productive_row and structure_national_production.id_productive_sub_row=production.id_productive_sub_row AND structure_national_production.id_product=production.id_product AND structure_national_production.id_sub_product=production.id_sub_product JOIN product ON product.id=structure_national_production.id_product JOIN sub_product ON sub_product.id=structure_national_production.id_sub_product join productive_row ON productive_row.id=structure_national_production.id_productive_row join productive_sub_row on productive_sub_row.id=structure_national_production.id_productive_sub_row JOIN unit_measure on unit_measure.id=structure_national_production.id_unit_measure WHERE production_period.designation= ? AND production.action_type= ? AND productive_row.id = ? AND region.designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $campaign, PDO::PARAM_STR);
        $prep->bindparam(2, $actionType, PDO::PARAM_STR);
        $prep->bindparam(3, $productiveRow, PDO::PARAM_STR);
        $prep->bindparam(4, $region, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['production'] = $reg->quant;
                $arrayData[$i]['percentage_production'] = $this->getPercentage($this->getTotalProdCampaign($campaign, $actionType, $productiveRow), $reg->quant) . "%";
                $arrayData[$i]['variation_production'] = $this->getVariation($reg->quant, $this->getProdRegionVariation("2020/2021", $actionType, $productiveRow, $region)) . "%";
                $arrayData[$i]['area'] = $reg->area;
                $arrayData[$i]['percentage_area'] = $this->getPercentage($this->getTotalAreaCampaign($campaign, $actionType, $productiveRow), $reg->area) . "%";
                $arrayData[$i]['variation_area'] = $this->getVariation($reg->area, $this->getAreaRegionVariation("2020/2021", $actionType, $productiveRow, $region)) . "%";
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

///
    function getProdProvince($campaign, $actionType, $productiveRow, $province) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT SUM(production.quantity) as quant, SUM(production.available_area) as area, region.designation as region FROM province join region ON province.id_region=region.id JOIN production ON production.id_province=province.id JOIN production_period on production.id_production_period=production_period.id join structure_national_production ON structure_national_production.id_productive_row=production.id_productive_row and structure_national_production.id_productive_sub_row=production.id_productive_sub_row AND structure_national_production.id_product=production.id_product AND structure_national_production.id_sub_product=production.id_sub_product JOIN product ON product.id=structure_national_production.id_product JOIN sub_product ON sub_product.id=structure_national_production.id_sub_product join productive_row ON productive_row.id=structure_national_production.id_productive_row join productive_sub_row on productive_sub_row.id=structure_national_production.id_productive_sub_row JOIN unit_measure on unit_measure.id=structure_national_production.id_unit_measure WHERE production_period.designation= ? AND production.action_type= ? AND productive_row.designation = ? AND province.designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $campaign, PDO::PARAM_STR);
        $prep->bindparam(2, $actionType, PDO::PARAM_STR);
        $prep->bindparam(3, $productiveRow, PDO::PARAM_STR);
        $prep->bindparam(4, $province, PDO::PARAM_STR);
        try {
            $prep->execute();
            $arrayData[$i]['province'] = $province;
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['region'] = $reg->region;
                $arrayData[$i]['production'] = $reg->quant;

                $arrayData[$i]['percentage_production'] = $this->getPercentage($this->getProdRegionVariation($campaign, $actionType, $productiveRow, $reg->region), $reg->quant) . "%";
                $arrayData[$i]['variation_production'] = $this->getVariation($reg->quant, $this->getProdProvVariation("2020/2021", $actionType, $productiveRow, $province)) . "%";

                $arrayData[$i]['area'] = $reg->area;

                $arrayData[$i]['percentage_area'] = $this->getPercentage($this->getAreaRegionVariation($campaign, $actionType, $productiveRow, $reg->region), $reg->area) . "%";
                $arrayData[$i]['variation_area'] = $this->getVariation($reg->area, $this->getAreaProvVariation("2020/2021", $actionType, $productiveRow, $province)) . "%";
                $i++;
            }


            return $arrayData;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    function getAllProdProvince($campaign, $actionType, $productiveRow, $province) {
        $i = 0;
        $arrayData = [];
        foreach ($this->getProvince() as $province) {
            foreach ($this->getProdRow() as $productiveRow) {
                
            }
        }
        return $arrayData;
    }

    function getProvPerRow($productiveRow) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT DISTINCT(province.designation) as province_name FROM province join region ON province.id_region=region.id JOIN production ON production.id_province=province.id JOIN production_period on production.id_production_period=production_period.id join structure_national_production ON structure_national_production.id_productive_row=production.id_productive_row and structure_national_production.id_productive_sub_row=production.id_productive_sub_row AND structure_national_production.id_product=production.id_product AND structure_national_production.id_sub_product=production.id_sub_product JOIN product ON product.id=structure_national_production.id_product JOIN sub_product ON sub_product.id=structure_national_production.id_sub_product join productive_row ON productive_row.id=structure_national_production.id_productive_row join productive_sub_row on productive_sub_row.id=structure_national_production.id_productive_sub_row JOIN unit_measure on unit_measure.id=structure_national_production.id_unit_measure WHERE productive_row.designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $productiveRow, PDO::PARAM_STR);
        try {
            $prep->execute();

            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['province_name'] = $reg->province_name;
                $arrayData[$i]['province_data'] = $this->getProdProvince("2021/2022", "Colher", $productiveRow, $reg->province_name);

                $i++;
            }



            return $arrayData;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

//////////
    function getProvPerRow2($productiveRow, $typeProdRow) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT DISTINCT(province.designation) as province_name FROM province join region ON province.id_region=region.id JOIN production ON production.id_province=province.id JOIN production_period on production.id_production_period=production_period.id join structure_national_production ON structure_national_production.id_productive_row=production.id_productive_row and structure_national_production.id_productive_sub_row=production.id_productive_sub_row AND structure_national_production.id_product=production.id_product AND structure_national_production.id_sub_product=production.id_sub_product JOIN product ON product.id=structure_national_production.id_product JOIN sub_product ON sub_product.id=structure_national_production.id_sub_product join productive_row ON productive_row.id=structure_national_production.id_productive_row join productive_sub_row on productive_sub_row.id=structure_national_production.id_productive_sub_row JOIN unit_measure on unit_measure.id=structure_national_production.id_unit_measure WHERE productive_row.designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $productiveRow, PDO::PARAM_STR);
        try {
            $prep->execute();

            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                if ($typeProdRow == "Vegetal") {
                    $arrayData[$i]['province_name'] = $reg->province_name;
                    $arrayData[$i]['province_data'] = $this->getProdProvince("2021/2022", "Colher", $productiveRow, $reg->province_name);
                } else {
                    $arrayData[$i]['province_name'] = $reg->province_name;
                    $arrayData[$i]['province_data'] = $this->getProdProvince("2021/2022", "N/A", $productiveRow, $reg->province_name);
                }


                $i++;
            }



            return $arrayData;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

////////////////
    function getProdProvVariation($campaign, $actionType, $productiveRow, $province) {
        $i = 0;
        $production = 0;
        $cons = "SELECT SUM(production.quantity) as quant, SUM(production.available_area) as area, region.designation as region FROM province join region ON province.id_region=region.id JOIN production ON production.id_province=province.id JOIN production_period on production.id_production_period=production_period.id join structure_national_production ON structure_national_production.id_productive_row=production.id_productive_row and structure_national_production.id_productive_sub_row=production.id_productive_sub_row AND structure_national_production.id_product=production.id_product AND structure_national_production.id_sub_product=production.id_sub_product JOIN product ON product.id=structure_national_production.id_product JOIN sub_product ON sub_product.id=structure_national_production.id_sub_product join productive_row ON productive_row.id=structure_national_production.id_productive_row join productive_sub_row on productive_sub_row.id=structure_national_production.id_productive_sub_row JOIN unit_measure on unit_measure.id=structure_national_production.id_unit_measure WHERE production_period.designation= ? AND production.action_type= ? AND productive_row.designation = ? AND province.designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $campaign, PDO::PARAM_STR);
        $prep->bindparam(2, $actionType, PDO::PARAM_STR);
        $prep->bindparam(3, $productiveRow, PDO::PARAM_STR);
        $prep->bindparam(4, $province, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $production = $reg->quant;
                $i++;
            }
            return $production;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

////////////////
    function getAreaProvVariation($campaign, $actionType, $productiveRow, $province) {
        $i = 0;
        $area = 0;
        $cons = "SELECT SUM(production.quantity) as quant, SUM(production.available_area) as area, region.designation as region FROM province join region ON province.id_region=region.id JOIN production ON production.id_province=province.id JOIN production_period on production.id_production_period=production_period.id join structure_national_production ON structure_national_production.id_productive_row=production.id_productive_row and structure_national_production.id_productive_sub_row=production.id_productive_sub_row AND structure_national_production.id_product=production.id_product AND structure_national_production.id_sub_product=production.id_sub_product JOIN product ON product.id=structure_national_production.id_product JOIN sub_product ON sub_product.id=structure_national_production.id_sub_product join productive_row ON productive_row.id=structure_national_production.id_productive_row join productive_sub_row on productive_sub_row.id=structure_national_production.id_productive_sub_row JOIN unit_measure on unit_measure.id=structure_national_production.id_unit_measure WHERE production_period.designation= ? AND production.action_type= ? AND productive_row.designation = ? AND province.designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $campaign, PDO::PARAM_STR);
        $prep->bindparam(2, $actionType, PDO::PARAM_STR);
        $prep->bindparam(3, $productiveRow, PDO::PARAM_STR);
        $prep->bindparam(4, $province, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $area = $reg->area;
                $i++;
            }
            return $area;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

///////////////////////
    function getProdRegionVariation($campaign, $actionType, $productiveRow, $region) {
        $i = 0;
        $arrayData = [];
        $production = 0;
        $cons = "SELECT SUM(production.quantity) as quant, SUM(production.available_area) as area FROM province join region ON province.id_region=region.id JOIN production ON production.id_province=province.id JOIN production_period on production.id_production_period=production_period.id join structure_national_production ON structure_national_production.id_productive_row=production.id_productive_row and structure_national_production.id_productive_sub_row=production.id_productive_sub_row AND structure_national_production.id_product=production.id_product AND structure_national_production.id_sub_product=production.id_sub_product JOIN product ON product.id=structure_national_production.id_product JOIN sub_product ON sub_product.id=structure_national_production.id_sub_product join productive_row ON productive_row.id=structure_national_production.id_productive_row join productive_sub_row on productive_sub_row.id=structure_national_production.id_productive_sub_row JOIN unit_measure on unit_measure.id=structure_national_production.id_unit_measure WHERE production_period.designation= ? AND production.action_type= ? AND productive_row.designation = ? AND region.designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $campaign, PDO::PARAM_STR);
        $prep->bindparam(2, $actionType, PDO::PARAM_STR);
        $prep->bindparam(3, $productiveRow, PDO::PARAM_STR);
        $prep->bindparam(4, $region, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $production = $reg->quant;
//$arrayData[$i]['production'] = $reg->quant;
//$arrayData[$i]['percentage_production'] = $this->getPercentage($this->getTotalProdCampaign($campaign, $actionType, $productiveRow), $reg->quant) . "%";
//$arrayData[$i]['area'] = $reg->area;
//$arrayData[$i]['percentage_area'] = $this->getPercentage($this->getTotalAreaCampaign($campaign, $actionType, $productiveRow), $reg->area) . "%";
                $i++;
            }
            return $production;
//return $arrayData;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

///////////////////////
    function getAreaRegionVariation($campaign, $actionType, $productiveRow, $region) {
        $i = 0;
        $arrayData = [];
        $area = 0;
        $cons = "SELECT SUM(production.quantity) as quant, SUM(production.available_area) as area FROM province join region ON province.id_region=region.id JOIN production ON production.id_province=province.id JOIN production_period on production.id_production_period=production_period.id join structure_national_production ON structure_national_production.id_productive_row=production.id_productive_row and structure_national_production.id_productive_sub_row=production.id_productive_sub_row AND structure_national_production.id_product=production.id_product AND structure_national_production.id_sub_product=production.id_sub_product JOIN product ON product.id=structure_national_production.id_product JOIN sub_product ON sub_product.id=structure_national_production.id_sub_product join productive_row ON productive_row.id=structure_national_production.id_productive_row join productive_sub_row on productive_sub_row.id=structure_national_production.id_productive_sub_row JOIN unit_measure on unit_measure.id=structure_national_production.id_unit_measure WHERE production_period.designation= ? AND production.action_type= ? AND productive_row.designation = ? AND region.designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $campaign, PDO::PARAM_STR);
        $prep->bindparam(2, $actionType, PDO::PARAM_STR);
        $prep->bindparam(3, $productiveRow, PDO::PARAM_STR);
        $prep->bindparam(4, $region, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $area = $reg->area;
//$arrayData[$i]['production'] = $reg->quant;
//$arrayData[$i]['percentage_production'] = $this->getPercentage($this->getTotalProdCampaign($campaign, $actionType, $productiveRow), $reg->quant) . "%";
//$arrayData[$i]['area'] = $reg->area;
//$arrayData[$i]['percentage_area'] = $this->getPercentage($this->getTotalAreaCampaign($campaign, $actionType, $productiveRow), $reg->area) . "%";
                $i++;
            }
            return $area;
//return $arrayData;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

///////////////////////
    function getQuantPerRow($campaign, $productiveRow) {
        $i = 0;
        $arrayData = [];
        $quant = 0;
        $cons = "SELECT SUM(production.quantity) as quant FROM province join region ON province.id_region=region.id JOIN production ON production.id_province=province.id JOIN production_period on production.id_production_period=production_period.id join structure_national_production ON structure_national_production.id_productive_row=production.id_productive_row and structure_national_production.id_productive_sub_row=production.id_productive_sub_row AND structure_national_production.id_product=production.id_product AND structure_national_production.id_sub_product=production.id_sub_product JOIN product ON product.id=structure_national_production.id_product JOIN sub_product ON sub_product.id=structure_national_production.id_sub_product join productive_row ON productive_row.id=structure_national_production.id_productive_row join productive_sub_row on productive_sub_row.id=structure_national_production.id_productive_sub_row JOIN unit_measure on unit_measure.id=structure_national_production.id_unit_measure WHERE production_period.designation= ? AND productive_row.designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $campaign, PDO::PARAM_STR);
        $prep->bindparam(2, $productiveRow, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $quant = $reg->quant;
//$arrayData[$i]['production'] = $reg->quant;
//$arrayData[$i]['percentage_production'] = $this->getPercentage($this->getTotalProdCampaign($campaign, $actionType, $productiveRow), $reg->quant) . "%";
//$arrayData[$i]['area'] = $reg->area;
//$arrayData[$i]['percentage_area'] = $this->getPercentage($this->getTotalAreaCampaign($campaign, $actionType, $productiveRow), $reg->area) . "%";
                $i++;
            }
            return $quant;
//return $arrayData;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

///////////////////////
    function getAllQuantPerRow2() {
        $i = 0;
        $arrayData = [];
        foreach ($this->getProdRow() as $row) {
            $arrayData[$i]['row'] = $row;
            $arrayData[$i]['variation'] = $this->getVariation($this->getQuantPerRow("2021/2022", $row), $this->getQuantPerRow("2020/2021", $row));
            foreach ($this->getProdPeriod() as $campaign) {
                $arrayData[$i]['campaign'] = $campaign;
                $arrayData[$i]['production_data'] = $this->getQuantPerRow($campaign, $row);
                $i++;
            }
        }
        return $arrayData;
    }

    function getAllQuantPerRow() {
        $i = 0;
        $arrayData = [];
        foreach ($this->getProdRow() as $row) {
            $arrayData[$i]['row'] = $row;
//$arrayData[$i]['variation'] = $this->getVariation($this->getQuantPerRow("2021/2022", $row), $this->getQuantPerRow("2020/2021", $row));
//            foreach ($this->getProdPeriod() as $campaign) {
//                $arrayData[$i]['campaign'] = $campaign;            
            $arrayData[$i]['current_campaign'] = $this->getQuantPerRow("2021/2022", $row) . "(t)" . "-" . $this->getMeasureConvertion($this->getQuantPerRow("2021/2022", $row)) . "(Kg)";
            $arrayData[$i]['before_campaign'] = $this->getQuantPerRow("2020/2021", $row) . "(t)" . "-" . $this->getMeasureConvertion($this->getQuantPerRow("2020/2021", $row)) . "(Kg)";
            $arrayData[$i]['variation'] = $this->getVariation($this->getQuantPerRow("2021/2022", $row), $this->getQuantPerRow("2020/2021", $row)) . "%";
            $i++;
//            }
        }

        return $arrayData;
    }

////////
    function getRowArea($productveRow, $compaign, $actionType) {
        $i = 0;
        $sown_area = 0;
        $arrayData = [];
        $cons = "SELECT SUM(production.available_area) as sown_area FROM province join region ON province.id_region=region.id JOIN production ON production.id_province=province.id JOIN production_period on production.id_production_period=production_period.id join structure_national_production ON structure_national_production.id_productive_row=production.id_productive_row and structure_national_production.id_productive_sub_row=production.id_productive_sub_row AND structure_national_production.id_product=production.id_product AND structure_national_production.id_sub_product=production.id_sub_product JOIN product ON product.id=structure_national_production.id_product JOIN sub_product ON sub_product.id=structure_national_production.id_sub_product join productive_row ON productive_row.id=structure_national_production.id_productive_row join productive_sub_row on productive_sub_row.id=structure_national_production.id_productive_sub_row JOIN unit_measure on unit_measure.id=structure_national_production.id_unit_measure WHERE production_period.designation= ? AND production.action_type = ? AND productive_row.designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $compaign, PDO::PARAM_STR);
        $prep->bindparam(2, $actionType, PDO::PARAM_STR);
        $prep->bindparam(3, $productveRow, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $sown_area = $reg->sown_area;
            }
            return $sown_area;
//return $arrayData;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

////////
    function getRowProduction($productveRow, $compaign, $actionType) {
        $i = 0;
        $productuion = 0;
        $arrayData = [];
        $cons = "SELECT SUM(production.quantity) as productuion FROM province join region ON province.id_region=region.id JOIN production ON production.id_province=province.id JOIN production_period on production.id_production_period=production_period.id join structure_national_production ON structure_national_production.id_productive_row=production.id_productive_row and structure_national_production.id_productive_sub_row=production.id_productive_sub_row AND structure_national_production.id_product=production.id_product AND structure_national_production.id_sub_product=production.id_sub_product JOIN product ON product.id=structure_national_production.id_product JOIN sub_product ON sub_product.id=structure_national_production.id_sub_product join productive_row ON productive_row.id=structure_national_production.id_productive_row join productive_sub_row on productive_sub_row.id=structure_national_production.id_productive_sub_row JOIN unit_measure on unit_measure.id=structure_national_production.id_unit_measure WHERE production_period.designation= ? AND production.action_type = ? AND productive_row.designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $compaign, PDO::PARAM_STR);
        $prep->bindparam(2, $actionType, PDO::PARAM_STR);
        $prep->bindparam(3, $productveRow, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $productuion = $reg->productuion;
            }
            return $productuion;
//return $arrayData;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

///////////////////////
    function getRowTypeProd($productionType) {
//Arroz com feijão.
        $i = 0;
        $arrayData = [];
        $cons = "SELECT productive_row.designation as name_row, productive_row.id as id_row, productive_row.comment as prod_comment from productive_row JOIN type_production on type_production.id=productive_row.id_type_production WHERE type_production.designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $productionType, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                if ($productionType == "Vegetal") {
                    $arrayData[$i]['productive_row'] = $reg->name_row;
                    $arrayData[$i]['provinces'] = $this->getProvPerRow($reg->name_row);
                    $arrayData[$i]['id_row'] = $reg->id_row;
//Getting the productiveRow photo              
                    $productPhoto = new DocumentStorage('rowPhoto', $reg->id_row, $this->dbh);
                    $productPhoto = $productPhoto->getDocument();
                    $arrayData[$i]['photo'] = $productPhoto;
                    $arrayData[$i]['area'] = $this->getRowArea($reg->name_row, "2021/2022", "semear");
                    $arrayData[$i]['production'] = $this->getRowProduction($reg->name_row, "2021/2022", "colher");
                    $arrayData[$i]['description'] = $reg->prod_comment;
                    $arrayData[$i]['variation'] = $this->getVariation($this->getRowProduction($reg->name_row, "2021/2022", "Colher"), $this->getRowProduction($reg->name_row, "2020/2021", "Colher")) . "%";
                } else {
                    $arrayData[$i]['productive_row'] = $reg->name_row;
                    $arrayData[$i]['provinces'] = $this->getProvPerRow($reg->name_row);
                    $arrayData[$i]['id_row'] = $reg->id_row;
//Getting the productiveRow photo              
                    $productPhoto = new DocumentStorage('rowPhoto', $reg->id_row, $this->dbh);
                    $productPhoto = $productPhoto->getDocument();
                    $arrayData[$i]['photo'] = $productPhoto;
//$arrayData[$i]['area'] = $this->getRowArea($reg->name_row, "2021/2022", "semear");
                    $arrayData[$i]['production'] = $this->getRowProduction($reg->name_row, "2021/2022", "N/A");
                    $arrayData[$i]['description'] = $reg->prod_comment;
                    $arrayData[$i]['variation'] = $this->getVariation($this->getRowProduction($reg->name_row, "2021/2022", "N/A"), $this->getRowProduction($reg->name_row, "2020/2021", "N/A")) . "%";
                }
//$arrayData[$i]['production'] = $reg->quant;
//$arrayData[$i]['percentage_production'] = $this->getPercentage($this->getTotalProdCampaign($campaign, $actionType, $productiveRow), $reg->quant) . "%";
//$arrayData[$i]['area'] = $reg->area;
//$arrayData[$i]['percentage_area'] = $this->getPercentage($this->getTotalAreaCampaign($campaign, $actionType, $productiveRow), $reg->area) . "%";
                $i++;
            }
            return $arrayData;
//return $arrayData;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

///////////////////////
    function getDataPerRow($productiveRow, $typeProdRow) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT productive_row.id as id_row, productive_row.comment as prod_comment from productive_row JOIN type_production on type_production.id=productive_row.id_type_production WHERE productive_row.designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $productiveRow, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                if ($typeProdRow == "Vegetal") {
                    $arrayData[$i]['productive_row'] = $productiveRow;
                    $arrayData[$i]['provinces'] = $this->getProvPerRow2($productiveRow, $typeProdRow);
                    $arrayData[$i]['id_row'] = $reg->id_row;
                    //Getting the productiveRow photo              
                    $productPhoto = new DocumentStorage('rowPhoto', $reg->id_row, $this->dbh);
                    $productPhoto = $productPhoto->getDocument();
                    $arrayData[$i]['photo'] = $productPhoto;
                    $arrayData[$i]['area'] = $this->getRowArea($productiveRow, "2021/2022", "semear");
                    $arrayData[$i]['production'] = $this->getRowProduction($productiveRow, "2021/2022", "colher");
                    $arrayData[$i]['description'] = $reg->prod_comment;
                } else {
                    $arrayData[$i]['productive_row'] = $productiveRow;
                    $arrayData[$i]['provinces'] = $this->getProvPerRow2($productiveRow, $typeProdRow);
                    $arrayData[$i]['id_row'] = $reg->id_row;
//Getting the productiveRow photo              
                    $productPhoto = new DocumentStorage('rowPhoto', $reg->id_row, $this->dbh);
                    $productPhoto = $productPhoto->getDocument();
                    $arrayData[$i]['photo'] = $productPhoto;
//$arrayData[$i]['area'] = $this->getRowArea($productiveRow, "2021/2022", "semear");
                    $arrayData[$i]['production'] = $this->getRowProduction($productiveRow, "2021/2022", "N/A");
                    $arrayData[$i]['variation'] = $this->getVariation($this->getRowProduction($productiveRow, "2021/2022", "N/A"), $this->getRowProduction($productiveRow, "2020/2021", "N/A")) . "%";
                    $arrayData[$i]['description'] = $reg->prod_comment;
                }

                $i++;
            }
            return $arrayData;
//return $arrayData;
        } catch (Exception $e) {
//Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

///////////////////////
    function getAllRowTypeProd() {
        $i = 0;
        $arrayData = [];
        foreach ($this->getTypeProduction() as $typeProd) {
            $arrayData[$i]['type_production'] = $typeProd;
            $arrayData[$i]['productive_rows'] = $this->getRowTypeProd($typeProd);

            $i++;
        }
        return $arrayData;
    }

//

    function getAllProdRegion() {
        $i = 0;
        $arrayData = [];
        $region = $this->getRegion();
        foreach ($region as $rn) {
            $arrayData[$i]['region'] = $rn;
            foreach ($this->getActionType() as $at) {
                $arrayData[$i]['action_type'] = $at;
                foreach ($this->getProdPeriod() as $prodPer) {
                    $arrayData[$i]['campaign'] = $prodPer;
                    $arrayData[$i]['region_data'] = $this->getProductionRegion("2020/2021", $at, 1, $rn);
                    $i++;
                }
            }
        }

        return $arrayData;
    }

    //////getSumQuantRow

    function getSumQuantRow() {
        $i = 0;
        $total = 0;
        $arrayData = [];
        $cons = "SELECT productive_row.designation as name_row, SUM(production.quantity) as quant, production_period.designation as period FROM province join region ON province.id_region=region.id JOIN production ON production.id_province=province.id JOIN production_period on production.id_production_period=production_period.id join structure_national_production ON structure_national_production.id_productive_row=production.id_productive_row and structure_national_production.id_productive_sub_row=production.id_productive_sub_row AND structure_national_production.id_product=production.id_product AND structure_national_production.id_sub_product=production.id_sub_product JOIN product ON product.id=structure_national_production.id_product JOIN sub_product ON sub_product.id=structure_national_production.id_sub_product join productive_row ON productive_row.id=structure_national_production.id_productive_row join productive_sub_row on productive_sub_row.id=structure_national_production.id_productive_sub_row JOIN unit_measure on unit_measure.id=structure_national_production.id_unit_measure GROUP by productive_row.id";
        $prep = $this->dbh->prepare($cons);
        //$prep->bindparam(1, $productionType, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $total += $reg->quant;
                $arrayData[$i]['productive_row'] = $reg->name_row;
                $arrayData[$i]['production'] = $reg->quant;
                $arrayData[$i]['period'] = $reg->period;
                $arrayData[$i]['percentage'] = ($reg->quant / $this->getGeneralSumQuantRow()) * (100) . "%";
                $i++;
            }
            $arrayData[$i]['total'] = $total;
            return $arrayData;
        } catch (Exception $e) {
            return false;
        }
    }

    //////getGeneralSumQuantRow

    function getGeneralSumQuantRow() {
        $i = 0;
        $quant = 0;
        $arrayData = [];
        $cons = "SELECT SUM(production.quantity) as quant FROM province join region ON province.id_region=region.id JOIN production ON production.id_province=province.id JOIN production_period on production.id_production_period=production_period.id join structure_national_production ON structure_national_production.id_productive_row=production.id_productive_row and structure_national_production.id_productive_sub_row=production.id_productive_sub_row AND structure_national_production.id_product=production.id_product AND structure_national_production.id_sub_product=production.id_sub_product JOIN product ON product.id=structure_national_production.id_product JOIN sub_product ON sub_product.id=structure_national_production.id_sub_product join productive_row ON productive_row.id=structure_national_production.id_productive_row join productive_sub_row on productive_sub_row.id=structure_national_production.id_productive_sub_row JOIN unit_measure on unit_measure.id=structure_national_production.id_unit_measure";
        $prep = $this->dbh->prepare($cons);
        //$prep->bindparam(1, $productionType, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $quant = $reg->quant;
            }
            return $quant;
        } catch (Exception $e) {
            return false;
        }
    }

    //Get balanceCategory
    public function getBalanceCategory() {
        $arrayCategoryName = [];
        $category = new TradeBalanceCategory($this->dbh);
        $arrayCategory = $category->readTradeBalaCate();
        foreach ($arrayCategory as $cn) {
            $arrayCategoryName[] = $cn['designation'];
        }
        return $arrayCategoryName;
    }

    //Get Product per Category
    function getProductPerCategory($balance) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT DISTINCT(product_balance.designation) AS product_balance, trade_balance_category.designation AS product_category FROM trade_balance JOIN product_balance ON trade_balance.id_product_balance=product_balance.id JOIN trade_balance_category ON trade_balance_category.id=trade_balance.id_trade_balance_category";
        $prep = $this->dbh->prepare($cons);
        //$prep->bindparam(1, $category, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {

                $arrayData[$i]['product_balance'] = $reg->product_balance;
                $arrayData[$i]['product_category'] = $reg->product_category;
                $arrayData[$i]['before_value'] = $this->getValuePerProduct($reg->product_balance, 2022, $balance) . "(m USD)";
                $arrayData[$i]['current_value'] = $this->getValuePerProduct($reg->product_balance, 2023, $balance) . "(m USD)";
                $arrayData[$i]['variation_value'] = $this->getVariation($this->getValuePerProduct($reg->product_balance, 2023, $balance), $this->getValuePerProduct($reg->product_balance, 2022, $balance)) . "%";

                $arrayData[$i]['before_quant'] = $this->getQuantPerProduct($reg->product_balance, 2022, $balance) . "(ton)";
                $arrayData[$i]['current_quant'] = $this->getQuantPerProduct($reg->product_balance, 2023, $balance) . "(ton)";
                $arrayData[$i]['variation_quant'] = $this->getVariation($this->getQuantPerProduct($reg->product_balance, 2023, $balance), $this->getQuantPerProduct($reg->product_balance, 2022, $balance)) . "%";
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            return false;
        }
    }

    /// Ge value
    function getValuePerProduct($product, $year, $tradeType) {
        $i = 0;
        $arrayData = [];
        $value = 0;
        $cons = "SELECT trade_balance.value AS value_balance, trade_balance.quant AS quant_balance FROM trade_balance JOIN product_balance ON trade_balance.id_product_balance=product_balance.id JOIN trade_balance_category ON trade_balance_category.id=trade_balance.id_trade_balance_category WHERE product_balance.designation = ? AND trade_balance.year = ? AND trade_balance.trade_balance_type = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $product, PDO::PARAM_STR);
        $prep->bindparam(2, $year, PDO::PARAM_STR);
        $prep->bindparam(3, $tradeType, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $value = $reg->value_balance;
            }
            return $value;
        } catch (Exception $e) {
            return false;
        }
    }

/// Get quant
    function getQuantPerProduct($product, $year, $tradeType) {
        $i = 0;
        $arrayData = [];
        $quant = 0;
        $cons = "SELECT trade_balance.value AS value_balance, trade_balance.quant AS quant_balance FROM trade_balance JOIN product_balance ON trade_balance.id_product_balance=product_balance.id JOIN trade_balance_category ON trade_balance_category.id=trade_balance.id_trade_balance_category WHERE product_balance.designation = ? AND trade_balance.year = ? AND trade_balance.trade_balance_type = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $product, PDO::PARAM_STR);
        $prep->bindparam(2, $year, PDO::PARAM_STR);
        $prep->bindparam(3, $tradeType, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $quant = $reg->quant_balance;
                $i++;
            }
            return $quant;
        } catch (Exception $e) {
            return false;
        }
    }

    //Get Product Per General Category
    function getProductPerGeneralCategory($balance) {
        $i = 0;
        $arrayData = [];
        try {
            foreach ($this->getBalanceCategory() as $bcn) {
                $arrayData[$i]['Category_name'] = $bcn;
                $arrayData[$i]['products'] = $this->getProductPerCategory($balance);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            return false;
        }
    }

    //////getTraining

    function getTraining() {
        $i = 0;
        $totalMale = 0;
        $totalFemale = 0;
        $arrayData = [];
        $cons = "SELECT training_initiative.designation AS initiative, quant_male, quant_female, quant_formation FROM training JOIN province ON province.id=training.id_province JOIN training_initiative ON training_initiative.id=training.id_training_initiative";
        $prep = $this->dbh->prepare($cons);
        //$prep->bindparam(1, $productionType, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['initiative'] = $reg->initiative;
                $arrayData[$i]['quant_male'] = $reg->quant_male;
                $arrayData[$i]['quant_female'] = $reg->quant_female;
                $arrayData[$i]['quant_formation'] = $reg->quant_formation;
                $arrayData[$i]['quant_male_and_female'] = $reg->quant_female + $reg->quant_male;
                $totalMale += $reg->quant_male;
                $totalFemale += $reg->quant_female;
                $i++;
            }
            $arrayData[$i]['quant_male'] = $totalMale;
            $arrayData[$i]['quant_female'] = $totalFemale;
            $arrayData[$i]['general_quant'] = $totalFemale + $totalMale;
            return $arrayData;
        } catch (Exception $e) {
            return false;
        }
    }

}

?>
