<?php

require_once 'Auditing.php';
require_once 'DocumentStorage.php';

class Legislation {

    public $id;
    public $title;
    public $legislationType;
    public $legislationPhoto;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create legislation
    function registerLegislation() {
        $cons = "INSERT INTO legislation VALUES(?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->title);
        $prep->bindparam(3, $this->legislationType);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of direction before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('legislação', 'inserir', '', $dataAfterExecution);
            return $lastId;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read all legislation 
    function readLegislation() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM legislation ";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['title'] = $reg->title;
                $arrayData[$i]['legislation_type'] = $reg->legislation_type;
                $document = new DocumentStorage("legislationPhoto", $reg->id, $this->dbh);
                $arrayData[$i]['photo'] = $document->getDocument();

                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined legislation
    function readDetermLegislation() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM legislation WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['title'] = $reg->title;
                $arrayData[$i]['legislation_type'] = $reg->legislation_type;
                $document = new DocumentStorage("legislationPhoto", $reg->id, $this->dbh);
                $arrayData[$i]['photo'] = $document->getDocument();
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update legislation
    function updateLegislation() {
        $cons = "UPDATE legislation SET title = ?, legislation_type = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->title);
        $prep->bindparam(2, $this->legislationType);
        $prep->bindparam(3, $this->id);

        //$prep->execute();
        // Get data of direction before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of direction before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('legislação', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            return $e->getMessage();
        }
    }

    // Delete legislation
    function deleteLegislation() {
        $cons = "DELETE FROM legislation WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of direction before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class direction
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('legislação', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data of a specific legislation
    function getDataLegislation($id) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM legislation WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['title'] = $reg->title;
                $arrayData[$i]['legislation_type'] = $reg->legislation_type;
                $document = new DocumentStorage("legislationPhoto", $reg->id, $this->dbh);
                $arrayData[$i]['photo'] = $document->getDocument();
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $IndicatorData = '';
        $cons = "SELECT * FROM legislation WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $IndicatorData = "id" . $reg->id . "title" . $reg->title . "legislation_type" . $reg->legislation_type;
                $i++;
            }

            return $IndicatorData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

    function checkLegislation() {
        $i = 0;
        $cons = "SELECT * FROM legislation WHERE title = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->title, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }

            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

}

?>