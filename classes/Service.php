<?php

require_once 'Auditing.php';

class Service {

    public $id;
    public $designation;
    public $acronym;
    public $domain_name;
    public $comment;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create service
    function registerService() {
        $cons = "INSERT INTO service VALUES(?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->designation);
        $prep->bindparam(3, $this->acronym);
        $prep->bindparam(4, $this->domain_name);
        $prep->bindparam(5, $this->comment);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of department before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('departamento', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read all service
    function readService() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM service";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['acronym'] = $reg->acronym;
                $arrayData[$i]['domain_name'] = $reg->domain_name;
                $arrayData[$i]['comment'] = $reg->comment;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined service
    function readDeterminedService() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM service WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['acronym'] = $reg->acronym;
                $arrayData[$i]['domain_name'] = $reg->domain_name;
                $arrayData[$i]['comment'] = $reg->comment;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update service
    function updateService() {
        $cons = "UPDATE service SET designation = ?, acronym = ?, domain_name = ?, comment = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation);
        $prep->bindparam(2, $this->acronym);
        $prep->bindparam(3, $this->domain_name);
        $prep->bindparam(4, $this->comment);
        $prep->bindparam(5, $this->id);
        //$prep->execute();
        // Get data of department before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of department before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('departamento', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            return $e->getMessage();
        }
    }

    // Delete service
    function deleteService() {
        $cons = "DELETE FROM service WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of department before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class department
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('departamento', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data of a specific service
    function getDataService($id) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM service WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                $arrayData[$i]['designation'] = $reg->designation;
                $arrayData[$i]['acronym'] = $reg->acronym;
                $arrayData[$i]['domain_name'] = $reg->domain_name;
                $arrayData[$i]['comment'] = $reg->comment;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $stringData = "";
        $dataReceivedFormated = '';
        $cons = "SELECT * FROM service WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $stringData = "Designation: " . $reg->designation . ", Acronym: " . $reg->acronym . ", Domain name: " . $reg->domain_name . ", Comment: " . $reg->comment;
            }

            return $stringData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

    function checkService() {
        $i = 0;
        $cons = "SELECT * FROM service WHERE acronym = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->acronym, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }

            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    //
    function checkService2($acronym) {
        $i = 0;
        $cons = "SELECT * FROM service WHERE acronym = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $acronym, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }

            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

}

?>