<?php

require_once 'Province.php';

class Fair {

    public $id;
    public $idProvince;
    public $heldFair;
    public $quantProductor;
    public $quantFisherman;
    public $quantMarketer;
    public $quantActivity;
    public $quantPartnership;
    public $quantFormer;
    public $quantSponsor;
    public $quantOcd;
    public $madeContract;
    public $transactionalAmount;
    public $cateringOperator;
    public $serviceProvider;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create fair 
    function registerFair() {
        $cons = "INSERT INTO fair VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->idProvince);
        $prep->bindparam(3, $this->heldFair);
        $prep->bindparam(4, $this->quantProductor);
        $prep->bindparam(5, $this->quantFisherman);
        $prep->bindparam(6, $this->quantMarketer);
        $prep->bindparam(7, $this->quantActivity);
        $prep->bindparam(8, $this->quantPartnership);
        $prep->bindparam(9, $this->quantFormer);
        $prep->bindparam(10, $this->quantSponsor);
        $prep->bindparam(11, $this->quantOcd);
        $prep->bindparam(12, $this->madeContract);
        $prep->bindparam(13, $this->transactionalAmount);
        $prep->bindparam(14, $this->cateringOperator);
        $prep->bindparam(15, $this->serviceProvider);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            // Get data of department before and after the execution of an action
//            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
//            // instance the class Auditing
//            $auditing = new Auditing($this->dbh);
//            $response = $auditing->insertDataAuditingFile('action', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read all fair
    function readFair() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM fair";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData[$i]['id'] = $reg->id;
                //Stancing the province Class
                $province = new Province($this->dbh);
                $province->id = $reg->id_province;
                $arrayData[$i]['province'] = $province->readDeterminedProvince()['designation'];
//                $arrayData[$i]['id_province'] = $reg->id_province;
                $arrayData[$i]['held_fair'] = $reg->held_fair;
                $arrayData[$i]['id_municipal'] = $reg->quant_productor;
                $arrayData[$i]['quant_productor'] = $reg->quant_fisherman;
                $arrayData[$i]['quant_marketer'] = $reg->quant_marketer;
                $arrayData[$i]['quant_activity'] = $reg->quant_activity;
                $arrayData[$i]['quant_partnership'] = $reg->quant_partnership;
                $arrayData[$i]['quant_former'] = $reg->quant_former;
                $arrayData[$i]['quant_sponsor'] = $reg->quant_sponsor;
                $arrayData[$i]['quant_ocd'] = $reg->quant_ocd;
                $arrayData[$i]['made_contract'] = $reg->made_contract;
                $arrayData[$i]['transactional_amount'] = $reg->transactional_amount;
                $arrayData[$i]['catering_operator'] = $reg->catering_operator;
                $arrayData[$i]['service_provider'] = $reg->service_provider;

//                $directionData = new Direction($this->dbh);
//                $arrayDirectionData = $directionData->getDataDirection($reg->id_direction);
//                $arrayData[$i]['direction'] = $arrayDirectionData;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined fair
    function readDeterminedFair() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM fair WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                //Stancing the province Class
                $province = new Province($this->dbh);
                $province->id = $reg->id_province;
                $arrayData['province'] = $province->readDeterminedProvince()['designation'];
                $arrayData['held_fair'] = $reg->held_fair;
                $arrayData['id_municipal'] = $reg->quant_productor;
                $arrayData['quant_productor'] = $reg->quant_fisherman;
                $arrayData['quant_marketer'] = $reg->quant_marketer;
                $arrayData['quant_activity'] = $reg->quant_activity;
                $arrayData['quant_partnership'] = $reg->quant_partnership;
                $arrayData['quant_former'] = $reg->quant_former;
                $arrayData['quant_sponsor'] = $reg->quant_sponsor;
                $arrayData['quant_ocd'] = $reg->quant_ocd;
                $arrayData['made_contract'] = $reg->made_contract;
                $arrayData['transactional_amount'] = $reg->transactional_amount;
                $arrayData['catering_operator'] = $reg->catering_operator;
                $arrayData['service_provider'] = $reg->service_provider;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update fair
    function updateFair() {
        $cons = "UPDATE fair SET id_province = ?, held_fair = ?, quant_productor = ?, quant_fisherman = ?, quant_marketer = ?, quant_activity = ?, quant_partnership = ?, quant_former = ?, quant_sponsor = ?, quant_ocd = ?, made_contract = ?, transactional_amount = ?, catering_operator = ?, service_provider = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idProvince);
        $prep->bindparam(2, $this->heldFair);
        $prep->bindparam(3, $this->quantProductor);
        $prep->bindparam(4, $this->quantFisherman);
        $prep->bindparam(5, $this->quantMarketer);
        $prep->bindparam(6, $this->quantActivity);
        $prep->bindparam(7, $this->quantPartnership);
        $prep->bindparam(8, $this->quantFormer);
        $prep->bindparam(9, $this->quantSponsor);
        $prep->bindparam(10, $this->quantOcd);
        $prep->bindparam(11, $this->madeContract);
        $prep->bindparam(12, $this->transactionalAmount);
        $prep->bindparam(13, $this->cateringOperator);
        $prep->bindparam(14, $this->serviceProvider);
        $prep->bindparam(15, $this->id);
        //$prep->execute();
        // Get data of department before and after the execution of an action
//        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();

//            //record update
//            // Get data of department before and after the execution of an action
//            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
//            // instance the class auditing
//            $auditing = new Auditing($this->dbh);
//            $response = $auditing->insertDataAuditingFile('action', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
            //true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            // return false;
            return $e->getMessage();
        }
    }

    // Delete fair
    function deleteFair() {
        $cons = "DELETE FROM fair WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);

        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data of a specific fair
    function getDataFair($id) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM fair WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id'] = $reg->id;
                //Stancing the province Class
                $province = new Province($this->dbh);
                $province->id = $reg->id_province;
                $arrayData['province'] = $province->readDeterminedProvince()['designation'];
                $arrayData['held_fair'] = $reg->held_fair;
                $arrayData['id_municipal'] = $reg->quant_productor;
                $arrayData['quant_productor'] = $reg->quant_fisherman;
                $arrayData['quant_marketer'] = $reg->quant_marketer;
                $arrayData['quant_activity'] = $reg->quant_activity;
                $arrayData['quant_partnership'] = $reg->quant_partnership;
                $arrayData['quant_former'] = $reg->quant_former;
                $arrayData['quant_sponsor'] = $reg->quant_sponsor;
                $arrayData['quant_ocd'] = $reg->quant_ocd;
                $arrayData['made_contract'] = $reg->made_contract;
                $arrayData['transactional_amount'] = $reg->transactional_amount;
                $arrayData['catering_operator'] = $reg->catering_operator;
                $arrayData['service_provider'] = $reg->service_provider;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT department.name AS dep_name,department.acronym AS dep_acronym,department.description AS dep_desc,direction.description AS dir_desc FROM department 
				JOIN direction ON department.id_direction = direction.id WHERE department.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                //$arrayData['id'] = 'Identificador: '.$reg->id;
                $arrayData['name'] = 'Nome: ' . $reg->dep_name;
                $arrayData['acronym'] = 'Sigla: ' . $reg->dep_acronym;
                $arrayData['description'] = 'Descrição: ' . $reg->dep_desc;
                $arrayData['Direction'] = 'Direção: ' . $reg->dir_desc;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

    function checkFair() {
        $i = 0;
        $cons = "SELECT * FROM fair WHERE designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }

            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

}

?>