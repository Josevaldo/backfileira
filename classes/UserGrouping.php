<?php

require_once 'Auditing.php';
require_once 'User.php';
require_once 'GroupUser.php';

class UserGrouping {

    public $id;
    public $idUser;
    public $idGroupUser;
    public $observation;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create user grouping
    function registerUserGrouping() {
        $cons = "INSERT INTO user_grouping VALUES(?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idUser);
        $prep->bindparam(2, $this->idGroupUser);
        $prep->bindparam(3, $this->observation);
        //$prep->execute();
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            //$lastId = $this->dbh->lastInsertId();
            $this->id = array($this->idUser, $this->idGroupUser);
            // Get data of groupUser before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class groupUser
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('grupo utilizador', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
            //return $e->getMessage();
        }
    }

    // Read all user grouping
    function readUserGrouping() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM user_grouping";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                //$arrayData[$i]['id_user'] = $reg->id_user;
                // Get data of a spefic user
                $userData = new User($this->dbh);
                $arrayUserData = $userData->getDataUser($reg->id_user);
                $arrayData[$i]['user'] = $arrayUserData;
                //$arrayData[$i]['id_groupUser'] = $reg->id_groupUser;
                // Get data of a spefic groupUser
                $groupUserData = new GroupUser($this->dbh);
                $arrayGroupUserData = $groupUserData->getDataGroupUser($reg->id_group_user);
                $arrayData[$i]['group_user'] = $arrayGroupUserData;
                $arrayData[$i]['observation'] = $reg->observation;
                $i++;
            }
            //$arrayData[$i]['total_record'] = $i;
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Read determined user grouping
    function readDeterminedUserGrouping() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM user_grouping WHERE id_user = ? AND id_group_user = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                //$arrayData['id_user'] = $reg->id_user;
                // Get data of a spefic user
                $userData = new User($this->dbh);
                $arrayUserData = $userData->getDataUser($reg->id_user);
                $arrayData['user'] = $arrayUserData;
                //$arrayData['id_groupUser'] = $reg->id_groupUser;
                // Get data of a spefic groupUser
                $groupUserData = new groupUser($this->dbh);
                $arrayGroupUserData = $groupUserData->getDatagroupUser($reg->id_group_user);
                $arrayData['group_user'] = $arrayGroupUserData;
                $arrayData['observation'] = $reg->observation;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Update groupUser
    function updateUserGrouping() {
        $cons = "UPDATE user_grouping SET id_user = ?,id_group_user = ?,observation = ? WHERE id_user = ? AND id_group_user = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idUser);
        $prep->bindparam(2, $this->idGroupUser);
        $prep->bindparam(3, $this->observation);
        $prep->bindparam(4, $this->id[0]);
        $prep->bindparam(5, $this->id[1]);
        //$prep->execute();
        // Get data of groupUser before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();
            //record update
            // Get data of groupUser before and after the execution of an action
            $this->id = array($this->idUser, $this->idGroupUser);
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
            // instance the class groupUser
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('grupo utilizador', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
            //return $e->getMessage();
        }
    }

    // Delete user grouping
    function deleteUserGrouping() {
        $cons = "DELETE FROM user_grouping WHERE id_user = ? AND id_group_user = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $this->id[1], PDO::PARAM_STR);
        //$prep->execute();
        // Get data of groupUser before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class groupUser
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('grupo utilizador', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            //record deleted
            return true;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get user group
    function getUserGroupUser($idUser) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM user_grouping 
				JOIN group_user ON user_grouping.id_group_user = group_user.id
				WHERE id_user = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $idUser, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[$i]['id_group'] = $reg->id;
                $arrayData[$i]['name'] = $reg->name;
                $arrayData[$i]['description'] = $reg->description;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // List user of a user grouping
    function listUserOfUserGrouping($id_group_user) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM user_grouping WHERE id_group_user = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id_group_user, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                //$arrayData['id_user'] = $reg->id_user;
                // Get data of a spefic user
                $userData = new User($this->dbh);
                $arrayUserData = $userData->getDataUser($reg->id_user);
                $arrayData[$i]['user'] = $arrayUserData;
                $arrayData[$i]['observation'] = $reg->observation;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get user name of a user grouping
    function getUserNameOfUserGrouping($name_group_user) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT user.name AS usr_name FROM user_grouping
				JOIN user ON user.id = user_grouping.id_user
				JOIN group_user ON group_user.id = user_grouping.id_group_user
				WHERE group_user.name = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $name_group_user, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData[] = $reg->usr_name;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data user grouping
    function getDataUserGrouping($id) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM user_grouping WHERE id_user = ? AND id_group_user = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id[0], PDO::PARAM_STR);
        $prep->bindparam(2, $id[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                //$arrayData['id_user'] = $reg->id_user;
                // Get data of a spefic user
                $userData = new User($this->dbh);
                $arrayUserData = $userData->getDataUser($reg->id_user);
                $arrayData['user'] = $arrayUserData;
                //$arrayData['id_groupUser'] = $reg->id_groupUser;
                // Get data of a spefic groupUser
                $groupUserData = new GroupUser($this->dbh);
                $arrayGroupUserData = $groupUserData->getDataGroupUser($reg->id_group_user);
                $arrayData['group_user'] = $arrayGroupUserData;
                $arrayData['observation'] = $reg->observation;
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT user.name AS user_name,user.email,group_user.name AS gu_name,group_user.description,user_grouping.observation FROM user_grouping 
				JOIN user ON user.id = user_grouping.id_user
				JOIN group_user ON group_user.id = user_grouping.id_group_user
				WHERE user_grouping.id_user = ? 
				AND user_grouping.id_group_user = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId[0], PDO::PARAM_STR);
        $prep->bindparam(2, $DataId[1], PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                $arrayData['user_name'] = 'Utilizador: ' . $reg->user_name;
                $arrayData['user_email'] = 'Email: ' . $reg->email;
                $arrayData['group_user_name'] = 'Grupo: ' . $reg->gu_name;
                $arrayData['group_user_designation'] = 'Designação: ' . $reg->description;
                $arrayData['observation'] = 'Observação: ' . $reg->observation;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

}

?>