<?php

class DocumentStorage {

    public $objectIdentifier;
    public $fileName;
    public $fileTemporaryName;
    public $rep;
    public $typeDocument;
    public $downloadFilePath;
    public $dbh;
//public $fileDomainForDownload = 'http://be.intranetmep.mep.gov.ao/documents';
    public $fileDomainForDownload = '/backfileira/documents';

    function __construct($documentPurpose, $objectIdentifier, $dbh) {
        $this->objectIdentifier = $objectIdentifier;
        $this->dbh = $dbh;
        switch ($documentPurpose) {
            case 'rowPhoto':
                $this->rep = '../documents/rowPhoto/' . $this->objectIdentifier . '/';
                $this->downloadFilePath = $this->fileDomainForDownload . '/rowPhoto/' . $this->objectIdentifier . '/';
                $this->typeDocument = 'image';
                break;
            case 'subRowPhoto':
                $this->rep = '../documents/subRowPhoto/' . $this->objectIdentifier . '/';
                $this->downloadFilePath = $this->fileDomainForDownload . '/subRowPhoto/' . $this->objectIdentifier . '/';
                $this->typeDocument = 'image';
                break;
            case 'productPhoto':
                $this->rep = '../documents/productPhoto/' . $this->objectIdentifier . '/';
                $this->downloadFilePath = $this->fileDomainForDownload . '/productPhoto/' . $this->objectIdentifier . '/';
                $this->typeDocument = '';
                break;
            case 'subProductPhoto':
                $this->rep = '../documents/subProductPhoto/' . $this->objectIdentifier . '/';
                $this->downloadFilePath = $this->fileDomainForDownload . '/subProductPhoto/' . $this->objectIdentifier . '/';
                $this->typeDocument = '';
                break;
            case 'product_5_4_Photo':
                $this->rep = '../documents/product_5_4_Photo/' . $this->objectIdentifier . '/';
                $this->downloadFilePath = $this->fileDomainForDownload . '/product_5_4_Photo/' . $this->objectIdentifier . '/';
                $this->typeDocument = 'image';
                break;
            case 'bannerPhoto':
                $this->rep = '../documents/bannerPhoto/' . $this->objectIdentifier . '/';
                $this->downloadFilePath = $this->fileDomainForDownload . '/bannerPhoto/' . $this->objectIdentifier . '/';
                $this->typeDocument = 'image';
                break;

            case 'legislationPhoto':
                $this->rep = '../documents/legislationPhoto/' . $this->objectIdentifier . '/';
                $this->downloadFilePath = $this->fileDomainForDownload . '/legislationPhoto/' . $this->objectIdentifier . '/';
                $this->typeDocument = '';
                break;
            case 'rowPhoto54':
                $this->rep = '../documents/rowPhoto54/' . $this->objectIdentifier . '/';
                $this->downloadFilePath = $this->fileDomainForDownload . '/rowPhoto54/' . $this->objectIdentifier . '/';
                $this->typeDocument = 'image';
                break;
            case 'eventPhoto':
                $this->rep = '../documents/eventPhoto/' . $this->objectIdentifier . '/';
                $this->downloadFilePath = $this->fileDomainForDownload . '/eventPhoto/' . $this->objectIdentifier . '/';
                $this->typeDocument = 'image';
                break;
            case 'galleryPhoto':
                $this->rep = '../documents/galleryPhoto/' . $this->objectIdentifier . '/';
                $this->downloadFilePath = $this->fileDomainForDownload . '/galleryPhoto/' . $this->objectIdentifier . '/';
                $this->typeDocument = 'image';
                break;
            case 'productBalancePhoto':
                $this->rep = '../documents/productBalancePhoto/' . $this->objectIdentifier . '/';
                $this->downloadFilePath = $this->fileDomainForDownload . '/productBalancePhoto/' . $this->objectIdentifier . '/';
                $this->typeDocument = 'image';
                break;
            default:
                $this->downloadFilePath = $this->fileDomainForDownload . '/csvDocument/';
                $this->rep = '../documents/csvDocument/';
        }
//return $this->rep;
    }

// store document
    function storeDocument() {
        if ($this->typeDocument == 'image')
            $fileExtension = array('tiff', 'tif', 'jpeg', 'jpg', 'png', 'gif', 'svg');
        else
            $fileExtension = array('tiff', 'tif', 'jpeg', 'jpg', 'png', 'pdf', 'gif', 'doc', 'docx', 'dot', 'dotx', 'xlsx', 'xls', 'xlsm', 'xlsb', 'xltx', 'txt', 'pptx', 'pptm', 'ppt', 'pub', 'csv');
        $fileBaseName = basename($this->fileName);
        $path = $this->fileName;
        $extension = strtolower(pathinfo($path, PATHINFO_EXTENSION));
        $filePointer = $this->rep;
        if (in_array($extension, $fileExtension)) {
            if (file_exists($filePointer)) {
                $moveUploadedFile = move_uploaded_file($this->fileTemporaryName, $filePointer . $fileBaseName);
                if ($moveUploadedFile)
                    return true;
                else
                    return false;
            } else {
                if (!empty($this->objectIdentifier)) {
                    $createDocumentImage = mkdir($filePointer);
                    if ($createDocumentImage) {
                        $moveUploadedFile = move_uploaded_file($this->fileTemporaryName, $filePointer . $fileBaseName);
                        if ($moveUploadedFile)
                            return true;
                        else
                            return false;
                    } else
                        return false;
                } else
                    return false;
            }
        } else
            return false;
    }

// store document with default document
    function storeDocumentDefaultDocument() {
//if($this->typeDocument == 'image') $fileExtension = array('tiff','tif','jpeg','jpg','png','gif','svg');
//else $fileExtension = array('tiff','tif','jpeg','jpg','png','pdf','gif','doc','docx','dot','dotx','xlsx','xls','xlsm','xlsb','xltx','txt','pptx','pptm','ppt','pub','csv');
        if (!file_exists($this->rep))
            mkdir($this->rep);
        $filePointer = $this->rep . 'defaultDocument.txt';
//$myfile = fopen($filePointer, "w") or die("Unable to open file!");
        $myfile = fopen($filePointer, "w");
        $txt = "Nehum documento submetido\n";
        fwrite($myfile, $txt);
        $txt1 = "Nehum documento submetido\n";
        fwrite($myfile, $txt1);
        fclose($myfile);
    }

// Alter document 
    function alterDocument() {
        $documentAltered = '';
        $i = 0;
        if (file_exists($this->rep)) {
            $files = scandir($this->rep);
            foreach ($files as $file) {
                if (($file != '.') AND ($file != '..')) {
                    $fName = $this->rep . $file;
                    if ($fName)
                        unlink($fName);
// store document
                    $documentAltered = $this->storeDocument();
                }
                $i++;
            }
        } else {
// Get data before and after the execution of an action
//$dataBeforeExecution = '';
// store document
            $documentAltered = $this->storeDocument();
            $i++;
        }
// Get data before and after the execution of an action
//$dataAfterExecution = $fileName;
// instance the class user
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile($this->rep, $this->typeDocument, '', '');
        return $i;
    }

// Alter document, version that allow submit more document text in a repository
    function alterDocument1() {
        $documentAltered = '';
        $i = 0;
        if (file_exists($this->rep)) {
            $files = scandir($this->rep);
            foreach ($files as $file) {
                if (($file != '.') AND ($file != '..')) {
                    $fName = $this->rep . $file;
                    if (($this->typeDocument == 'image') || ($this->typeDocument == 'proof')) {
// Get data before and after the execution of an action
//$dataBeforeExecution = $fName;
                        if ($fName)
                            unlink($fName);
                    }
// store document
                    $documentAltered = $this->storeDocument();
                }
                $i++;
            }
        } else {
// Get data before and after the execution of an action
//$dataBeforeExecution = '';
// store document
            $documentAltered = $this->storeDocument();
            $i++;
        }
// Get data before and after the execution of an action
//$dataAfterExecution = $fileName;
// instance the class user
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile($this->rep, $this->typeDocument, '', '');
        return $i;
    }

// Get document
    function getDocument() {
        $i = 0;
//$list = '';
        $list = array();
        if (file_exists($this->rep)) {
            $files = scandir($this->rep);
            foreach ($files as $file) {
//if(($file != '.') and ($file != '..')) $list = $this->downloadFilePath.$file;
                if (($file != '.') and ($file != '..'))
                    $list[] = $this->downloadFilePath . $file;
                $i++;
            }
        } else
            $list = array();
        return $list;
    }

// Get repository path
    function getRepositoryPath() {
        return $this->rep;
    }

// Get download File path
    function getDownloadFilePath() {
        return $this->downloadFilePath;
    }

// remove a specific file
    function removeSpecificFile($fileName) {
// Get repository path
        $rep = $this->getRepositoryPath();
        $fileToRemove = $rep . $fileName;
        if (file_exists($fileToRemove))
            $resp = unlink($fileToRemove);
        else
            $resp = false;
        return $rep;
    }

// Remove a directory with all kind of contents
    function removeDirectory($dir) {
//DIRECTORY_SEPARATOR = '/';
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (is_dir($dir . DIRECTORY_SEPARATOR . $object) && !is_link($dir . "/" . $object))
                        removeDirectory($dir . DIRECTORY_SEPARATOR . $object);
                    else
                        unlink($dir . DIRECTORY_SEPARATOR . $object);
                }
            }
            rmdir($dir);
        }
    }

}

?>