<?php

require_once 'Province.php';

class Ppn {

    public $id;
    public $idProvince;
    public $agricuture;
    public $livestock;
    public $fishing;
    public $productionIndustry;
    public $dbh;

    function __construct($dbh) {
        $this->dbh = $dbh;
    }

    // Create ppn
    function registerPpn() {
        $cons = "INSERT INTO ppn VALUES(?,?,?,?,?,?)";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id);
        $prep->bindparam(2, $this->idProvince);
        $prep->bindparam(3, $this->agricuture);
        $prep->bindparam(4, $this->livestock);
        $prep->bindparam(5, $this->fishing);
        $prep->bindparam(6, $this->productionIndustry);
        try {
            $prep->execute();
            //record inserted
            // Insert data in the auditing file
            $lastId = $this->dbh->lastInsertId();
            $dataAfterExecution = $this->getDataBeforeAfterAction($lastId);
            // instance the class Auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('ppn', 'inserir', '', $dataAfterExecution);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    // Read all ppn
    function readPpn() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM ppn";
        $prep = $this->dbh->prepare($cons);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {

                $arrayData[$i]['id'] = $reg->id;
                //Stancing the province Class
                $province = new Province($this->dbh);
                $province->id = $reg->id_province;
                $arrayData[$i]['province'] = $province->readDeterminedProvince()['designation'];
                $arrayData[$i]['agricuture'] = $reg->agricuture;
                $arrayData[$i]['livestock'] = $reg->livestock;
                $arrayData[$i]['fishing'] = $reg->fishing;
                $arrayData[$i]['production_industry'] = $reg->production_industry;
                $arrayData[$i]['total'] = ($reg->agricuture + $reg->livestock + $reg->fishing + $reg->production_industry);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            return false;
        }
    }

    // Read determined Ppn
    function readDeterminedPpn() {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM ppn WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                //Stancing the province Class
                $province = new Province($this->dbh);
                $province->id = $reg->id_province;
                $arrayData['province'] = $province->readDeterminedProvince()['designation'];
                $arrayData['agricuture'] = $reg->agricuture;
                $arrayData['livestock'] = $reg->livestock;
                $arrayData['fishing'] = $reg->fishing;
                $arrayData['production_industry'] = $reg->production_industry;
                $arrayData['total'] = ($reg->agricuture + $reg->livestock + $reg->fishing + $reg->production_industry);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            return false;
        }
    }

    // Update ppn
    function updatePpn() {
        $cons = "UPDATE ppn SET id_province = ?, agricuture = ?, livestock = ?, fishing = ?, production_industry = ? WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->idProvince);
        $prep->bindparam(2, $this->agricuture);
        $prep->bindparam(3, $this->livestock);
        $prep->bindparam(4, $this->fishing);
        $prep->bindparam(5, $this->productionIndustry);
        $prep->bindparam(6, $this->id);

//         Get data of department before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        try {
            $prep->execute();

//            //record update
//            // Get data of ppn before and after the execution of an action
            $dataAfterExecution = $this->getDataBeforeAfterAction($this->id);
//            // instance the class auditing
            $auditing = new Auditing($this->dbh);
            $response = $auditing->insertDataAuditingFile('ppn', 'alterar', $dataBeforeExecution, $dataAfterExecution);
            return true;
            //true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    // Delete ppn
    function deletePpn() {
        $cons = "DELETE FROM ppn WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->id, PDO::PARAM_STR);
        //$prep->execute();
        // Get data of ppn before and after the execution of an action
        $dataBeforeExecution = $this->getDataBeforeAfterAction($this->id);
        // instance the class department
        $auditing = new Auditing($this->dbh);
        $response = $auditing->insertDataAuditingFile('ppn', 'eliminar', $dataBeforeExecution, '');
        try {
            $prep->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    // Get data of a specific ppn
    function getDataPpn($id) {
        $i = 0;
        $arrayData = [];
        $cons = "SELECT * FROM ppn WHERE id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $id, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $arrayData['id'] = $reg->id;
                //Stancing the province Class
                $province = new Province($this->dbh);
                $province->id = $reg->id_province;
                $arrayData['province'] = $province->readDeterminedProvince()['designation'];
                $arrayData['agricuture'] = $reg->agricuture;
                $arrayData['livestock'] = $reg->livestock;
                $arrayData['fishing'] = $reg->fishing;
                $arrayData['production_industry'] = $reg->production_industry;
                $arrayData['total'] = ($reg->agricuture + $reg->livestock + $reg->fishing + $reg->production_industry);
                $i++;
            }
            return $arrayData;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

    // Get data before and after the execution of an action
    function getDataBeforeAfterAction($DataId) {
        $i = 0;
        $arrayData = [];
        $dataReceivedFormated = '';
        $cons = "SELECT department.name AS dep_name,department.acronym AS dep_acronym,department.description AS dep_desc,direction.description AS dir_desc FROM department 
				JOIN direction ON department.id_direction = direction.id WHERE department.id = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $DataId, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                //$i++;
                //$arrayData['id'] = 'Identificador: '.$reg->id;
                $arrayData['name'] = 'Nome: ' . $reg->dep_name;
                $arrayData['acronym'] = 'Sigla: ' . $reg->dep_acronym;
                $arrayData['description'] = 'Descrição: ' . $reg->dep_desc;
                $arrayData['Direction'] = 'Direção: ' . $reg->dir_desc;
                $i++;
            }
            //Format data of the system element
            if ($arrayData) {
                foreach ($arrayData as $dr) {
                    $dataReceivedFormated .= $dr . ', ';
                }
                $dataReceivedFormated = substr($dataReceivedFormated, 0, -2);
            } else
                $dataReceivedFormated = '';
            return $dataReceivedFormated;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            //return $e->getMessage();
            return false;
        }
    }

    function checkPpn() {
        $i = 0;
        $cons = "SELECT * FROM ppn WHERE designation = ?";
        $prep = $this->dbh->prepare($cons);
        $prep->bindparam(1, $this->designation, PDO::PARAM_STR);
        try {
            $prep->execute();
            while ($reg = $prep->fetch(PDO::FETCH_OBJ)) {
                $i++;
            }

            return $i;
        } catch (Exception $e) {
            //Some error occured. (i.e. violation of constraints)
            return false;
        }
    }

}

?>