<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Product_5_4.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class Product_5_4
$product = new Product_5_4($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if ($token) {
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
        $product->designation = $data->designation;
        if ($product->checkProduct_5_4())
            $responseReturned = $returned->returnResult(true, 'Este produto já existe no sistema', array());
        else {
            $product->id = NULL;
            $product->designation = $data->designation;
            $product->idProductiveRow = $data->id_productive_row;

            $response = $product->registerProduct_5_4();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Produto registado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Produto não registado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    //if($token){
    $response = $product->readProduct_5_4(); // Read all product_5_4
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Produto encontrado', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhum produto encontrado', array());
    //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update Product_5_4
//        $product->designation = $data->designation;
//        if ($banner->checkBanner())
//            $responseReturned = $returned->returnResult(true, 'Este banner já existe no sistema', array());
//        else {
        $product->id = $data->id;
        $product->designation = $data->designation;
        $product->idProductiveRow = $data->id_productive_row;
        $response = $product->updateProduct_5_4();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Produto actualizado com successo', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Produto não actualizado', array());
//        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        // Delete indicator
        //$direction->id = $data->id;
        foreach ($data->id as $id) {
            $product->id = $id;
            // Retrieve the response about the delete of Product_5_4
            $response = $product->deleteProduct_5_4();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Produto(s)eliminado(s) com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Produto(s) não eliminado(s)', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//} else
//    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>