<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/ProductiveRow54.php";
require_once "../classes/UserToken.php";
require_once "../classes/Profile.php";
require_once "../classes/DocumentStorage.php";
require_once "../classes/Returned.php";
//require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class user
$productiveRow = new ProductiveRow54($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
/* $json = file_get_contents('php://input');
  $data = json_decode($json); */
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        //if($token){
        if ((empty($_FILES['rowPhoto54']['name'])))
            $responseReturned = $returned->returnResult(false, 'Falta imagens para submeter', array());
        else {
            $data = json_decode($_POST['productiveData']);
            $productiveRow->id = NULL;
            $productiveRow->designation = $data->designation;
            $productiveRow->description = $data->description;

            //$user->role = $data->role;
            // Check if the email already exists
            $productiveRowExist = $productiveRow->checkProductiveRow54();
            if ($productiveRowExist)
                $responseReturned = $returned->returnResult(false, 'Esta fileira já existe no sistema', array());
            else {
                // Check if the tax identification number already exists
                //$tin = $user->checkTaxIdentificationNumberExists();
                //if($tin) $responseReturned = $returned->returnResult(false,'Este NIF já existe no sistema',array());
                //else{
                // Retrieve the response about the register of user
                $response = $productiveRow->registerProductiveRow54();
                // Return the result
                //if($response) $responseReturned = $returned->returnResult(true,'user registado com successo',$response);
                if ($response) {
                    $productiveRow->id = $response;
                    $productiveRow->rowPhoto54['fileName'] = $_FILES['rowPhoto54']['name'];
                    $productiveRow->rowPhoto54['tmpName'] = $_FILES['rowPhoto54']['tmp_name'];
                    // instance the class DocumentStorage
                    $rowPhotoSubmited = new DocumentStorage('rowPhoto54', $productiveRow->id, $db);
                    $rowPhotoSubmited->fileName = $_FILES['rowPhoto54']['name'];
                    $rowPhotoSubmited->fileTemporaryName = $_FILES['rowPhoto54']['tmp_name'];
                    // store document
                    $rowPhotoStored = $rowPhotoSubmited->storeDocument();
                    // instance the class DocumentStorage to birth day person image

                    if (($rowPhotoStored))
                        $responseReturned = $returned->returnResult(true, ' Fileira registada com successo', array());
                    else
                        $responseReturned = $returned->returnResult(false, 'Fileira não registado', array());
                } else
                    $responseReturned = $returned->returnResult(false, 'Fileira não registado', array());
                //}
            }
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>