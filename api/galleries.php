<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Gallery.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class gallery
$gallery = new Gallery($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
//        $legislation->title = $data2[1];
        $gallery->title = $data->title;
//        $legislation->designation = $data2[1];
        if ($gallery->checkGallery())
            $responseReturned = $returned->returnResult(true, 'Esta foto já existe no sistema', array());
        else {

            $gallery->id = NULL;
            $gallery->title = $data->title;
            $response = $gallery->registerGallery();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Foto registada com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Foto não registada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $gallery->readGallery(); // Read all photos
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Foto encontrada', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhuma Foto encontrada', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update  Legislation
//        $productiveRow->designation = $data->designation;
//        if ($productiveRow->checkProductiveRow())
//            $responseReturned = $returned->returnResult(true, 'Esta Fileira já existe no sistema', array());
//        else {
        $gallery->id = $data->id;
        $gallery->title = $data->title;

        $response = $gallery->updateGallery();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Foto actualizada com successo', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Foto não actualizada', array());
//        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        foreach ($data->id as $idd) {
            // Delete Gallery
            $gallery->id = $idd;
            // Retrieve the response about the delete of Legislation
            $response = $gallery->deletegallery();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Foto eliminada com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Foto não eliminada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>