<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/ProductBalance.php";
require_once "../classes/UserToken.php";
require_once "../classes/Profile.php";
require_once "../classes/DocumentStorage.php";
require_once "../classes/Returned.php";
//require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class product Balance
$product = new ProductBalance($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        //if($token){
        if ((empty($_FILES['productBalancePhoto']['name'])))
            $responseReturned = $returned->returnResult(false, 'Falta imagens para submeter', array());
        else {
            $data = json_decode($_POST['product_data']);
            $product->id = null;
            $product->designation = $data->designation;
            $product->comment = $data->comment;

            $productExist = $product->checkProductBalance();
            if ($productExist)
                $responseReturned = $returned->returnResult(false, 'Este producto já existe no sistema', array());
            else {
                $response = $product->registerProductBalance();
                // Return the result
                //if($response) $responseReturned = $returned->returnResult(true,'user registado com successo',$response);
                if ($response) {
                    $product->id = $response;
                    $product->productBalancePhoto['fileName'] = $_FILES['productBalancePhoto']['name'];
                    $product->productBalancePhoto['tmpName'] = $_FILES['productBalancePhoto']['tmp_name'];
                    // instance the class DocumentStorage
                    $productBalancePhotoSubmited = new DocumentStorage('productBalancePhoto', $product->id, $db);
                    $productBalancePhotoSubmited->fileName = $_FILES['productBalancePhoto']['name'];
                    $productBalancePhotoSubmited->fileTemporaryName = $_FILES['productBalancePhoto']['tmp_name'];
                    // store document
                    $productBalancePhotoStored = $productBalancePhotoSubmited->storeDocument();

                    if (($productBalancePhotoStored))
                        $responseReturned = $returned->returnResult(true, 'Produto registado com successo', array());
                    else
                        $responseReturned = $returned->returnResult(false, 'Produto não registado', array());
                } else
                    $responseReturned = $returned->returnResult(false, 'Produto não registado', array());
                //}
            }
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>