<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Banner.php";
require_once "../classes/UserToken.php";
require_once "../classes/DocumentStorage.php";
require_once "../classes/Returned.php";
//require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class banner
$banner = new Banner($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
/* $json = file_get_contents('php://input');
  $data = json_decode($json); */
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        //if($token){
        if ((empty($_FILES['bannerPhoto']['name'])))
            $responseReturned = $returned->returnResult(false, 'Falta imagens para submeter', array());
        else {
            $data = json_decode($_POST['bannerData']);
            $banner->id = NULL;
            $banner->title = $data->title;
            $banner->description = $data->description;

            // Check if the banner already exists
            $bannerExist = $banner->checkBanner();
            if ($bannerExist)
                $responseReturned = $returned->returnResult(false, 'Este banner já existe no sistema', array());
            else {
                $response = $banner->registerBanner();
                // Return the result
                //if($response) $responseReturned = $returned->returnResult(true,'user registado com successo',$response);
                if ($response) {
                    $banner->id = $response;
                    $banner->bannerPhoto['fileName'] = $_FILES['bannerPhoto']['name'];
                    $banner->bannerPhoto['tmpName'] = $_FILES['bannerPhoto']['tmp_name'];
                    // instance the class DocumentStorage
                    $bannerPhotoSubmited = new DocumentStorage('bannerPhoto', $banner->id, $db);
                    $bannerPhotoSubmited->fileName = $_FILES['bannerPhoto']['name'];
                    $bannerPhotoSubmited->fileTemporaryName = $_FILES['bannerPhoto']['tmp_name'];
                    // store document
                    $bannerPhotoStored = $bannerPhotoSubmited->storeDocument();

                    if (($bannerPhotoStored))
                        $responseReturned = $returned->returnResult(true, 'Banner registado com successo', array());
                    else
                        $responseReturned = $returned->returnResult(false, 'Banner não registado', array());
                } else
                    $responseReturned = $returned->returnResult(false, 'Banner não registado', array());
                //}
            }
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>