<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/SubProduct.php";
require_once "../classes/UserToken.php";
require_once "../classes/Profile.php";
require_once "../classes/DocumentStorage.php";
require_once "../classes/Returned.php";
//require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class Sub product
$subProduct = new SubProduct($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        //if($token){
        if ((empty($_FILES['subProductPhoto']['name'])))
            $responseReturned = $returned->returnResult(false, 'Falta imagens para submeter', array());
        else {
            $data = json_decode($_POST['sub_product_data']);
            $subProduct->id = null;
            $subProduct->designation = $data->designation;
            $subProduct->customsTariff = $data->customs_tariff;
            $subProduct->prodesiProduct = $data->prodesi_product;

            $subProductExist = $subProduct->checkSubPruduct();
            if ($subProductExist)
                $responseReturned = $returned->returnResult(false, 'Este subproducto já existe no sistema', array());
            else {
                $response = $subProduct->registerSubProduct();
                // Return the result
                //if($response) $responseReturned = $returned->returnResult(true,'user registado com successo',$response);
                if ($response) {
                    $subProduct->id = $response;
                    $subProduct->subProductPhoto['fileName'] = $_FILES['subProductPhoto']['name'];
                    $subProduct->subProductPhoto['tmpName'] = $_FILES['subProductPhoto']['tmp_name'];
                    // instance the class DocumentStorage
                    $subProductPhotoSubmited = new DocumentStorage('subProductPhoto', $subProduct->id, $db);
                    $subProductPhotoSubmited->fileName = $_FILES['subProductPhoto']['name'];
                    $subProductPhotoSubmited->fileTemporaryName = $_FILES['subProductPhoto']['tmp_name'];
                    // store document
                    $subProductPhotoStored = $subProductPhotoSubmited->storeDocument();
                    if (($subProductPhotoStored))
                        $responseReturned = $returned->returnResult(true, ' Subproduto registado com successo', array());
                    else
                        $responseReturned = $returned->returnResult(false, 'Subproduto não registado', array());
                } else
                    $responseReturned = $returned->returnResult(false, 'Subproduto não registado', array());
                //}
            }
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>