<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/User.php";
require_once "../classes/UserToken.php";
require_once "../classes/Profile.php";
require_once "../classes/DocumentStorage.php";
require_once "../classes/Returned.php";
//require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class user
$user = new User($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
/* $json = file_get_contents('php://input');
  $data = json_decode($json); */
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        //if($token){
        if ((empty($_FILES['userPhoto']['name'])) || (empty($_FILES['birthdayPersonImage']['name'])))
            $responseReturned = $returned->returnResult(false, 'Falta imagens para submeter', array());
        else {
            $data = json_decode($_POST['userData']);
            $user->id = NULL;
            $user->name = $data->name;
            $user->email = $data->email;
            $user->password = password_hash($data->password, PASSWORD_DEFAULT);
            $user->telephone = $data->telephone;
            $user->dateBirth = $data->date_birth;
            $user->idOffice = $data->id_office;
            $user->idDepartment = $data->id_department;
            //$user->role = $data->role;
            // Check if the email already exists
            $emailExist = $user->checkEmailExists();
            if ($emailExist)
                $responseReturned = $returned->returnResult(false, 'Este email já existe no sistema', array());
            else {
                // Check if the tax identification number already exists
                //$tin = $user->checkTaxIdentificationNumberExists();
                //if($tin) $responseReturned = $returned->returnResult(false,'Este NIF já existe no sistema',array());
                //else{
                // Retrieve the response about the register of user
                $response = $user->registerUser();
                // Return the result
                //if($response) $responseReturned = $returned->returnResult(true,'user registado com successo',$response);
                if ($response) {
                    $user->id = $response;
                    $user->userPhoto['fileName'] = $_FILES['userPhoto']['name'];
                    $user->userPhoto['tmpName'] = $_FILES['userPhoto']['tmp_name'];
                    // instance the class DocumentStorage
                    $userPhotoSubmited = new DocumentStorage('userPhoto', $user->id, $db);
                    $userPhotoSubmited->fileName = $_FILES['userPhoto']['name'];
                    $userPhotoSubmited->fileTemporaryName = $_FILES['userPhoto']['tmp_name'];
                    // store document
                    $userPhotoStored = $userPhotoSubmited->storeDocument();
                    // instance the class DocumentStorage to birth day person image
                    $birthdayImageSubmited = new DocumentStorage('birthdayPersonImage', $user->id, $db);
                    $birthdayImageSubmited->fileName = $_FILES['birthdayPersonImage']['name'];
                    $birthdayImageSubmited->fileTemporaryName = $_FILES['birthdayPersonImage']['tmp_name'];
                    // store document
                    $birthdayImageStored = $birthdayImageSubmited->storeDocument();
                    // instance the class DocumentStorage for other document of a user
                    $otherDocumentSubmited = new DocumentStorage('otherDocument', $user->id, $db);
                    if (empty($_FILES['otherDocument']['name'])) {
                        // store document with default document
                        $defaultDocument = $otherDocumentSubmited->storeDocumentDefaultDocument();
                    } else {
                        $numberOtherDocument = count($_FILES['otherDocument']['name']);
                        for ($i = 0; $i < $numberOtherDocument; $i++) {
                            $otherDocumentSubmited->fileName = $_FILES['otherDocument']['name'][$i];
                            $otherDocumentSubmited->fileTemporaryName = $_FILES['otherDocument']['tmp_name'][$i];
                            // store document
                            $otherDocumentStored = $otherDocumentSubmited->storeDocument();
                        }
                    }
                    if (($birthdayImageStored) AND ($userPhotoStored))
                        $responseReturned = $returned->returnResult(true, 'Usuário registado com successo', array());
                    else
                        $responseReturned = $returned->returnResult(false, 'Usuário não registado', array());
                } else
                    $responseReturned = $returned->returnResult(false, 'Usuário não registado', array());
                //}
            }
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>