<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Region.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class Region
$region = new Region($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
        $region->designation = $data->designation;
        if ($region->checkRegion())
            $responseReturned = $returned->returnResult(true, 'Esta região já existe no sistema', array());
        else {
            $region->id = NULL;
            $region->designation = $data->designation;
            $region->description = $data->description;
            $response = $region->registerRegion();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Região registada com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Região não registada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
//        if($token){
    $response = $region->readRegion(); // Read all regions
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Região encontrada', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhuma região encontrada', array());
//        }else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update region
//        $region->designation = $data->designation;
//        if ($region->checkRegion())
//            $responseReturned = $returned->returnResult(true, 'Esta região já existe no sistema', array());
//        else {
            $region->id = $data->id;
            $region->designation = $data->designation;
            $region->description = $data->description;
            $response = $region->updateRegion();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Região actualizada com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Região não actualizada', array());
//        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        // Delete department
        //$department->id = $data->id;
        foreach ($data->id as $id) {
            $region->id = $id;
            // Retrieve the response about the delete of department
            $response = $region->deleteRegion();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Região eliminada com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Região não eliminada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}

$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>