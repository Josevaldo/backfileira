<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/TypeProduction.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class typeProduction
$typeProduction = new TypeProduction($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
        $typeProduction->designation = $data->designation;
        if ($typeProduction->checkTypeProduct())
            $responseReturned = $returned->returnResult(true, 'Este Tipo de produção já existe no sistema', array());
        else {
            $typeProduction->id = NULL;
            $typeProduction->designation = $data->designation;
            $typeProduction->comment = $data->comment;
            // Retrieve the response about the register of typeProduction
            $response = $typeProduction->registerTypeProduction();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Tipo de produção registado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Tipo de produção não registado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $typeProduction->readTypeProduction(); // Read all typeProduction
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Tipo de produção encontrado', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhum tipo de produção encontrado', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update typeProduction
//        $typeProduction->designation = $data->designation;
//        if ($typeProduction->checkTypeProduct())
//            $responseReturned = $returned->returnResult(true, 'Este Tipo de produção já existe no sistema', array());
//        else {
            $typeProduction->id = $data->id;
            $typeProduction->designation = $data->designation;
            $typeProduction->comment = $data->comment;
            // Retrieve the response about the register of typeProduction
            $response = $typeProduction->updateTypeProduction();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Tipo de produção actualizado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Tipo de produção não actualizado', array());
//        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        foreach ($data->id as $idd) {
            // Delete typeProduction
            $typeProduction->id = $idd;
            // Retrieve the response about the delete of typeProduction
            $response = $typeProduction->deleteTypeProduction();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Tipo de produção eliminado com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Tipo de produção não eliminado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>