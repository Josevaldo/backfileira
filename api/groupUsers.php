<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/GroupUser.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class groupUser
$groupUser = new GroupUser($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if ($token) {
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
        $groupUser->name = $data->name;
        if ($groupUser->checkGroupUser())
            $responseReturned = $returned->returnResult(true, 'Este grupo já existe no sistema!', array());
        else {
            $groupUser->id = NULL;
            $groupUser->description = $data->description;
            $groupUser->name = $data->name;
            $response = $groupUser->registerGroupUser();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Grupo registado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Grupo não registado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    //if($token){
    $response = $groupUser->readGroupUser(); // Read all groupUser
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Grupo encontrado', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhum grupo encontrado', array());
    //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update groupUser
        $groupUser->name = $data->name;
//        if ($groupUser->checkGroupUser())
            $responseReturned = $returned->returnResult(true, 'Este grupo já existe no sistema!', array());
//        else {
            $groupUser->id = $data->id;
            $groupUser->description = $data->description;
            $groupUser->name = $data->name;
            $response = $groupUser->updateGroupUser();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Grupo actualizado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Grupo não actualizado', array());
//        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        // Delete groupUser
        //$groupUser->id = $data->id;
        foreach ($data->id as $id) {
            $groupUser->id = $id;
            // Retrieve the response about the delete of groupUser
            $response = $groupUser->deleteGroupUser();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Grupo eliminado com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Grupo não eliminado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//} else
//    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>