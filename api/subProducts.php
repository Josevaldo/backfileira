<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/SubProduct.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class subproduct
$subProduct = new SubProduct($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
// Register product
        $subProduct->designation = $data->designation;
        if ($subProduct->checkSubPruduct())
            $responseReturned = $returned->returnResult(true, 'Este subproduto já existe no sistema', array());
        else {
            $subProduct->id = null;
            $subProduct->designation = $data->designation;
            $subProduct->customsTariff = $data->customs_tariff;
            $subProduct->prodesiProduct = $data->prodesi_product;
            // Retrieve the response about the update of product
            $response = $subProduct->registerSubProduct();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Subproduto registrado com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Subproduto não regsitrado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $subProduct->readSubProduct(); // Read all subproduct
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Subproduto encontrado', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhum Subproduto encontrado', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update subproduct
//        $subProduct->designation = $data->designation;
//        if ($subProduct->checkSubPruduct())
//            $responseReturned = $returned->returnResult(true, 'Este Subproduto já existe no sistema', array());
//        else {
            $subProduct->id = $data->id;
            $subProduct->designation = $data->designation;
            $subProduct->customsTariff = $data->customs_tariff;
            $subProduct->prodesiProduct = $data->prodesi_product;
            // Retrieve the response about the update of product
            $response = $subProduct->updateSubProduct();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Subproduto actualizado com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Subproduto não actualizado', array());
//        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        // Delete product
        foreach ($data->id as $idd) {
            $subProduct->id = $idd;
            // Retrieve the response about the delete of product
            $response = $subProduct->deleteSubProduct();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Subproduto eliminado com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Subproduto não eliminado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>