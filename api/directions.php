<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Direction.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class direction
$direction = new Direction($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if ($token) {
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
        $direction->name = $data->name;
        if ($direction->checkDirection())
            $responseReturned = $returned->returnResult(true, 'Esta direcção já existe no sistema', array());
        else {
            $direction->id = NULL;
            $direction->description = $data->description;
            $direction->name = $data->name;
            $direction->acronym = $data->acronym;
            $response = $direction->registerDirection();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Direção registada com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Direção não registada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    //if($token){
    $response = $direction->readDirection(); // Read all direction
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Direção encontrada', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhum Direção encontrada', array());
    //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update direction
        $direction->name = $data->name;
        if ($direction->checkDirection())
            $responseReturned = $returned->returnResult(true, 'Esta direcção já existe no sistema', array());
        else {
            $direction->id = $data->id;
            $direction->description = $data->description;
            $direction->name = $data->name;
            $direction->acronym = $data->acronym;
            $response = $direction->updateDirection();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Direção actualizada com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Direção não actualizada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        // Delete direction
        //$direction->id = $data->id;
        foreach ($data->id as $id) {
            $direction->id = $id;
            // Retrieve the response about the delete of direction
            $response = $direction->deleteDirection();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Direção eliminada com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Direção não eliminada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//} else
//    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>