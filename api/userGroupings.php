<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/UserGrouping.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class userGrouping
$userGrouping = new UserGrouping($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if ($token) {
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
        $userGrouping->idUser = $data->id_user;
        $userGrouping->idGroupUser = $data->id_group_user;
        $userGrouping->observation = $data->observation;
        // Retrieve the response about the register of userGrouping
        $response = $userGrouping->registeruserGrouping();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Grupo utilizador registado com successo', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Grupo utilizador não registado', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $userGrouping->readUserGrouping(); // Read all userGrouping
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Grupos utilizador encontrados', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhum grupo utilizador encontrado', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update userGrouping
        $userGrouping->id = $data->id;
        $userGrouping->idUser = $data->id_user;
        $userGrouping->idGroupUser = $data->id_group_user;
        $userGrouping->observation = $data->observation;
        // Retrieve the response about the update of userGrouping
        $response = $userGrouping->updateUserGrouping();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Grupo utilizador actualizado com successo', array());
        else
            $responseReturned = $returned->returnResult(false, 'Grupos não actualizado', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {

        // Delete userGrouping
        //$userGrouping->id = $data->id;
        foreach ($data->id as $id) {
            $userGrouping->id = $id;
            // Retrieve the response about the delete of userGrouping
            $response = $userGrouping->deleteUserGrouping();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Grupo utilizador eliminado com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Grupo utilizador não eliminado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//} else
//    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>