<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/ProductiveRow.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class productiveRow
$productiveRow = new ProductiveRow($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
        $productiveRow->designation = $data->designation;
        if ($productiveRow->checkProductiveRow())
            $responseReturned = $returned->returnResult(true, 'Esta Fileira já existe no sistema', array());
        else {
            $productiveRow->id = NULL;
            $productiveRow->designation = $data->designation;
            $productiveRow->comment = $data->comment;
            $productiveRow->idTypeProduction = $data->id_type_production;
            // Retrieve the response about the register of productive Row
            $response = $productiveRow->registerProductiveRow();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Fileira registada com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Fileira não registada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $productiveRow->readProductiveRow(); // Read all productiveRow
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Fileira encontrada', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhuma fileira encontrada', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update productive Row
//        $productiveRow->designation = $data->designation;
//        if ($productiveRow->checkProductiveRow())
//            $responseReturned = $returned->returnResult(true, 'Esta Fileira já existe no sistema', array());
//        else {
        $productiveRow->id = $data->id;
        $productiveRow->designation = $data->designation;
        $productiveRow->comment = $data->comment;
        $productiveRow->idTypeProduction = $data->id_type_production;
        // Retrieve the response about the register of productive Row
        $response = $productiveRow->updateProductiveRow();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Fileira actualizada com successo', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Fileira não actualizada', array());
//        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        foreach ($data->id as $idd) {
            // Delete productiveRow
            $productiveRow->id = $idd;
            // Retrieve the response about the delete of productive Row
            $response = $productiveRow->deleteProductiveRow();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Fileira eliminada com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Fileira não eliminada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>