<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: PUT,POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/ProductiveSubRow.php";
require_once "../classes/Returned.php";
require_once "../classes/DocumentStorage.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class user
$productivesubRow = new ProductiveSubRow($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
/* $json = file_get_contents('php://input');
  $data = json_decode($json); */
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    // Get the authorization to access resource
    $token = $userToken->getAuthorization();
    if ($token) {
        //$submitedFile = $_FILES['userPhoto']['name'];
        if ($_FILES['subRowPhoto']['name']) {
            $productivesubRow->id = $_POST['id_sub_row'];
            // Check if this user is registered in the system
            $checkSubRow = $productivesubRow->readDeterminedProductiveSubRow();
            if ($checkSubRow) {
                $subRowPhoto = new DocumentStorage('subRowPhoto', $productivesubRow->id, $db);
                $subRowPhoto->fileName = $_FILES['subRowPhoto']['name'];
                $subRowPhoto->fileTemporaryName = $_FILES['subRowPhoto']['tmp_name'];
                $subRowPhoto->objectIdentifier = $productivesubRow->id;
                $response = $subRowPhoto->alterDocument();
                // Return the result
                if ($response)
                    $responseReturned = $returned->returnResult(true, 'Nova imagem submetida com successo', array());
                else
                    $responseReturned = $returned->returnResult(false, 'Imagem não foi submetida', array());
            } else
                $responseReturned = $returned->returnResult(false, 'A subfileira não existe no sistema', array());
        } else
            $responseReturned = $returned->returnResult(false, 'Nehuma imagem submetida', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>