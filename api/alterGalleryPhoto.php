<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: PUT,POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Gallery.php";
require_once "../classes/Returned.php";
require_once "../classes/DocumentStorage.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class gallery
$gallery = new Gallery($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
/* $json = file_get_contents('php://input');
  $data = json_decode($json); */
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    // Get the authorization to access resource
    $token = $userToken->getAuthorization();
    if ($token) {
        //$submitedFile = $_FILES['userPhoto']['name'];
        if ($_FILES['galleryPhoto']['name']) {
            $gallery->id = $_POST['id_gallery'];
            // Check if this user is registered in the system
            $checkEvent = $gallery->readDetermGallery();
            if ($checkEvent) {
                // Alter image 
                //$rep = '../images/users/'.$user->id.'/';
                $galleryImage = new DocumentStorage('galleryPhoto', $gallery->id, $db);
                $galleryImage->fileName = $_FILES['galleryPhoto']['name'];
                $galleryImage->fileTemporaryName = $_FILES['galleryPhoto']['tmp_name'];
                $galleryImage->objectIdentifier = $gallery->id;
                $response = $galleryImage->alterDocument();
                // Return the result
                if ($response)
                    $responseReturned = $returned->returnResult(true, 'Nova imagem submetida com successo', array());
                else
                    $responseReturned = $returned->returnResult(false, 'Imagem não foi submetida', array());
            } else
                $responseReturned = $returned->returnResult(false, 'O a foto não existe no sistema', array());
        } else
            $responseReturned = $returned->returnResult(false, 'Nehuma imagem submetida', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>