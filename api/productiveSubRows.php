<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/ProductiveSubRow.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class productiveSubRow
$productiveSubRow = new ProductiveSubRow($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
        $productiveSubRow->designation = $data->designation;
        if ($productiveSubRow->checkProductiveRow())
            $responseReturned = $returned->returnResult(true, 'Esta Sub Fileira já existe no sistema', array());
        else {
            $productiveSubRow->id = NULL;
            $productiveSubRow->designation = $data->designation;
            $productiveSubRow->comment = $data->comment;
            // Retrieve the response about the register of productiveSubRow
            $response = $productiveSubRow->registerProductiveSubRow();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Sub Fileira registada com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Sub Fileira não registada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $productiveSubRow->readProductiveSubRow(); // Read all productiveSubRow
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Sub Fileira encontrada', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhuma Sub Fileira encontrada', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update productiveSubRow
//        $productiveSubRow->designation = $data->designation;
//        if ($productiveSubRow->checkProductiveRow())
//            $responseReturned = $returned->returnResult(true, 'Esta Sub Fileira já existe no sistema', array());
//        else {
        $productiveSubRow->id = $data->id;
        $productiveSubRow->designation = $data->designation;
        $productiveSubRow->comment = $data->comment;
        // Retrieve the response about the register of productiveSubRow
        $response = $productiveSubRow->updateProductiveSubRow();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Sub Fileira actualizada com successo', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Sub Fileira não actualizada', array());
//        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        foreach ($data->id as $idd) {
            // Delete productiveSubRow
            $productiveSubRow->id = $idd;
            // Retrieve the response about the delete of productiveSubRow
            $response = $productiveSubRow->deleteProductiveSubRow();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Sub Fileira eliminada com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Sub Fileira não eliminada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>