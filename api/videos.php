<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Video.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class video
$video = new Video($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
//        $legislation->title = $data2[1];
        $video->title = $data->title;
//        $legislation->designation = $data2[1];
        if ($video->checkVideo())
            $responseReturned = $returned->returnResult(true, 'Este vídeo já existe no sistema', array());
        else {

            $video->id = NULL;
            $video->title = $data->title;
            $video->link = $data->link;
            $response = $video->registerVideo();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Video registado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Video não registado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $video->readVideo(); // Read all videos 
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Video encontrada', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhuma Foto encontrada', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update  Legislation
//        $productiveRow->designation = $data->designation;
//        if ($productiveRow->checkProductiveRow())
//            $responseReturned = $returned->returnResult(true, 'Esta Fileira já existe no sistema', array());
//        else {
        $video->id = $data->id;
        $video->title = $data->title;
        $video->link = $data->link;

        $response = $video->updateVideo();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Vídeo actualizado com successo', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Vídeo não actualizado', array());
//        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        foreach ($data->id as $idd) {
            // Delete video
            $video->id = $idd;
            // Retrieve the response about the delete of Legislation
            $response = $video->deleteVideo();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Vídeo eliminado com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Vídeo não eliminada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>