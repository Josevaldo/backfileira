<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/ProductionPeriod.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class ProductionPeriod
$produtionPeriod = new ProductionPeriod($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
        $produtionPeriod->designation = $data->designation;
        if ($produtionPeriod->checkProductionPeriod())
            $responseReturned = $returned->returnResult(true, 'Este período já existe no sistema', array());
        else {
            $produtionPeriod->id = NULL;
            $produtionPeriod->designation = $data->designation;
            $produtionPeriod->startDate = $data->start_date;
            $produtionPeriod->endDate = $data->end_date;
            $produtionPeriod->observation = $data->observation;
            $response = $produtionPeriod->registerProductionPeriod();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Período registado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Período não registado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
//        if($token){
    $response = $produtionPeriod->readProductionPeriod(); // Read all productions periods
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Período encontrada', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhum Período encontrada', array());
//        }else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update producyionPeriod
//        $produtionPeriod->designation = $data->designation;
//        if ($produtionPeriod->checkProductionPeriod())
//            $responseReturned = $returned->returnResult(true, 'Este período já existe no sistema', array());
//        else {
            $produtionPeriod->id = $data->id;
            $produtionPeriod->designation = $data->designation;
            $produtionPeriod->startDate = $data->start_date;
            $produtionPeriod->endDate = $data->end_date;
            $produtionPeriod->observation = $data->observation;
            $response = $produtionPeriod->updateProductionPeriod();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Período actualizado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Período não actualizado', array());
//        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        // Delete department
        //$department->id = $data->id;
        foreach ($data->id as $id) {
            $produtionPeriod->id = $id;
            // Retrieve the response about the delete of campaign
            $response = $produtionPeriod->deleteProductionPeriod();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Período eliminado com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Período não eliminado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}

$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>