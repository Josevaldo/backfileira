<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Service.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class service
$service = new Service($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
        $service->acronym = $data->acronym;
        if ($service->checkService())
            $responseReturned = $returned->returnResult(true, 'Este serviço já existe no sistema', array());
        else {
            $service->id = NULL;
            $service->acronym = $data->acronym;
            $service->comment = $data->comment;
            $service->designation = $data->designation;
            $service->domain_name = $data->domain_name;
            $response = $service->registerService();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Serviço registado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Serviço não registado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
//        if($token){
    $response = $service->readService(); // Read all service
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Serviço encontrado', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhum serviço encontrado', array());
//        }else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update service
//        $service->acronym = $data->acronym;
//        if ($service->checkService())
//            $responseReturned = $returned->returnResult(true, 'Este serviço já existe no sistema', array());
//        else {
            $service->id = $data->id;
            $service->acronym = $data->acronym;
            $service->comment = $data->comment;
            $service->designation = $data->designation;
            $service->domain_name = $data->domain_name;
            $response = $service->updateService();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Serviço actualizado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Serviço não actualizado', array());
//        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        // Delete department
        //$department->id = $data->id;
        foreach ($data->id as $id) {
            $service->id = $id;
            // Retrieve the response about the delete of department
            $response = $service->deleteService();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Serviço eliminado com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Serviço não eliminado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}

$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>