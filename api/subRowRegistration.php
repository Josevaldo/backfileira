<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/ProductiveSubRow.php";
require_once "../classes/UserToken.php";
require_once "../classes/Profile.php";
require_once "../classes/DocumentStorage.php";
require_once "../classes/Returned.php";
//require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class productiveSubRow
$productiveSubRow = new ProductiveSubRow($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        //if($token){
        if ((empty($_FILES['subRowPhoto']['name'])))
            $responseReturned = $returned->returnResult(false, 'Falta imagens para submeter', array());
        else {
            $data = json_decode($_POST['sub_row_Data']);
            $productiveSubRow->id = NULL;
            $productiveSubRow->designation = $data->designation;
            $productiveSubRow->comment = $data->comment;

            $subRowExist = $productiveSubRow->checkProductiveRow();
            if ($subRowExist)
                $responseReturned = $returned->returnResult(false, 'Esta subfileira já existe no sistema', array());
            else {
                $response = $productiveSubRow->registerProductiveSubRow();
                // Return the result
                //if($response) $responseReturned = $returned->returnResult(true,'user registado com successo',$response);
                if ($response) {
                    $productiveSubRow->id = $response;
                    $productiveSubRow->subRowPhoto['fileName'] = $_FILES['subRowPhoto']['name'];
                    $productiveSubRow->subRowPhoto['tmpName'] = $_FILES['subRowPhoto']['tmp_name'];
                    // instance the class DocumentStorage
                    $subRowPhotoSubmited = new DocumentStorage('subRowPhoto', $productiveSubRow->id, $db);
                    $subRowPhotoSubmited->fileName = $_FILES['subRowPhoto']['name'];
                    $subRowPhotoSubmited->fileTemporaryName = $_FILES['subRowPhoto']['tmp_name'];
                    // store document
                    $suRowPhotoStored = $subRowPhotoSubmited->storeDocument();

                    if (($suRowPhotoStored))
                        $responseReturned = $returned->returnResult(true, 'Subfileira registada com successo', array());
                    else
                        $responseReturned = $returned->returnResult(false, 'Subfileira não registada', array());
                } else
                    $responseReturned = $returned->returnResult(false, 'Subfileira não registada', array());
                //}
            }
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>