<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Training.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class training
$training = new Training($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);

// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];

if ('POST' === $method) {
    if ($token) {
//        $trainingInitiative->designation = $data->designation;
//        if ($trainingInitiative->checkTrainingIniti())
//            $responseReturned = $returned->returnResult(true, 'Esta iniciativa já existe no sistema', array());
//        else {
        $training->id = NULL;
        $training->quantMale = $data->quant_male;
        $training->quantFemale = $data->quant_female;
        $training->quantFormation = $data->quant_formation;
        $training->idTrainginInitiative = $data->id_training_initiative;
        $training->idProvince = $data->id_province;
        $response = $training->registerTraining();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Capacitação registada com successo', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Capacitação não registada', array());
//        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $training->readTraining();
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Capacitação encontrada', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhuma capacitação encontrada', array());
} elseif ('PUT' === $method) {
    if ($token) {
        $training->id = $data->id;
        $training->quantMale = $data->quant_male;
        $training->quantFemale = $data->quant_female;
        $training->quantFormation = $data->quant_formation;
        $training->idTrainginInitiative = $data->id_training_initiative;
        $training->idProvince = $data->id_province;

        // Retrieve the response about the register of training initiative
        $response = $training->updateTraining();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Capacitação actualizada com successo', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Capacitação não actualizada', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        foreach ($data->id as $idd) {
            $training->id = $idd;
            // Retrieve the response about the delete of training
            $response = $training->deleteTraining();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Capacitação eliminada com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Capacitação não eliminada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>