<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Gallery.php";
require_once "../classes/UserToken.php";
require_once "../classes/DocumentStorage.php";
require_once "../classes/Returned.php";
//require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class Gallery
$gallery = new Gallery($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
/* $json = file_get_contents('php://input');
  $data = json_decode($json); */
// Get the authorization to access resource
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        //if($token){
        if ((empty($_FILES['galleryPhoto']['name'])))
            $responseReturned = $returned->returnResult(false, 'Falta imagens para submeter', array());
        else {
            $data = json_decode($_POST['galleryData']);
            $gallery->id = NULL;
            $gallery->title = $data->title;

            //$user->role = $data->role;
            // Check if the email already exists
            $galleryExist = $gallery->checkGallery();
            if ($galleryExist)
                $responseReturned = $returned->returnResult(false, 'Esta foto já existe no sistema', array());
            else {
                // Check if the tax identification number already exists
                //$tin = $user->checkTaxIdentificationNumberExists();
                //if($tin) $responseReturned = $returned->returnResult(false,'Este NIF já existe no sistema',array());
                //else{
                // Retrieve the response about the register of user
                $response = $gallery->registerGallery();
                // Return the result
                //if($response) $responseReturned = $returned->returnResult(true,'user registado com successo',$response);
                if ($response) {
                    $gallery->id = $response;
                    $gallery->galleryPhoto['fileName'] = $_FILES['galleryPhoto']['name'];
                    $gallery->galleryPhoto['tmpName'] = $_FILES['galleryPhoto']['tmp_name'];
                    // instance the class DocumentStorage
                    $galleryPhotoSubmited = new DocumentStorage('galleryPhoto', $gallery->id, $db);
                    $galleryPhotoSubmited->fileName = $_FILES['galleryPhoto']['name'];
                    $galleryPhotoSubmited->fileTemporaryName = $_FILES['galleryPhoto']['tmp_name'];
                    // store document
                    $galleryPhotoStored = $galleryPhotoSubmited->storeDocument();
                    // instance the class DocumentStorage to birth day person image

                    if (($galleryPhotoStored))
                        $responseReturned = $returned->returnResult(true, 'Foto registada com successo', array());
                    else
                        $responseReturned = $returned->returnResult(false, 'Foto não registada', array());
                } else
                    $responseReturned = $returned->returnResult(false, 'Foto não registada', array());
                //}
            }
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>