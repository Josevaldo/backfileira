<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Banner.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class banner
$banner = new Banner($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if ($token) {
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
        $banner->title = $data->title;
        if ($banner->checkBanner())
            $responseReturned = $returned->returnResult(true, 'Este banner já existe no sistema', array());
        else {
            $banner->id = NULL;
            $banner->title = $data->title;
            $banner->description = $data->description;

            $response = $banner->registerBanner();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Banner registado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Banner não registado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    //if($token){
    $response = $banner->readBanner(); // Read all banners
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Banner encontrado', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhum banner encontrado', array());
    //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update Indicator
        $banner->title = $data->title;
        if ($banner->checkBanner())
            $responseReturned = $returned->returnResult(true, 'Este banner já existe no sistema', array());
        else {
            $banner->id = $data->id;
            $banner->title = $data->title;
            $banner->description = $data->description;
            $response = $banner->updateBanner();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Banner actualizado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Banner não actualizado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        // Delete indicator
        //$direction->id = $data->id;
        foreach ($data->id as $id) {
            $banner->id = $id;
            // Retrieve the response about the delete of indicator
            $response = $banner->deleteBanner();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Banner(s)eliminado(s) com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Banner(s) não eliminado(s)', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//} else
//    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>