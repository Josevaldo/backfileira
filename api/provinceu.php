<?php

header("Access-Control-Allow-Origin: *"); 
header("Content-Type: application/json; charset=UTF-8"); 
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE"); 
header("Access-Control-Max-Age: 3600"); 
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Province.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/*spl_autoload_register();*/

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class province
$province = new Province($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
	// retrieve de method used
	$method = $_SERVER['REQUEST_METHOD'];
	if('POST' === $method){
		if($token){
			$province->id = NULL;
			$province->designation = $data->designation;
			// Retrieve the response about the register of user
			$response = $province->registerProvince();
			// Return the result
			if($response) $responseReturned = $returned->returnResult(true,'Provincia registada com successo',$response);
			else $responseReturned = $returned->returnResult(false,'Provincia não registada',array());
		}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
	}elseif('GET' === $method){
		$response = $province->readProvince(); // Read all province
		if($response) $responseReturned = $returned->returnResult(true,'Provincia encontrada',$response);
		else $responseReturned = $returned->returnResult(false,'Nemhum Provincia encontrada',array());
	}elseif('PUT' === $method){
		if($token){
			// Update province
			$province->id = $data->id;
			$province->designation = $data->designation;
			// Retrieve the response about the update of province
			$response = $province->updateprovince();
			// Return the result
			if($response) $responseReturned = $returned->returnResult(true,'Provincia actualizada com successo',array());
			else $responseReturned = $returned->returnResult(false,'Provincia não actualizada',array());
		}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
	}elseif('DELETE' === $method){
		if($token){
			// Delete province
			$province->id = $data->id;
			// Retrieve the response about the delete of province
			$response = $province->deleteprovince();
			// Return the result
			if($response) $responseReturned = $returned->returnResult(true,'Provincia eliminada com successo',array());
			else $responseReturned = $returned->returnResult(false,'Provincia não eliminada',array());
		}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
	}else{
		$responseReturned = $returned->returnResult(false,'Pedido não executado',array());
	}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>