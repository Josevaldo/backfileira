<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Production.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class product
$production = new Production($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
        $production->id = null;
        $production->registrationDate = date('Y-m-d H:i:s');
        $production->quantity = $data->quantity;
        $production->comment = $data->comment;
        $production->actionType = $data->action_type;
        if (empty($data->available_area))
            $data->available_area = 0;
        $production->availableArea = $data->available_area;
        $production->explorationType = $data->exploration_type;
        $production->idProvince = $data->id_province;
        $production->idProdPeriod = $data->id_prod_period;
        $production->idProductiveRow = $data->id_roductive_row;
        $production->idProductiveSubRow = $data->id_productive_sub_row;
        $production->idProduct = $data->id_product;
        $production->idSubProduct = $data->id_sub_product;

        // Retrieve the response about the register of product
        $response = $production->registerProduction();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Produção registada com successo', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Produção não registada', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $production->readProduction(); // Read all product
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Produção encontrada', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhuma produção encontrada', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update product
        $production->id = $data->id;
        //$production->registrationDate = date('Y-m-d H:i:s');
        $production->quantity = $data->quantity;
        $production->comment = $data->comment;
        $production->actionType = $data->action_type;
        if (empty($data->available_area))
            $data->available_area = 0;
        $production->availableArea = $data->available_area;
        $production->explorationType = $data->exploration_type;
        $production->idProvince = $data->id_province;
        $production->idProdPeriod = $data->id_prod_period;
        $production->idProductiveRow = $data->id_roductive_row;
        $production->idProductiveSubRow = $data->id_productive_sub_row;
        $production->idProduct = $data->id_product;
        $production->idSubProduct = $data->id_sub_product;

        // Retrieve the response about the register of product
        $response = $production->updateProduction();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Produção actualizada com successo', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Produção não actualizada', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        // Delete product
        foreach ($data->id as $idd) {
            $production->id = $idd;
            // Retrieve the response about the delete of product
            $response = $production->deleteProduction();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Produção eliminada com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Produção não eliminada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>
