<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/StructureNationalProduction.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class snp
$snp = new StructureNationalProduction($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
        $snp->idProductiveRow = $data->id_productive_row;
        $snp->idProductiveSubRow = $data->id_productive_sub_row;
        $snp->idProduct = $data->id_product;
        $snp->idSubProduct = $data->id_sub_product;
        $snp->idUnitMeasure = $data->id_unit_measure;
        // Retrieve the response about the register of productive Row
        $response = $snp->registerStructureNationalProduction();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Estrutura da produção nacional registada com successo', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Estrutura da produção nacional não registada', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $snp->readStructureNationalProduction(); // Read all snp
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Estrutura da produção nacional encontrada', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhuma estrutura da produção nacional encontrada', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update productive Row
        $snp->idProductiveRow = $data->id_productive_row;
        $snp->idProductiveSubRow = $data->id_productive_sub_row;
        $snp->idProduct = $data->id_product;
        $snp->idSubProduct = $data->id_sub_product;
        $snp->idUnitMeasure = $data->id_unit_measure;
        foreach ($data->id as $v) {
            $snp->id[] = $v;
        }
        // Retrieve the response about the update of productive Row
        $response = $snp->updateStructureNationalProduction();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Estrutura da produção nacional actualizada com successo', array());
        else
            $responseReturned = $returned->returnResult(false, 'Estrutura da produção nacional não actualizada', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        // Delete snp
        foreach ($data->id as $idd) {
            $snp->idProductiveRow = $idd[0];
            $snp->idProductiveSubRow = $idd[1];
            $snp->idProduct = $idd[2];
            $snp->idSubProduct = $idd[3];
            // Retrieve the response about the delete of productive Row
            $response = $snp->deleteStructureNationalProduction();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Estrutura da produção nacional eliminada com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Estrutura da produção nacional não eliminada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>