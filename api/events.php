<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Eventt.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class event
$event = new Eventt($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
//$data2 = [[2, 3], "Novembro azulax", "2023/11/01", "Luanda", "Uma data para conscientização do cancro da próstata!"];
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
//$method = "DELETE";
$method = $_SERVER['REQUEST_METHOD'];

if ('POST' === $method) {
    if ($token) {
//    $event->title = $data2[1];
        $event->title = $data->title;
        //$productiveRow->designation = $data2[1];
        if ($event->checkEvent())
            $responseReturned = $returned->returnResult(true, 'Este evento já existe no sistema', array());
        else {


            $event->id = NULL;
            $event->title = $data->title;
            $event->event_date = $data->event_date;
            $event->local = $data->local;
            $event->description = $data->description;
            $response = $event->registerEvent();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Evento registado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Evento não registado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $event->readEvent(); // Read all event
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Evento(s)encontrada(s)', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhum evento encontrado', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update event
//        $productiveRow->designation = $data->designation;
//        if ($productiveRow->checkProductiveRow())
//            $responseReturned = $returned->returnResult(true, 'Esta Fileira já existe no sistema', array());
//        else {

        $event->id = $data->id;
        $event->title = $data->title;
        $event->event_date = $data->event_date;
        $event->local = $data->local;
        $event->description = $data->description;

        $response = $event->updateEvent();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Evento actualizado com successo', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Evento não actualizado', array());
//        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
//    if ($token) {
    foreach ($data->id as $idd) {
        // Delete event
        $event->id = $idd;
        // Retrieve the response about the delete of productive Row
        $response = $event->deleteEvent();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Evento eliminado com successo', array());
        else
            $responseReturned = $returned->returnResult(false, 'Evento não eliminado', array());
    }
//    } else
//        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>