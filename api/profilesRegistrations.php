<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");
//header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Profile.php";
require_once "../classes/User.php";
require_once "../classes/Role.php";
require_once "../classes/Service.php";
require_once "../classes/Office.php";
require_once "../classes/Department.php";
require_once "../classes/Role.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class profile
$profile = new Profile($db);
$user = new User($db);
$office = new Office($db);
$department = new Department($db);
$direction = new Direction($db);
$role = new Role($db);
$service = new Service($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if ($token) {
// retrieve de method used
//$method = $_SERVER['REQUEST_METHOD'];
$method = "POST";
if ('POST' === $method) {
//    if ($token) {
    if (!$user->checkEmailExists2($data->email_user) & !$office->checkOffice2($data->name_office) & !$department->checkDepartment2($data->name_department) & !$direction->checkDirection2($data->name_direction) & !$role->checkRole2($data->name_role) & !$service->checkService2($data->acronym_service)) {
        //User office datas
        $office->id = NULL;
        $office->description = $data->description_office;
        $office->name = $data->name_office;
        $idOffice = $office->registerOffice();
        //User direction datas
        $direction->id = NULL;
        $direction->description = $data->description_direction;
        $direction->name = $data->name_direction;
        $direction->acronym = $data->acronym_direction;
        $idDirection = $direction->registerDirection();
//User department datas
        $department->id = NULL;
        $department->description = $data->description_department;
        $department->name = $data->name_department;
        $department->acronym = $data->acronym_department;
        $department->idDirection = $idDirection;
        $idDepartment = $department->registerDepartment();

        //User datas
        $user->id = NULL;
        $user->name = $data->name_user;
        $user->email = $data->email_user;
        //$user->password = $data->password;
        $user->password = password_hash($data->password_user, PASSWORD_DEFAULT);
        $user->telephone = $data->telephone;
        $user->dateBirth = $data->date_birth_user;
        $user->idOffice = $idOffice;
        $user->idDepartment = $idDepartment;
        $idUser = $user->registerUser();
        //User role datas
        $role->id = NULL;
        $role->name = $data->name_role;
        $role->acronym = $data->acronym_role;
        $role->designation = $data->designation_role;
        $idRole = $role->registerRole();
        //User service datas
        $service->id = NULL;
        $service->acronym = $data->acronym_service;
        $service->comment = $data->comment_service;
        $service->designation = $data->designation_service;
        $service->domain_name = $data->domain_name_service;
        $idService = $service->registerService();
        //Profile´s datas
        $profile->idUser = $idUser;
        $profile->idRole = $idRole;
        $profile->idService = $idService;
        $profile->observation = "Home Programming";
        // Retrieve the response about the register of profile
        $response = $profile->registerProfile();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Perfil registado com successo', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Perfil não registado', array());
    } elseif ($user->checkEmailExists2($data->email_user) & $office->checkOffice2($data->name_office) & $department->checkDepartment2($data->name_department) & $direction->checkDirection2($data->name_direction) & $role->checkRole2($data->name_role) & $service->checkService2($data->acronym_service)) {

        //Profile´s datas
        $profile->idUser = $user->checkEmailExists2($data->email_user);
        $profile->idRole = $role->checkRole2($data->name_role);
        $profile->idService = $service->checkService2($data->acronym_service);
        $profile->observation = "Home Programming";
        // Retrieve the response about the register of profile
        $response = $profile->registerProfile();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Perfil registado com successo 2', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Perfil não registado', array());
    } else
        echo 'I´m there';


//    } else
//        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $profile->readProfile(); // Read all profile
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Perfis encontrados', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhum perfil encontrado', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update profile
        $profile->id = $data->id;
        $profile->idUser = $data->id_user;
        $profile->idRole = $data->id_role;
        $profile->idService = $data->id_service;
        $profile->observation = $data->observation;
        // Retrieve the response about the update of profile
        $response = $profile->updateProfile();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Perfil actualizado com successo', array());
        else
            $responseReturned = $returned->returnResult(false, 'Perfil não actualizado', array());
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {

// Delete profile
        //$profile->id = $data->id;
        foreach ($data->id as $id) {
            $profile->id = $id;

            // Retrieve the response about the delete of profile
            $response = $profile->deleteProfile();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Perfil eliminado com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Perfil não eliminado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//} else
//    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>