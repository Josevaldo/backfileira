<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Ppn.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";

//require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class ppn
$ppn = new Ppn($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
//    $ppn->designation = $data->designation;
//    if ($fair->checkFair())
//        $responseReturned = $returned->returnResult(true, 'Esta acção já existe no sistema', array());
//    else {
        $ppn->id = NULL;
        $ppn->idProvince = $data->id_province;
        $ppn->agricuture = $data->agricuture;
        $ppn->livestock = $data->agricuture;
        $ppn->fishing = $data->fishing;
        $ppn->productionIndustry = $data->production_industry;
        $response = $ppn->registerPpn();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'ppn registado com successo', $response);
        else
            $responseReturned = $returned->returnResult(false, 'ppn não registado', array());
//    }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
//        if($token){
    $response = $ppn->readPpn(); // Read all ppn
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Ppn encontrado', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhum ppn encontrado', array());
//        }else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update department
//        $department->name = $data->name;
//        if ($department->checkDepartment())
//            $responseReturned = $returned->returnResult(true, 'Este departamento já existe no sistema', array());
//        else {
        $ppn->id = $data->id;
        $ppn->idProvince = $data->id_province;
        $ppn->agricuture = $data->agricuture;
        $ppn->livestock = $data->livestock;
        $ppn->fishing = $data->fishing;
        $ppn->productionIndustry = $data->production_industry;
        $response = $ppn->updatePpn();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Ppn actualizado com successo', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Ppn não actualizado', array());
//        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        // Delete ppn
        //$department->id = $data->id;
        foreach ($data->id as $id) {
            $ppn->id = $id;
            // Retrieve the response about the delete of department
            $response = $ppn->deletePpn();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Ppn eliminado com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Ppn não eliminado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}

$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>