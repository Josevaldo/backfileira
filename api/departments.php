<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Department.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class department
$department = new Department($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
        $department->name = $data->name;
        if ($department->checkDepartment())
            $responseReturned = $returned->returnResult(true, 'Este departamento já existe no sistema', array());
        else {
            $department->id = NULL;
            $department->description = $data->description;
            $department->name = $data->name;
            $department->acronym = $data->acronym;
            $department->idDirection = $data->id_direction;
            $response = $department->registerDepartment();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Departamento registado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Departamento não registado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
//        if($token){
    $response = $department->readDepartment(); // Read all department
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Departamento encontrado', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhum Departamento encontrado', array());
//        }else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update department
        $department->name = $data->name;
        if ($department->checkDepartment())
            $responseReturned = $returned->returnResult(true, 'Este departamento já existe no sistema', array());
        else {
            $department->id = $data->id;
            $department->description = $data->description;
            $department->name = $data->name;
            $department->acronym = $data->acronym;
            $department->idDirection = $data->id_direction;
            $response = $department->updateDepartment();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Departamento actualizado com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Departamento não actualizado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        // Delete department
        //$department->id = $data->id;
        foreach ($data->id as $id) {
            $department->id = $id;
            // Retrieve the response about the delete of department
            $response = $department->deleteDepartment();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Departamento eliminado com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Departamento não eliminado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}

$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>