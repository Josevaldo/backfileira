<?php

header("Access-Control-Allow-Origin: *"); 
header("Content-Type: application/json; charset=UTF-8"); 
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE"); 
header("Access-Control-Max-Age: 3600"); 
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/UnitMeasure.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/*spl_autoload_register();*/

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class unitMeasure
$unitMeasure = new UnitMeasure($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
	// retrieve de method used
	$method = $_SERVER['REQUEST_METHOD'];
	if ('POST' === $method) {
		if($token){
			$unitMeasure->id = NULL;
			$unitMeasure->designation = $data->designation;
			$unitMeasure->symbol = $data->symbol;
			// Retrieve the response about the register of unitMeasure
			$response = $unitMeasure->registerUnitMeasure();
			// Return the result
			if($response) $responseReturned = $returned->returnResult(true,'Unidade de Medida registada com successo',$response);
			else $responseReturned = $returned->returnResult(false,'Unidade de Medida não registada',array());
		}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
	}elseif('GET' === $method){
		$response = $unitMeasure->readUnitMeasure(); // Read all unitMeasure
		if($response) $responseReturned = $returned->returnResult(true,'Unidade de Medida encontrada',$response);
		else $responseReturned = $returned->returnResult(false,'Nemhum Unidade de Medida encontrada',array());
	}elseif('PUT' === $method){
		if($token){
			// Update unitMeasure
			$unitMeasure->id = $data->id;
			$unitMeasure->designation = $data->designation;
			$unitMeasure->symbol = $data->symbol;
			// Retrieve the response about the update of unitMeasure
			$response = $unitMeasure->updateUnitMeasure();
			// Return the result
			if($response) $responseReturned = $returned->returnResult(true,'Unidade de Medida actualizada com successo',array());
			else $responseReturned = $returned->returnResult(false,'Unidade de Medida não actualizada',array());
		}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
	}elseif('DELETE' === $method){
		if($token){
			// Delete unitMeasure
			$unitMeasure->id = $data->id;
			// Retrieve the response about the delete of unitMeasure
			$response = $unitMeasure->deleteUnitMeasure();
			// Return the result
			if($response) $responseReturned = $returned->returnResult(true,'Unidade de Medida eliminada com successo',array());
			else $responseReturned = $returned->returnResult(false,'Unidade de Medida não eliminada',array());
		}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
	}else{
		$responseReturned = $returned->returnResult(false,'Pedido não executado',array());
	}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>