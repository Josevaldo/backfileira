<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/TrainingIniciative.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class training initiative
$trainingInitiative = new TrainingIniciative($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);

// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];

if ('POST' === $method) {
    if ($token) {
        $trainingInitiative->designation = $data->designation;
        if ($trainingInitiative->checkTrainingIniti())
            $responseReturned = $returned->returnResult(true, 'Esta iniciativa já existe no sistema', array());
        else {
            $trainingInitiative->id = NULL;
            $trainingInitiative->designation = $data->designation;
            $trainingInitiative->comment = $data->comment;

            // Retrieve the response about the register of tradeBalance Category
            $response = $trainingInitiative->registerTrainingIniti();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Iniciativa registada com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Iniciativa não registada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $trainingInitiative->readTradeTrainingIniti(); // Read all training initiative
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Iniciativa encontrada', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhuma iniciativa encontrada', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update trade balance
//        $province->designation = $data->designation;
//        if ($province->checkProvince())
//            $responseReturned = $returned->returnResult(true, 'Esta Província já existe no sistema', array());
//        else {
        $trainingInitiative->id = $data->id;
        $trainingInitiative->designation = $data->designation;
        $trainingInitiative->comment = $data->comment;

        // Retrieve the response about the register of training initiative
        $response = $trainingInitiative->updateTrainingIniti();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Iniciativa actualizada com successo', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Iniciativa não actualizada', array());
//        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        // Delete trade balance category
        foreach ($data->id as $idd) {
            $trainingInitiative->id = $idd;
            // Retrieve the response about the delete of training initiative
            $response = $trainingInitiative->deleteTrainingIniti();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Iniciativa eliminada com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Iniciativa não eliminada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>