<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Fair.php";
require_once "../classes/Returned.php";
//require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class fair
$fair = new Fair($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
//$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
//$token = $userToken->getAuthorization();
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
//    if ($token) {
//    $fair->designation = $data->designation;
//    if ($fair->checkFair())
//        $responseReturned = $returned->returnResult(true, 'Esta acção já existe no sistema', array());
//    else {
    $fair->id = NULL;
    $fair->idProvince = $data->id_province;
    $fair->heldFair = $data->held_fair;
    $fair->quantProductor = $data->quant_productor;
    $fair->quantFisherman = $data->quant_fisherman;
    $fair->quantMarketer = $data->quant_marketer;
    $fair->quantActivity = $data->quant_activity;
    $fair->quantPartnership = $data->quant_partnership;
    $fair->quantFormer = $data->quant_former;
    $fair->quantSponsor = $data->quant_sponsor;
    $fair->quantOcd = $data->quant_ocd;
    $fair->madeContract = $data->made_contract;
    $fair->transactionalAmount = $data->transactional_amount;
    $fair->cateringOperator = $data->catering_operator;
    $fair->serviceProvider = $data->service_provider;
    $response = $fair->registerFair();
    // Return the result
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Feira registada com successo', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Feira não registada', array());
//    }
//    } else
//        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
//        if($token){
    $response = $fair->readFair(); // Read all fair
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Feira encontrada', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhuma feira encontrada', array());
//        }else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
} elseif ('PUT' === $method) {
//    if ($token) {
    // Update department
//        $department->name = $data->name;
//        if ($department->checkDepartment())
//            $responseReturned = $returned->returnResult(true, 'Este departamento já existe no sistema', array());
//        else {
    $fair->id = $data->id;
    $fair->idProvince = $data->id_province;
    $fair->heldFair = $data->held_fair;
    $fair->quantProductor = $data->quant_productor;
    $fair->quantFisherman = $data->quant_fisherman;
    $fair->quantMarketer = $data->quant_marketer;
    $fair->quantActivity = $data->quant_activity;
    $fair->quantPartnership = $data->quant_partnership;
    $fair->quantFormer = $data->quant_former;
    $fair->quantSponsor = $data->quant_sponsor;
    $fair->quantOcd = $data->quant_ocd;
    $fair->madeContract = $data->made_contract;
    $fair->transactionalAmount = $data->transactional_amount;
    $fair->cateringOperator = $data->catering_operator;
    $fair->serviceProvider = $data->service_provider;
    $response = $fair->updateFair();
    // Return the result
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Feira actualizada com successo', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Feira não actualizada', array());
//        }
//    } else
//        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
//    if ($token) {
    // Delete department
    //$department->id = $data->id;
    foreach ($data->id as $id) {
        $fair->id = $id;
        // Retrieve the response about the delete of department
        $response = $fair->deleteFair();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Feira eliminada com successo', array());
        else
            $responseReturned = $returned->returnResult(false, 'Feira não eliminada', array());
    }
//    } else
//        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}

$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>