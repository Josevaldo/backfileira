<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/TradeBalanceCategory.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class tradeBalanceCategory
$tradeBalanceCateg = new TradeBalanceCategory($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);

// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];

if ('POST' === $method) {
    if ($token) {
        $tradeBalanceCateg->designation = $data->designation;
        if ($tradeBalanceCateg->checkTradeBalaCate())
            $responseReturned = $returned->returnResult(true, 'Esta categoria já existe no sistema', array());
        else {
            $tradeBalanceCateg->id = NULL;
            $tradeBalanceCateg->designation = $data->designation;
            $tradeBalanceCateg->comment = $data->comment;

            // Retrieve the response about the register of tradeBalance Category
            $response = $tradeBalanceCateg->registerTradeBalaCate();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Categoria registada com successo', $response);
            else
                $responseReturned = $returned->returnResult(false, 'Categoria não registada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $tradeBalanceCateg->readTradeBalaCate(); // Read all trade balance category
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Categoria encontrada', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhuma Categoria encontrada', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update trade balance
//        $province->designation = $data->designation;
//        if ($province->checkProvince())
//            $responseReturned = $returned->returnResult(true, 'Esta Província já existe no sistema', array());
//        else {
        $tradeBalanceCateg->id = $data->id;
        $tradeBalanceCateg->designation = $data->designation;
        $tradeBalanceCateg->comment = $data->comment;

        // Retrieve the response about the register of user
        $response = $tradeBalanceCateg->updateTradeBalaCate();
        // Return the result
        if ($response)
            $responseReturned = $returned->returnResult(true, 'Categoria actualizada com successo', $response);
        else
            $responseReturned = $returned->returnResult(false, 'Categoria não actualizada', array());
//        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        // Delete trade balance category
        foreach ($data->id as $idd) {
            $tradeBalanceCateg->id = $idd;
            // Retrieve the response about the delete of trade balance category
            $response = $tradeBalanceCateg->deleteTradeBalaCate();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Categoria eliminada com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Categoria não eliminada', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>