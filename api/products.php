<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Product.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class product
$product = new Product($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
// Takes raw data from the request 
$json = file_get_contents('php://input');
$data = json_decode($json);
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
// retrieve de method used
$method = $_SERVER['REQUEST_METHOD'];
if ('POST' === $method) {
    if ($token) {
// Register product
        $product->designation = $data->designation;
        if ($product->checkPruduct())
            $responseReturned = $returned->returnResult(true, 'Este Produto já existe no sistema', array());
        else {
            $product->id = null;
            $product->designation = $data->designation;
            $product->customsTariff = $data->customs_tariff;
            $product->prodesiProduct = $data->prodesi_product;
            // Retrieve the response about the update of product
            $response = $product->registerProduct();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Produto registrado com sucesso com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Produto não regsitrado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('GET' === $method) {
    $response = $product->readProduct(); // Read all product
    if ($response)
        $responseReturned = $returned->returnResult(true, 'Produto encontrado', $response);
    else
        $responseReturned = $returned->returnResult(false, 'Nemhum produto encontrado', array());
} elseif ('PUT' === $method) {
    if ($token) {
        // Update product
//        $product->designation = $data->designation;
//        if ($product->checkPruduct())
//            $responseReturned = $returned->returnResult(true, 'Este Produto já existe no sistema', array());
//        else {
            $product->id = $data->id;
            $product->designation = $data->designation;
            $product->customsTariff = $data->customs_tariff;
            $product->prodesiProduct = $data->prodesi_product;
            // Retrieve the response about the update of product
            $response = $product->updateProduct();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Produto actualizado com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Produto não actualizado', array());
//        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} elseif ('DELETE' === $method) {
    if ($token) {
        // Delete product
        foreach ($data->id as $idd) {
            $product->id = $idd;
            // Retrieve the response about the delete of product
            $response = $product->deleteProduct();
            // Return the result
            if ($response)
                $responseReturned = $returned->returnResult(true, 'Produto eliminado com successo', array());
            else
                $responseReturned = $returned->returnResult(false, 'Produto não eliminado', array());
        }
    } else
        $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
} else {
    $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>