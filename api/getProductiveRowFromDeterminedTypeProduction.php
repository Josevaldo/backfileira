<?php

header("Access-Control-Allow-Origin: *"); 
header("Content-Type: application/json; charset=UTF-8"); 
header("Access-Control-Allow-Methods: GET"); 
header("Access-Control-Max-Age: 3600"); 
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/ProductiveRow.php";
require_once "../classes/Returned.php";
require_once "../classes/UserToken.php";
/*spl_autoload_register();*/

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class province
$productiveRow = new ProductiveRow($db);
// instance the class that create the user's token results
$userToken = new UserToken();
// instance the class returned
$returned = new Returned();
// Get the authorization to access resource
$token = $userToken->getAuthorization();
//if($token){
	// retrieve de method used
	$method = $_SERVER['REQUEST_METHOD'];
	if ('GET' === $method) {
		$productiveRow->idTypeProduction = $_GET['id_type_production'];
		// Read determined fileira
		$response = $productiveRow->getProductiveRowFromDeterminedTypeProduction();
		if($response) $responseReturned = $returned->returnResult(true,'Fileira encontrada com successo',$response);
		else $responseReturned = $returned->returnResult(false,'Fileira não encontrada',array());
	}else{
		$responseReturned = $returned->returnResult(false,'Pedido não executado',array());
	}
//}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>