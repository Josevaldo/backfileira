<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST,GET,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
//header("Access-Control-Allow-Headers: Content-Type");

require_once "../classes/DatabaseConnection.php";
require_once "../classes/Eventt.php";
require_once "../classes/UserToken.php";
require_once "../classes/Profile.php";
require_once "../classes/DocumentStorage.php";
require_once "../classes/Returned.php";
//require_once "../classes/UserToken.php";
/* spl_autoload_register(); */

//Instance the class DatabaseConnection
$databaseConnection = new DatabaseConnection();
$db = $databaseConnection->tryConnect();
// instance the class event
$event = new Eventt($db);
// instance the class that return results
$returned = new Returned();
// instance the class that create the user's token results
$userToken = new UserToken();
$token = $userToken->getAuthorization();
if ($token) {
    // retrieve de method used
    $method = $_SERVER['REQUEST_METHOD'];
    if ('POST' === $method) {
        //if($token){
        if ((empty($_FILES['eventPhoto']['name'])))
            $responseReturned = $returned->returnResult(false, 'Falta imagens para submeter', array());
        else {
            $data = json_decode($_POST['event_data']);
            $event->id = NULL;
            $event->title = $data->title;
            $event->event_date = $data->event_date;
            $event->local = $data->local;
            $event->description = $data->description;

            $subProductExist = $event->checkEvent();
            if ($subProductExist)
                $responseReturned = $returned->returnResult(false, 'Este evento já existe no sistema', array());
            else {
                $response = $event->registerEvent();
                // Return the result
                //if($response) $responseReturned = $returned->returnResult(true,'user registado com successo',$response);
                if ($response) {
                    $event->id = $response;
                    $event->eventPhoto['fileName'] = $_FILES['eventPhoto']['name'];
                    $event->eventPhoto['tmpName'] = $_FILES['eventPhoto']['tmp_name'];
                    // instance the class DocumentStorage
                    $PhotoSubmited = new DocumentStorage('eventPhoto', $event->id, $db);
                    $PhotoSubmited->fileName = $_FILES['eventPhoto']['name'];
                    $PhotoSubmited->fileTemporaryName = $_FILES['eventPhoto']['tmp_name'];
                    // store document
                    $PhotoStored = $PhotoSubmited->storeDocument();
                    if (($PhotoStored))
                        $responseReturned = $returned->returnResult(true, 'Evento registado com successo', array());
                    else
                        $responseReturned = $returned->returnResult(false, 'Evento não registado', array());
                } else
                    $responseReturned = $returned->returnResult(false, 'Evento não registado', array());
                //}
            }
        }
        //}else $responseReturned = $returned->returnResult(false,'Acesso ao serviço não autorizado',array());
    } else {
        $responseReturned = $returned->returnResult(false, 'Pedido não executado', array());
    }
} else
    $responseReturned = $returned->returnResult(false, 'Acesso ao serviço não autorizado', array());
$responseReturned = json_encode($responseReturned);
echo $responseReturned;
//print_r($data);
//var_dump($data);
http_response_code();
?>